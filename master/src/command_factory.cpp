#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <boost/algorithm/string.hpp>
#include "command_factory.h"

using namespace std;

namespace jdmq
{
	CommandFactory::CommandFactory(std::string name): name(name)
	{
	}

	CommandFactory::~CommandFactory()
	{
		for (command_map::iterator it = cmd_map.begin(); it != cmd_map.end(); it++)
			delete it->second;
	}

	void CommandFactory::register_command(std::string key, Command *cmd)
	{
		cmd_map[key] = cmd;
		CommandAutocomplete::get_instance().add_key(key);
	}

	void CommandFactory::run(void *ptr)
	{
		cout << "Begin interactive shell" << endl;
		print_help();

		int history = 0;
		string history_file;
		char *line;
		string str_line;
		string key;
		arg_list args;

		if (isatty(fileno(stdin))) {
			history = 1;
			if (getenv("HOME") != NULL) {
				history_file += getenv("HOME");
				history_file += "/." + name;
				history_file += "_test_cli.history";
				linenoiseHistoryLoad(history_file.c_str());
			}
		}

		CommandAutocomplete & ca = CommandAutocomplete::get_instance();
		ca.add_key("help");
		ca.add_key("clear");
		ca.add_key("quit");
		ca.add_key("exit");
		linenoiseSetCompletionCallback(CommandAutocomplete::completion);

		while (1) {
			key = "";
			args.clear();

			line = linenoise(">>> ");
			if (line == NULL)
				break;			
			str_line = line;
			if (history) 
				linenoiseHistoryAdd(line);
			if (history_file.size() > 0) 
				linenoiseHistorySave(history_file.c_str());
			free(line);

			boost::trim(str_line);

			if (str_line.size() == 0)
				continue;

			boost::split(args, str_line, boost::is_any_of(" \t\n\r"), boost::token_compress_on);
			key = args[0];
			args.erase(args.begin());

			if (key == "help") {
				print_help();
			} else if (key == "clear") {
				linenoiseClearScreen();
			} else if (key == "quit" || key == "exit") {
				break;
			} else if (cmd_map.find(key) != cmd_map.end()) {
				if (cmd_map[key]->execute(ptr, args))
					break;
			} else {
				cout << "Wrong command: " << key << "(" << key.size() << "). Type help for command list" << endl;
			}
		}
	}

	void CommandFactory::print_help()
	{
		cout << "Command list:" << endl;
		cout << "\t-help" << endl << "\t-quit" << endl;
		command_map::iterator it;
		for (it = cmd_map.begin(); it != cmd_map.end(); ++it)
			cout << "\t-" << (it->first) << endl; 
	}

	CommandAutocomplete::CommandAutocomplete()
	{
	}

	CommandAutocomplete::~CommandAutocomplete()
	{
	}

	CommandAutocomplete & CommandAutocomplete::get_instance()
	{
		static CommandAutocomplete instance;
		return instance;
	}

	void CommandAutocomplete::add_key(string key)
	{
		command_keys.push_back(key);
	}

	const vector<string> & CommandAutocomplete::get_command_keys()
	{
		return command_keys;
	}

	void CommandAutocomplete::completion(const char *buf, linenoiseCompletions *lc)
	{
		int i;
		const vector<string> & command_keys = CommandAutocomplete::get_instance().get_command_keys();
		for (i = 0; i < command_keys.size(); i++) {
			if (command_keys[i].find(buf) == 0)
				linenoiseAddCompletion(lc, command_keys[i].c_str());
		}
	}
}
