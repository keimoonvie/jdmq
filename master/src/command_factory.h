#ifndef JDMQ_MASTER_COMMAND_FACTORY_H_
#define JDMQ_MASTER_COMMAND_FACTORY_H_

#include <string>
#include <map>
#include "line_noise.h"
#include "command.h"

namespace jdmq
{
	typedef std::map<std::string, Command *> command_map;

	class CommandFactory
	{
	public:
		CommandFactory(std::string name);
		virtual ~CommandFactory();
		void register_command(std::string key, Command *cmd);
		void run(void *ptr);
	private:
		void print_help();
		command_map cmd_map;
		std::string name;
	};

	class CommandAutocomplete
	{
	public:
		static CommandAutocomplete & get_instance();
		virtual ~CommandAutocomplete();

		void add_key(std::string key);
		const std::vector<std::string> & get_command_keys();
		static void completion(const char *buf, linenoiseCompletions *lc);
	private:
		CommandAutocomplete();
		std::vector<std::string> command_keys;
	};
}

#endif
