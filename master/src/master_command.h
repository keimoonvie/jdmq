#ifndef JDMQ_MASTER_MASTER_COMMAND_H_
#define JDMQ_MASTER_MASTER_COMMAND_H_

#include "command.h"

namespace jdmq
{
	class AddGroupCommand: public Command
	{
	public:
		bool execute(void *ptr, arg_list args);
	};

	class RemoveGroupCommand: public Command
        {
	public:
		bool execute(void *ptr, arg_list args);
        };

	class AddConnectToCommand: public Command
        {
	public:
		bool execute(void *ptr, arg_list args);
        };

	class RemoveConnectToCommand: public Command
        {
	public:
		bool execute(void *ptr, arg_list args);
        };

	class ViewGroupCommand: public Command
	{
	public:
		bool execute(void *ptr, arg_list args);
	};

	class ViewMasterCommand: public Command
        {
        public:
                bool execute(void *ptr, arg_list args);
        };

	class ViewMasterConfigCommand: public Command
        {
        public:
                bool execute(void *ptr, arg_list args);
        };
}

#endif

