#ifdef __FreeBSD__
#define _WITH_GETLINE
#endif
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <jdmq/jdmq.h>
#include "master_command.h"

using namespace std;

namespace jdmq
{
	string read_line(const string & question)
	{
		cout << question;
		fflush(stdout);
		char *line = NULL;
		size_t len = 0;
		ssize_t size = getline(&line, &len, stdin);
		string data;
		if (line) {
			data.assign(line, size);
			free(line);
		}
		boost::trim(data);
		return data;
	}

	vector<string> read_vector(const string & question)
	{
		string line = read_line(question + "(separate items by space) ");
		vector<string> items;
		boost::split(items, line, boost::is_any_of(" \t\n\r"), boost::token_compress_on);
		return items;
	}

	void print_node_info(const NodeInfoMessage & node_info)
	{
		cout << "-------- Node info " << node_info.node_id() << " -------" << endl;
		cout << "Group: " << node_info.group() << endl;
		cout << "Host: " << node_info.host() << endl;
		cout << "Port: " << node_info.port() << endl;
		cout << "State: " << node_info.state() << endl;
	}

	void print_node_group(const NodeGroupMessage & node_group)
	{
		cout << "======= Node Group " << node_group.name() << " =======" << endl;
		cout << "Self connected: " << node_group.self_connected() << endl;
		cout << "Connect from: ";
		for (int i = 0; i < node_group.connect_from_size(); i++)
			cout << node_group.connect_from(i) << ", ";
		cout << endl;
		cout << "Connect to: ";
		for (int i = 0; i < node_group.connect_to_size(); i++)
			cout << node_group.connect_to(i) << ", ";
                cout << endl;
		cout << "Node info: " << endl;
		for (int i = 0; i < node_group.node_size(); i++)
			print_node_info(node_group.node(i));
	}

	bool AddGroupCommand::execute(void *ptr, arg_list args)
	{
		client_t *client = (client_t *) ptr;
		AddGroupMessage message;
		message.set_name(read_line("Name: "));
		message.set_self_connected(boost::lexical_cast<bool>(read_line("Self connected [0/1]: ")));
		vector<string> connect_to = read_vector("Connect to: ");
		for (int i = 0; i < connect_to.size(); i++)
			message.add_connect_to(connect_to[i]);
		vector<string> connect_from = read_vector("Connect from: ");
		for (int i = 0;i < connect_from.size(); i++)
			message.add_connect_from(connect_from[i]);
		string data = message.SerializeAsString();
		message_t packet(data.data(), data.size(), ADD_GROUP_MSG);
		message_t *reply;
		if (client->send_sync("master", &packet, &reply)) {
			if (reply->get_type() == RET_ADD_GROUP_ERROR_MSG || reply->get_type() == RET_ADD_GROUP_SUCCESS_MSG) {
				ACKMessage ack;
				ProtocolUtils::parse_proto(reply, &ack);
                                cout << "Code: " << ack.code() << ", " << ack.message() << endl;
			} else {
				cerr << "Message type error: " << reply->get_type() << endl;
                        }
                        delete(reply);
		}
		return false;
	}

	bool RemoveGroupCommand::execute(void *ptr, arg_list args)
        {
		if (args.size() < 1) {
                        cerr << "Usage: remove-group name" << endl;
                        return false;
                }
		client_t *client = (client_t *) ptr;
		RemoveGroupMessage message;
                message.set_name(args[0]);
                string data = message.SerializeAsString();
                message_t packet(data.data(), data.size(), REMOVE_GROUP_MSG);
                message_t *reply;
                if (client->send_sync("master", &packet, &reply)) {
			if (reply->get_type() == RET_REMOVE_GROUP_ERROR_MSG || reply->get_type() == RET_REMOVE_GROUP_SUCCESS_MSG) {
                                ACKMessage ack;
				ProtocolUtils::parse_proto(reply, &ack);
                                cout << "Code: " << ack.code() << ", " << ack.message() << endl;
                        } else {
                                cerr << "Message type error: " << reply->get_type() << endl;
                        }
			delete reply;
                }
		return false;
	}

	bool AddConnectToCommand::execute(void *ptr, arg_list args)
        {
		client_t *client = (client_t *) ptr;
                AddConnectToMessage message;
                message.set_name(read_line("Name: "));
                vector<string> connect_to = read_vector("Connect to: ");
                for (int i = 0; i < connect_to.size(); i++)
                        message.add_connect_to(connect_to[i]);
                string data = message.SerializeAsString();
                message_t packet(data.data(), data.size(), ADD_CONNECT_TO_MSG);
                message_t *reply;
                if (client->send_sync("master", &packet, &reply)) {
                        if (reply->get_type() == RET_ADD_CONNECT_TO_ERROR_MSG || reply->get_type() == RET_ADD_CONNECT_TO_SUCCESS_MSG) {
                                ACKMessage ack;
				ProtocolUtils::parse_proto(reply, &ack);
                                cout << "Code: " << ack.code() << ", " << ack.message() << endl;
			} else {
				cerr << "Message type error: " << reply->get_type() << endl;
                        }
			delete(reply);
                }
                return false;
	}

	bool RemoveConnectToCommand::execute(void *ptr, arg_list args)
        {
		client_t *client = (client_t *) ptr;
                RemoveConnectToMessage message;
                message.set_name(read_line("Name: "));
                vector<string> connect_to = read_vector("Connect to: ");
                for (int i = 0; i < connect_to.size(); i++)
                        message.add_connect_to(connect_to[i]);
                string data = message.SerializeAsString();
                message_t packet(data.data(), data.size(), REMOVE_CONNECT_TO_MSG);
                message_t *reply;
		if (client->send_sync("master", &packet, &reply)) {
                        if (reply->get_type() == RET_REMOVE_CONNECT_TO_ERROR_MSG || reply->get_type() == RET_REMOVE_CONNECT_TO_SUCCESS_MSG) {
                                ACKMessage ack;
				ProtocolUtils::parse_proto(reply, &ack);
                                cout << "Code: " << ack.code() << ", " << ack.message() << endl;
			} else {
				cerr << "Message type error: " << reply->get_type() << endl;
                        }
			delete(reply);
                }
                return false;
	}

	bool ViewGroupCommand::execute(void *ptr, arg_list args)
        {
		if (args.size() < 1) {
			cerr << "Usage: view-group name" << endl;
			return false;
		}
		client_t *client = (client_t *) ptr;
		ViewGroupMessage message;
		message.set_name(args[0]);
		string data = message.SerializeAsString();
		message_t packet(data.data(), data.size(), VIEW_GROUP_MSG);
		message_t *reply;
		if (client->send_sync("master", &packet, &reply)) {
			if (reply->get_type() == RET_VIEW_GROUP_ERROR_MSG) {
				ACKMessage ack;
				ProtocolUtils::parse_proto(reply, &ack);
				cout << "Error: " << ack.code() << ", " << ack.message() << endl;
			} else if (reply->get_type() == RET_VIEW_GROUP_SUCCESS_MSG) {
				NodeGroupMessage group;
				ProtocolUtils::parse_proto(reply, &group);
				print_node_group(group);
			} else {
				cerr << "Message type error: " << reply->get_type() << endl;
			}
			delete(reply);
		}
                return false;
        }

	bool ViewMasterCommand::execute(void *ptr, arg_list args)
        {
		client_t *client = (client_t *) ptr;
                string data = "";
		message_t packet(data.data(), data.size(), VIEW_MASTER_MSG);
                message_t *reply;
		if (client->send_sync("master", &packet, &reply)) {
                        if (reply->get_type() == RET_VIEW_MASTER_ERROR_MSG) {
				ACKMessage ack;
				ProtocolUtils::parse_proto(reply, &ack);
				cout << "Error: " << ack.code() << ", " << ack.message() << endl;
			} else if (reply->get_type() == RET_VIEW_MASTER_SUCCESS_MSG) {
				ServerInfoMsg groups;
				ProtocolUtils::parse_proto(reply, &groups);
				for (int i = 0; i < groups.group_size(); i++)
					print_node_group(groups.group(i));
			} else {
				cerr <<"Message type error: " << reply->get_type() << endl;
			}
                        delete(reply);
		}
                return false;
	}

	bool ViewMasterConfigCommand::execute(void *ptr, arg_list args)
        {
		client_t *client = (client_t *) ptr;
		string data = "";
                message_t packet(data.data(), data.size(), VIEW_MASTER_CONFIG_MSG);
		message_t *reply;
		if (client->send_sync("master", &packet, &reply)) {
			if (reply->get_type() == RET_VIEW_MASTER_CONFIG_ERROR_MSG) {
				ACKMessage ack;
				ProtocolUtils::parse_proto(reply, &ack);
                                cout << "Error: " << ack.code() << ", " << ack.message() << endl;
			} else if (reply->get_type() == RET_VIEW_MASTER_CONFIG_SUCCESS_MSG) {
                                ServerConfigMsg config;
				ProtocolUtils::parse_proto(reply, &config);
				cout << config.config() << endl;
                        } else {
				cerr <<"Message type error: " << reply->get_type() << endl;
			}
			delete(reply);
		}
		return false;
	}
}
