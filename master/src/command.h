#ifndef JDMQ_MASTER_COMMAND_H_
#define JDMQ_MASTER_COMMAND_H_

#include <vector>
#include <string>

namespace jdmq
{
	typedef std::vector<std::string> arg_list;

	class Command
	{
	public:
 	        Command() {}
		virtual ~Command() {}
		virtual bool execute(void *ptr, arg_list args) = 0;
	};
}

#endif
