#include <iostream>
#include <string>
#include <google/protobuf/stubs/common.h>
#include <jdmq/jdmq.h>
#include "command_factory.h"
#include "master_command.h"

using namespace std;
using namespace jdmq;

int run_server(int argc, char **argv)
{
	if (argc < 3) {
		cerr << "Usage: " << argv[0] << " server config_file" << endl;
		return 2;
	}
	JDMQ_LOGGER_INIT(Logger::DEBUG);
	master_t & master = master_t::get_instance();
	if (!master.init(argv[2]))
		return 1;
	master.serve();
	JDMQ_LOGGER_FINALIZE();
	return 0;
}

int run_shell(int argc, char **argv)
{
	if (argc < 4) {
		cerr << "Usage: " << argv[0] << " shell host port" << endl;
		return 2;
	}
	JDMQ_LOGGER_INIT(Logger::WARNING);
	client_t client;
	client.set_request_timeout(2);
	client.init();
	if (!client.add_node(1, "master", argv[2], argv[3]))
		return 1;
	client.fork_and_serve();

	CommandFactory cf("master_shell");
	cf.register_command("add-group", new AddGroupCommand());
	cf.register_command("remove-group", new RemoveGroupCommand());
	cf.register_command("add-connect-to", new AddConnectToCommand());
	cf.register_command("remove-connect-to", new RemoveConnectToCommand());
	cf.register_command("view-group", new ViewGroupCommand());
	cf.register_command("view-master", new ViewMasterCommand());
	cf.register_command("view-master-config", new ViewMasterConfigCommand());	

	cf.run(&client);
	
	client.stop();
	client.join();
	JDMQ_LOGGER_FINALIZE();
        return 0;
}

int main(int argc, char *argv[])
{
	if (argc < 2) {
		cerr << "Usage: " << argv[0] << " server|shell" << endl;
		return 2;
	}
	string task = argv[1];
	int status;
	if (task == "server") {
		status = run_server(argc, argv);
	} else if (task == "shell") {
		status = run_shell(argc, argv);
	} else {
		cerr << "Usage: " << argv[0] << " server|shell" << endl;
		return 2;
	}
	google::protobuf::ShutdownProtobufLibrary();
	return status;
}
