#include <iostream>
#include <google/protobuf/stubs/common.h>
#include <jdmq/jdmq.h>

using namespace std;
using namespace jdmq;

int main(int argc, char *argv[])
{
	if (argc < 2) {
		cerr << "Usage: " << argv[0] << " config_file" << endl;
		return 2;
	}
	JDMQ_LOGGER_INIT(Logger::DEBUG);
	Node & node = Node::get_instance();
	if (!node.init(argv[1]))
		return 1;
	node.serve();
	JDMQ_LOGGER_FINALIZE();
	google::protobuf::ShutdownProtobufLibrary();	
}
