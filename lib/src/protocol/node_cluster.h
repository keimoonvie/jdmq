#ifndef JDMQ_PROTOCOL_NODE_CLUSTER_H_
#define JDMQ_PROTOCOL_NODE_CLUSTER_H_

#include <map>
#include "../common/common.h"
#include "../common/scoped_lock.h"
#include "../mq/mq.h"
#include "node_ring.h"

namespace jdmq
{
	typedef std::map<std::string, node_ring_ptr> node_cluster_map_t;

	class NodeCluster
	{
	public:
		NodeCluster(int virtual_node_number = 1, int rand_factor = 314);
		virtual ~NodeCluster();

		inline void set_virtual_node_number(int virtual_node_number)
		{
			this->virtual_node_number = virtual_node_number;
		}

		inline void set_rand_factor(int rand_factor)
		{
			this->rand_factor = rand_factor;
		}

		bool add_node(node_instance_ptr node_instance);
		bool del_node(node_instance_ptr node_instance);
		node_ring_ptr get_node_ring(const std::string & group_name);
		node_instance_ptr get_node(const std::string & group_name, const std::string & key);

	private:
		node_cluster_map_t node_cluster_map;
		int virtual_node_number;
		int rand_factor;
		mutex_t mutex;
	};

	typedef NodeCluster node_cluster_t;
}

#endif
