#ifndef JDMQ_PROTOCOL_UTILS_H_
#define JDMQ_PROTOCOL_UTILS_H_

#include <string>
#include "../common/common.h"
#include "../mq/mq.h"
#include "protobuf/jdmq.pb.h"
#include "node_instance.h"

namespace jdmq
{
	class ProtocolUtils
	{
	public:
		static bool parse_proto(message_t *message, google::protobuf::Message *pbmessage);
		static int send_data(socket_ptr socket, const std::string & data, int type, int flags = 0, uint64_t message_id = 0);
		static int send_proto(socket_ptr socket, google::protobuf::Message *pbmessage, int type, int flags = 0, uint64_t message_id = 0);
		static int send_ack(socket_ptr socket, int code, const std::string & message, int type, int flags = 0, uint64_t message_id = 0);
	};
}

#endif
