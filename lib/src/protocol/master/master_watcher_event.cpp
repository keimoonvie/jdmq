#include "../../mq/watcher.h"
#include "../../mq/socket.h"
#include "master.h"
#include "master_watcher_event.h"

namespace jdmq
{
	void MasterWatcherEvent::on_accept(socket_ptr socket)
	{
        }

        void MasterWatcherEvent::on_close(socket_ptr socket)
	{
		Master & master = Master::get_instance();
		mutex_t & mutex = master.get_mutex();
		jdmq_synchronize(mutex);
		master.del_node(socket);
	}
}
