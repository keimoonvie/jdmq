#include <string.h>
#include <errno.h>
#include "../../common/utilities.h"
#include "../../common/logger.h"
#include "../../mq/watcher.h"
#include "../constants.h"
#include "management_service.h"
#include "master_service.h"
#include "master_watcher_event.h"
#include "master.h"

using namespace std;
using namespace libconfig;

namespace jdmq
{
	Master::Master(): caught_signal(false)
	{
		watcher = new watcher_t();
		nodes.set_deleted_key(-1);
		groups.set_deleted_key("");
		socket_nodes.set_deleted_key(0);
	}

	Master::Master(const Master & rhs)
        {
	}

	Master::~Master()
        {
                if (watcher != NULL)
			delete watcher;
	}

	const Master & Master::operator=(const Master & master)
	{
        }

	Master & Master::get_instance()
        {
                static Master instance;
                return instance;
        }

	bool Master::init(const char *config_file)
	{
		try {
                        config.readFile(config_file);
                } catch (const FileIOException &fioex) {
                        JDMQ_LOGGER_ERROR("config", "I/O error while reading configuration file: %s", config_file);
                        return false;
                } catch (const ParseException &pex) {
                        JDMQ_LOGGER_ERROR("config", "Parse error at %s:%d - %s", pex.getFile(), pex.getLine(), pex.getError());
			return false;
                }

		//Read interfaces
                const Setting & interface_settings = config.getRoot()["interfaces"];
                if (interface_settings.getLength() < 1) {
			JDMQ_LOGGER_ERROR("config", "No interface found");
			return false;
                }
		for (int i = 0; i < interface_settings.getLength(); i++) {
			const Setting & interface_setting = interface_settings[i];
			string host, port;
			if (!interface_setting.lookupValue("host", host) || !interface_setting.lookupValue("port", port)) {
				JDMQ_LOGGER_ERROR("config", "Lookup interface fail at item %d", i);
				return false;
			}
			watcher->add_interface(host, port);
		}

		//Read worker size
		int worker_size;
		if (config.lookupValue("master.io_worker_size", worker_size))
			watcher->create_io_workers(worker_size);
		if (config.lookupValue("master.process_worker_size", worker_size))
			watcher->create_process_workers(worker_size);
		if (config.lookupValue("master.cron_worker_size", worker_size))
                        watcher->create_cron_workers(worker_size);
		if (config.lookupValue("master.spare_worker_size", worker_size))
                        watcher->create_spare_workers(worker_size);

		//Buffer/pipe size
		int buffer_size;
		if (config.lookupValue("master.buffer_size", buffer_size))
			watcher->set_socket_buffer_size(buffer_size);
		int pipe_size;
		if (config.lookupValue("master.pipe_size", pipe_size))
			watcher->set_socket_pipe_size(pipe_size);

		//Read groups
		const Setting & group_settings = config.getRoot()["groups"];
		for (int i = 0; i < group_settings.getLength(); i++) {
			const Setting & group_setting = group_settings[i];
			string name;
			bool self_connected;
                        if (!group_setting.lookupValue("name", name) || !group_setting.lookupValue("self_connected", self_connected)) {
                                JDMQ_LOGGER_ERROR("config", "Lookup group fail at item %d", i);
                                return false;
                        }
                        node_group_ptr node_group(new node_group_t(name, self_connected));
                        const Setting & connect_to_settings = group_setting["connect_to"];
                        for (int j = 0; j < connect_to_settings.getLength(); j++) {
                                node_group->get_connect_to().insert(connect_to_settings[j]);
                        }
                        groups[name] = node_group;
                }

		for (node_group_map_t::iterator it = groups.begin(); it != groups.end(); it++) {
                        node_group_ptr node_group = it->second;
                        const set<string> & connect_to = node_group->get_connect_to();
                        set<string> ignore_group;
                        for (set<string>::const_iterator it2 = connect_to.begin(); it2 != connect_to.end(); it2++) {
                                string group_to = *it2;
                                if (groups.find(group_to) != groups.end()) {
                                        groups[group_to]->get_connect_from().insert(node_group->get_name());
                                } else {
                                        ignore_group.insert(group_to);
                                }
                        }

			for (set<string>::iterator it2 = ignore_group.begin(); it2 != ignore_group.end(); it2++)
                                node_group->get_connect_to().erase(*it2);
                }

		//Init watcher
                return watcher->init();
	}

	void master_interrupt_cb(int sig)
        {
		Master::get_instance().handle_interrupt(sig);
        }

	void master_abnormal_cb(int sig)
        {
		Master::get_instance().handle_segfault(sig);
        }

	void Master::serve()
	{
		//Add service
		watcher->add_service(new ManagementService());
		watcher->add_service(new MasterService());

		//Add event watcher
		watcher->set_watcher_event(new MasterWatcherEvent());

		//Add signal handler.
                //Hook SIGTERM and SIGINT
                if (!Utilities::register_interrupt_cb(master_interrupt_cb)) {
                        JDMQ_LOGGER_ERROR("master", "Register interrupt handler error: %d (%s)", errno, strerror(errno));
                        return;
                }
                //Hook SIGSEGV and SIGABRT
                if (!Utilities::register_abnormal_cb(master_abnormal_cb)) {
                        JDMQ_LOGGER_ERROR("master", "Register abnormal handler error: %d (%s)", errno, strerror(errno));
                        return;
                }

		JDMQ_LOGGER_INFO("master", "Begin serving");
                watcher->serve();
	}

	void Master::stop()
	{
		JDMQ_LOGGER_INFO("master", "Master stopping");
                watcher->finalize();
	}

	node_group_ptr Master::get_node_group(const string & name)
	{
		jdmq_synchronize(mutex);
		if (groups.find(name) != groups.end())
			return groups[name];
		else
			return node_group_ptr();
	}

	int Master::add_node(node_instance_ptr new_node, socket_ptr socket)
	{
		if (groups.find(new_node->get_info().group()) == groups.end())
			return NOT_FOUND_ERROR;
		if (nodes.find(new_node->get_info().node_id()) != nodes.end())
			return KEY_FOUND_ERROR;
		new_node->connect(socket);
		nodes[new_node->get_info().node_id()] = new_node;
		groups[new_node->get_info().group()]->get_nodes()[new_node->get_info().node_id()] = new_node;
		socket_nodes[socket.get()] = new_node;
		JDMQ_LOGGER_INFO("master", "New node: %s", new_node->to_string().c_str());
		return NO_ERROR;
	}

	int Master::active_node(socket_ptr socket)
	{
		if (socket_nodes.find(socket.get()) == socket_nodes.end())
			return NOT_FOUND_ERROR;
		node_instance_ptr node = socket_nodes[socket.get()];
		if (node->get_info().state() != NODE_ALIVE)
			return KEY_FOUND_ERROR;
		node->active();
		JDMQ_LOGGER_INFO("master", "Node active: %s", node->to_string().c_str());
		return NO_ERROR;
	}

	void Master::del_node(socket_ptr socket)
	{
		if (socket_nodes.find(socket.get()) == socket_nodes.end())
			return;
		node_instance_ptr node = socket_nodes[socket.get()];
		JDMQ_LOGGER_INFO("master", "Node down: %s", node->to_string().c_str());
		if (nodes.find(node->get_info().node_id()) != nodes.end())
			nodes.erase(node->get_info().node_id());
		if (groups.find(node->get_info().group()) != groups.end())
			if (groups[node->get_info().group()]->get_nodes().find(node->get_info().node_id()) != groups[node->get_info().group()]->get_nodes().end())
				groups[node->get_info().group()]->get_nodes().erase(node->get_info().node_id());
		socket_nodes.erase(socket.get());
	}

	node_instance_ptr Master::get_node(socket_ptr socket)
	{
		if (socket_nodes.find(socket.get()) == socket_nodes.end())
                        return node_instance_ptr();
		else
			return socket_nodes[socket.get()];
	}

	void Master::get_connect_to(node_instance_ptr node_instance, node_instance_map_t & connect_to)
	{
		const set<string> & connect_to_group = groups[node_instance->get_info().group()]->get_connect_to();
                for (set<string>::const_iterator it = connect_to_group.begin(); it != connect_to_group.end(); it++) {
                        string group_name = *it;
                        if (groups.find(group_name) != groups.end()) {
				for (node_instance_map_t::iterator it2 = groups[group_name]->get_nodes().begin(); it2 != groups[group_name]->get_nodes().end(); it2++) {
					if (it2->second->get_info().state() == NODE_ACTIVE)
						connect_to[it2->second->get_info().node_id()] = it2->second;
                                }
                        }
		}

		if (groups[node_instance->get_info().group()]->is_self_connected()) {
			for (node_instance_map_t::iterator it2 = groups[node_instance->get_info().group()]->get_nodes().begin(); 
			     it2 != groups[node_instance->get_info().group()]->get_nodes().end(); it2++) {
				if (node_instance->get_info().node_id() != it2->second->get_info().node_id() &&
				    it2->second->get_info().state() == NODE_ACTIVE)
					connect_to[it2->second->get_info().node_id()] = it2->second;
			}
		}
        }

	void Master::get_connect_from(node_instance_ptr node_instance, node_instance_map_t & connect_from)
	{
		const set<string> & connect_from_group = groups[node_instance->get_info().group()]->get_connect_from();
                for (set<string>::const_iterator it = connect_from_group.begin(); it != connect_from_group.end(); it++) {
                        string group_name = *it;
                        if (groups.find(group_name) != groups.end()) {
				for (node_instance_map_t::iterator it2 = groups[group_name]->get_nodes().begin(); it2 != groups[group_name]->get_nodes().end(); it2++) {
                                        if (it2->second->get_info().state() >= NODE_ALIVE)
                                                connect_from[it2->second->get_info().node_id()] = it2->second;
                                }
                        }
                }
        }

	void Master::handle_interrupt(int sig)
	{
		if (!caught_signal) {
			caught_signal = true;
			JDMQ_LOGGER_INFO("master", "Interrupt signal caught: %d", sig);
			stop();
		}
	}

	void Master::handle_segfault(int sig)
	{
		if (!caught_signal) {
			caught_signal = true;
			JDMQ_LOGGER_INFO("master", "Abnormal signal caught: %d", sig);
			vector<string> stack_traces = Utilities::get_stack_traces(50);
			for (int i = 0; i < stack_traces.size(); i++)
				JDMQ_LOGGER_ERROR("stack", "%s", stack_traces[i].c_str());
			stop();
		}
	}
}
