#include "../../common/logger.h"
#include "../../mq/message.h"
#include "../../mq/socket.h"
#include "../constants.h"
#include "../protocol_utils.h"
#include "../protobuf/jdmq.pb.h"
#include "master.h"
#include "master_service.h"

using namespace std;

namespace jdmq
{
	bool MasterService::process(message_t *message, socket_ptr socket)
	{
		switch (message->get_type()) {
                case REGISTER_NODE_MSG:
			process_register_node(message, socket);
			return true;
		case ACTIVE_NODE_MSG:
                        process_active_node(message, socket);
                        return true;
		case REBUILD_NODE_MSG:
			process_rebuild_node(message, socket);
                        return true;
		default:
			return false;
		}
	}

	void MasterService::process_register_node(message_t *message, socket_ptr socket)
	{
		NodeInfoMessage msg;
		if (!ProtocolUtils::parse_proto(message, &msg)) {
			ProtocolUtils::send_ack(socket, PARSE_ERROR, "PARSE_ERROR", RET_REGISTER_NODE_ERROR_MSG, message->get_flags(), message->get_id());
                        return;
		}
		node_instance_ptr new_node(new node_instance_t(msg.node_id(), msg.group(), msg.host(), msg.port()));
		Master & master = Master::get_instance();
		mutex_t & mutex = master.get_mutex();
		jdmq_synchronize(mutex);
		int code = master.add_node(new_node, socket);
		if (code != NO_ERROR) {
			ProtocolUtils::send_ack(socket, code, "", RET_REGISTER_NODE_ERROR_MSG, message->get_flags(), message->get_id());
			return;
		}
		//Notify connect to
		node_instance_map_t connect_to;
		master.get_connect_to(new_node, connect_to);
		NodeListMessage ret_msg;
		for (node_instance_map_t::iterator it = connect_to.begin(); it != connect_to.end(); it++) {
			notify_node_changed(new_node, it->second->get_socket(), NOTIFY_NODE_ALIVE_MSG);
			NodeInfoMessage *node_info = ret_msg.add_node();
			*node_info = it->second->get_info();
		}

		//Return result
		ProtocolUtils::send_proto(socket, &ret_msg, RET_REGISTER_NODE_SUCCESS_MSG, message->get_flags(), message->get_id());
	}

	void MasterService::process_active_node(message_t *message, socket_ptr socket)
	{
		Master & master = Master::get_instance();
                mutex_t & mutex = master.get_mutex();
                jdmq_synchronize(mutex);
		int code = master.active_node(socket);
		if (code != NO_ERROR) {
			ProtocolUtils::send_ack(socket, code, "", RET_ACTIVE_NODE_ERROR_MSG, message->get_flags(), message->get_id());
                        return;
		}
		node_instance_ptr node = master.get_node(socket);
		node_instance_map_t connect_from;
		master.get_connect_from(node, connect_from);

		//Return result
		NodeListMessage ret_msg;
		for (node_instance_map_t::iterator it = connect_from.begin(); it != connect_from.end(); it++) {
			NodeInfoMessage *node_info = ret_msg.add_node();
			*node_info = it->second->get_info();
		}
		ProtocolUtils::send_proto(socket, &ret_msg, RET_ACTIVE_NODE_SUCCESS_MSG, message->get_flags(), message->get_id());

		//Notify connect from
		for (node_instance_map_t::iterator it = connect_from.begin(); it != connect_from.end(); it++)
			notify_node_changed(node, it->second->get_socket(), NOTIFY_NODE_ALIVE_TO_CONNECT_MSG);
	}

	void MasterService::process_rebuild_node(message_t *message, socket_ptr socket)
        {
		NodeInfoMessage msg;
		if (!ProtocolUtils::parse_proto(message, &msg)) {
			ProtocolUtils::send_ack(socket, PARSE_ERROR, "PARSE_ERROR", RET_REBUILD_NODE_ERROR_MSG, message->get_flags(), message->get_id());
                        return;
                }
		Master & master = Master::get_instance();
		mutex_t & mutex = master.get_mutex();
		jdmq_synchronize(mutex);
		node_instance_ptr new_node(new node_instance_t(msg.node_id(), msg.group(), msg.host(), msg.port()));
		int code = master.add_node(new_node, socket);
		if (code != NO_ERROR) {
			ProtocolUtils::send_ack(socket, code, "", RET_REBUILD_NODE_ERROR_MSG, message->get_flags(), message->get_id());
                        return;
		}
		if (msg.state() == NODE_ACTIVE) {
			code = master.active_node(socket);
			if (code != NO_ERROR) {
				ProtocolUtils::send_ack(socket, code, "", RET_REBUILD_NODE_ERROR_MSG, message->get_flags(), message->get_id());
				return;
			}
		}
		ProtocolUtils::send_ack(socket, NO_ERROR, "NO_ERROR", RET_REBUILD_NODE_SUCCESS_MSG, message->get_flags(), message->get_id());
	}

	void MasterService::notify_node_changed(node_instance_ptr node_instance, socket_ptr socket, int type)
	{
		string data = node_instance->serialize();
		message_t message(data.data(), data.size(), type);
		message.set_flag(7);
		if (socket)
			socket->send(&message);
	}
}
