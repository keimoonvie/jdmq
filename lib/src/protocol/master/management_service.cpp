#include <sstream>
#include "../../common/logger.h"
#include "../../mq/message.h"
#include "../../mq/socket.h"
#include "../constants.h"
#include "../protocol_utils.h"
#include "../protobuf/jdmq.pb.h"
#include "master.h"
#include "management_service.h"

using namespace std;

namespace jdmq
{
	bool ManagementService::process(message_t *message, socket_ptr socket)
	{
		switch (message->get_type()) {
		case ADD_GROUP_MSG:
                        process_add_group(message, socket);
			return true;
		case REMOVE_GROUP_MSG:
                        process_remove_group(message, socket);
                        return true;
		case ADD_CONNECT_TO_MSG:
                        process_add_connect_to(message, socket);
                        return true;
		case REMOVE_CONNECT_TO_MSG:
                        process_remove_connect_to(message, socket);
                        return true;
		case VIEW_GROUP_MSG:
			process_view_group(message, socket);
			return true;
		case VIEW_MASTER_MSG:
			process_view_master(message, socket);
			return true;
		case VIEW_MASTER_CONFIG_MSG:
                        process_view_master_config(message, socket);
                        return true;
		default:
			return false;
		}
	}

	void ManagementService::process_add_group(message_t *message, socket_ptr socket)
        {
		AddGroupMessage msg;
                if (!ProtocolUtils::parse_proto(message, &msg)) {
			ProtocolUtils::send_ack(socket, PARSE_ERROR, "PARSE_ERROR", RET_ADD_GROUP_ERROR_MSG, message->get_flags(), message->get_id());
                        return;
                }
		mutex_t & mutex = Master::get_instance().get_mutex();
                jdmq_synchronize(mutex);
		node_group_map_t & groups = Master::get_instance().get_groups();
		if (groups.find(msg.name()) != groups.end()) {
			ProtocolUtils::send_ack(socket, KEY_FOUND_ERROR, "KEY_FOUND_ERROR", RET_ADD_GROUP_ERROR_MSG, message->get_flags(), message->get_id());
                        return;
		}
		node_group_ptr new_group(new node_group_t(msg.name(), msg.self_connected()));
		for (int i = 0; i < msg.connect_to_size(); i++) {
			if (groups.find(msg.connect_to(i)) != groups.end()) {
				new_group->get_connect_to().insert(msg.connect_to(i));
				groups[msg.connect_to(i)]->get_connect_from().insert(msg.name());
			}
		}

		for (int i = 0; i < msg.connect_from_size(); i++) {
			if (groups.find(msg.connect_from(i)) != groups.end()) {
				new_group->get_connect_from().insert(msg.connect_from(i));
				groups[msg.connect_from(i)]->get_connect_to().insert(msg.name());
			}
		}
		groups[msg.name()] = new_group;
		ProtocolUtils::send_ack(socket, NO_ERROR, "NO_ERROR", RET_ADD_GROUP_SUCCESS_MSG, message->get_flags(), message->get_id());
	}

	void ManagementService::process_remove_group(message_t *message, socket_ptr socket)
	{
		RemoveGroupMessage msg;
                if (!ProtocolUtils::parse_proto(message, &msg)) {
			ProtocolUtils::send_ack(socket, PARSE_ERROR, "PARSE_ERROR", RET_REMOVE_GROUP_ERROR_MSG, message->get_flags(), message->get_id());
			return;
		}
                mutex_t & mutex = Master::get_instance().get_mutex();
                jdmq_synchronize(mutex);
                node_group_map_t & groups = Master::get_instance().get_groups();
		if (groups.find(msg.name()) == groups.end()) {
			ProtocolUtils::send_ack(socket, NOT_FOUND_ERROR, "NOT_FOUND_ERROR", RET_REMOVE_GROUP_ERROR_MSG, message->get_flags(), message->get_id());
			return;
                }
		node_group_ptr group = groups[msg.name()];
		if (group->get_nodes().size() > 0) {
			ProtocolUtils::send_ack(socket, NOT_EMPTY_ERROR, "NOT_EMPTY_ERROR", RET_REMOVE_GROUP_ERROR_MSG, message->get_flags(), message->get_id());
			return;
		}
		groups.erase(msg.name());
		for (set<string>::iterator it = group->get_connect_to().begin(); it != group->get_connect_to().end(); it++)
			if (groups.find(*it) != groups.end())
				groups[*it]->get_connect_from().erase(msg.name());
		for (set<string>::iterator it = group->get_connect_from().begin(); it != group->get_connect_from().end(); it++)
			if (groups.find(*it) != groups.end())
				groups[*it]->get_connect_to().erase(msg.name());
		ProtocolUtils::send_ack(socket, NO_ERROR, "NO_ERROR", RET_REMOVE_GROUP_SUCCESS_MSG, message->get_flags(), message->get_id());
	}

	void ManagementService::process_add_connect_to(message_t *message, socket_ptr socket)
	{
		AddConnectToMessage msg;
                if (!ProtocolUtils::parse_proto(message, &msg)) {
			ProtocolUtils::send_ack(socket, PARSE_ERROR, "PARSE_ERROR", RET_ADD_CONNECT_TO_ERROR_MSG, message->get_flags(), message->get_id());
                        return;
                }
		mutex_t & mutex = Master::get_instance().get_mutex();
                jdmq_synchronize(mutex);
                node_group_map_t & groups = Master::get_instance().get_groups();
		if (groups.find(msg.name()) == groups.end()) {
			ProtocolUtils::send_ack(socket, NOT_FOUND_ERROR, "NOT_FOUND_ERROR", RET_ADD_CONNECT_TO_ERROR_MSG, message->get_flags(), message->get_id());
			return;
                }
		node_group_ptr group = groups[msg.name()];
		for (int i = 0; i < msg.connect_to_size(); i++) {
                        if (groups.find(msg.connect_to(i)) != groups.end()) {
				group->get_connect_to().insert(msg.connect_to(i));
				groups[msg.connect_to(i)]->get_connect_from().insert(msg.name());
			}
		}
		ProtocolUtils::send_ack(socket, NO_ERROR, "NO_ERROR", RET_ADD_CONNECT_TO_SUCCESS_MSG, message->get_flags(), message->get_id());
	}

	void ManagementService::process_remove_connect_to(message_t *message, socket_ptr socket)
	{
		RemoveConnectToMessage msg;
                if (!ProtocolUtils::parse_proto(message, &msg)) {
			ProtocolUtils::send_ack(socket, PARSE_ERROR, "PARSE_ERROR", RET_REMOVE_CONNECT_TO_ERROR_MSG, message->get_flags(), message->get_id());
                        return;
                }
                mutex_t & mutex = Master::get_instance().get_mutex();
                jdmq_synchronize(mutex);
                node_group_map_t & groups = Master::get_instance().get_groups();
                if (groups.find(msg.name()) == groups.end()) {
			ProtocolUtils::send_ack(socket, NOT_FOUND_ERROR, "NOT_FOUND_ERROR", RET_REMOVE_CONNECT_TO_ERROR_MSG, message->get_flags(), message->get_id());
                        return;
                }
		node_group_ptr group = groups[msg.name()];
		for (int i = 0; i < msg.connect_to_size(); i++) {
                        if (group->get_connect_to().find(msg.connect_to(i)) != group->get_connect_to().end() && 
			    groups.find(msg.connect_to(i)) != groups.end()) {
				group->get_connect_to().erase(msg.connect_to(i));
				groups[msg.connect_to(i)]->get_connect_from().erase(msg.name());
			}
		}
		ProtocolUtils::send_ack(socket, NO_ERROR, "NO_ERROR", RET_REMOVE_CONNECT_TO_SUCCESS_MSG, message->get_flags(), message->get_id());
	}

	void ManagementService::process_view_group(message_t *message, socket_ptr socket)
	{
		ViewGroupMessage msg;
		if (!ProtocolUtils::parse_proto(message, &msg)) {
			ProtocolUtils::send_ack(socket, PARSE_ERROR, "PARSE_ERROR", RET_VIEW_GROUP_ERROR_MSG, message->get_flags(), message->get_id());
			return;
		}
		mutex_t & mutex = Master::get_instance().get_mutex();
		jdmq_synchronize(mutex);
		node_group_ptr group = Master::get_instance().get_node_group(msg.name());
		if (group == NULL) {
			ProtocolUtils::send_ack(socket, NOT_FOUND_ERROR, "NOT_FOUND_ERROR", RET_VIEW_GROUP_ERROR_MSG, message->get_flags(), message->get_id());
			return;
		}
		ProtocolUtils::send_data(socket, group->serialize(), RET_VIEW_GROUP_SUCCESS_MSG, message->get_flags(), message->get_id());
	}

	void ManagementService::process_view_master(message_t *message, socket_ptr socket)
	{
		mutex_t & mutex = Master::get_instance().get_mutex();
                jdmq_synchronize(mutex);
		node_group_map_t & groups = Master::get_instance().get_groups();
		ServerInfoMsg ret_msg;
		for (node_group_map_t::iterator it = groups.begin(); it != groups.end(); it++) {
			NodeGroupMessage *group_info = ret_msg.add_group();
			*group_info = it->second->serialize_to_proto();
		}
		ProtocolUtils::send_proto(socket, &ret_msg, RET_VIEW_MASTER_SUCCESS_MSG, message->get_flags(), message->get_id());
	}

	void ManagementService::process_view_master_config(message_t *message, socket_ptr socket)
        {
		mutex_t & mutex = Master::get_instance().get_mutex();
                jdmq_synchronize(mutex);
                node_group_map_t & groups = Master::get_instance().get_groups();
		stringstream ss;
		ss << "groups:" << endl << "(" << endl;
		int i = 0;
		for (node_group_map_t::iterator it = groups.begin(); it != groups.end(); it++) {
			i++;
			ss << "\t{" << endl;
			ss << "\t\tname: \"" << it->second->get_name() << "\";" << endl;
			ss << "\t\tself_connected: " << (it->second->is_self_connected() ? "true" : "false") << ";" << endl;
			ss << "\t\tconnect_to: (";
			int j = 0;
			set<string> & connect_to = it->second->get_connect_to();
			for (set<string>::iterator it2 = connect_to.begin(); it2 != connect_to.end(); it2++) {
				j++;
				ss << "\"" << *it2 << "\"";
				if (j != connect_to.size())
					ss << ",";
			}
			ss << ");" << endl;
			ss << "\t}";
			if (i != groups.size())
				ss << "," << endl;
			else
				ss << endl;
		}
		ss << ");" << endl;
		ServerConfigMsg ret_msg;
		ret_msg.set_config(ss.str());
		ProtocolUtils::send_proto(socket, &ret_msg, RET_VIEW_MASTER_CONFIG_SUCCESS_MSG, message->get_flags(), message->get_id());
	}
}
