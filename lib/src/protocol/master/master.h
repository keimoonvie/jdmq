#ifndef JDMQ_PROTOCOL_MASTER_H_
#define JDMQ_PROTOCOL_MASTER_H_

#include <libconfig.h++>
#include "../../common/common.h"
#include "../../common/scoped_lock.h"
#include "../../mq/mq.h"
#include "../node_group.h"

namespace jdmq
{
	class Master
	{
	public:
		static Master & get_instance();
		virtual ~Master();

		bool init(const char *config_file);
		void serve();
		void stop();

		void handle_interrupt(int sig);
		void handle_segfault(int sig);

		node_group_ptr get_node_group(const std::string & name);
		int add_node(node_instance_ptr node_instance, socket_ptr socket);
		int active_node(socket_ptr socket);		
		void del_node(socket_ptr socket);
		node_instance_ptr get_node(socket_ptr socket);

		void get_connect_to(node_instance_ptr node_instance, node_instance_map_t & connect_to);
                void get_connect_from(node_instance_ptr node_instance, node_instance_map_t & connect_from);

		inline watcher_t * get_watcher()
		{
			return watcher;
		}

		inline node_group_map_t & get_groups()
		{
			return groups;
		}

		inline mutex_t & get_mutex()
		{
			return mutex;
		}

	private:
		Master();
		
		Master(const Master & rhs);
		const Master & operator=(const Master & rhs);

                watcher_t *watcher;
		libconfig::Config config;

		node_instance_map_t nodes;
		node_group_map_t groups;
		socket_node_map_t socket_nodes;

		mutex_t mutex;
		bool caught_signal;
	};

	typedef Master master_t;
}

#endif
