#ifndef JDMQ_PROTOCOL_MASTER_SERVICE_H_
#define JDMQ_PROTOCOL_MASTER_SERVICE_H_

#include "../../common/common.h"
#include "../../mq/mq.h"
#include "../../mq/service.h"
#include "../node_instance.h"

namespace jdmq
{
	class MasterService: public service_t
        {
        public:
                bool process(message_t *message, socket_ptr socket);
	private:
		void process_register_node(message_t *message, socket_ptr socket);
		void process_active_node(message_t *message, socket_ptr socket);
		void process_rebuild_node(message_t *message, socket_ptr socket);

		void notify_node_changed(node_instance_ptr node_instance, socket_ptr socket, int type);
	};
}

#endif
