#ifndef JDMQ_PROTOCOL_MANAGEMENT_SERVICE_H_
#define JDMQ_PROTOCOL_MANAGEMENT_SERVICE_H_

#include "../../common/common.h"
#include "../../mq/mq.h"
#include "../../mq/service.h"

namespace jdmq
{
	class ManagementService: public service_t
	{
	public:
		bool process(message_t *message, socket_ptr socket);
	private:
		void process_add_group(message_t *message, socket_ptr socket);
		void process_remove_group(message_t *message, socket_ptr socket);
		void process_add_connect_to(message_t *message, socket_ptr socket);
		void process_remove_connect_to(message_t *message, socket_ptr socket);

		void process_view_group(message_t *message, socket_ptr socket);
		void process_view_master(message_t *message, socket_ptr socket);
		void process_view_master_config(message_t *message, socket_ptr socket);
	};
}

#endif
