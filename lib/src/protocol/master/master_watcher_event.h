#ifndef JDMQ_PROTOCOL_MASTER_WATCHER_EVENT_H_
#define JDMQ_PROTOCOL_MASTER_WATCHER_EVENT_H_

#include "../../common/common.h"
#include "../../mq/watcher_event.h"

namespace jdmq
{
	class MasterWatcherEvent: public watcher_event_t
	{
        public:
		void on_accept(socket_ptr socket);
		void on_close(socket_ptr socket);
	};
}

#endif
