#include "../mq/message.h"
#include "../mq/socket.h"
#include "protocol_utils.h"

using namespace std;

namespace jdmq {
	bool ProtocolUtils::parse_proto(message_t *message, google::protobuf::Message *pbmessage)
	{
		return pbmessage->ParseFromArray(message->get_content(), message->get_content_size());
	}

	int ProtocolUtils::send_data(socket_ptr socket, const string & data, int type, int flags, uint64_t message_id)
	{
                message_t message(data.data(), data.size(), type, message_id);
		message.set_flags(flags);
                return socket->send(&message);
        }

	int ProtocolUtils::send_proto(socket_ptr socket, google::protobuf::Message *pbmessage, int type, int flags, uint64_t message_id)
	{
		string data = pbmessage->SerializeAsString();
		return send_data(socket, data, type, flags, message_id);
	}

	int ProtocolUtils::send_ack(socket_ptr socket, int code, const string & message, int type, int flags, uint64_t message_id)
	{
		ACKMessage ack;
		ack.set_code(code);
		ack.set_message(message);
		return send_proto(socket, &ack, type, flags, message_id);
	}
}
