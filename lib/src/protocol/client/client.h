#ifndef JDMQ_PROTOCOL_CLIENT_H_
#define JDMQ_PROTOCOL_CLIENT_H_

#include <stdint.h>
#include "../../common/common.h"
#include "../../common/thread.h"
#include "../../common/scoped_lock.h"
#include "../../mq/mq.h"
#include "../node_cluster.h"

namespace jdmq
{
	class Client
	{
	public:
		Client();
		virtual ~Client();

		bool init();
		void serve();
		bool fork_and_serve();
		void join();
		void stop();

		bool send_async(const std::string & group_name, message_t *message);
		bool broadcast(const std::string & group_name, message_t *message);
		bool send_sync(const std::string & group_name, message_t *message, message_t **reply);
		
		bool add_node(int id, const std::string & group_name, const std::string & host, const std::string & port);
		node_instance_ptr get_node_from_socket(socket_ptr socket);
		void del_node(node_instance_ptr node_info);
		void down_node(socket_ptr socket);
		void reconnect_node();

		inline watcher_t * get_watcher()
		{
			return watcher;
		}

		inline node_cluster_t & get_node_cluster()
		{
			return node_cluster;
		}

		inline void set_request_timeout(int request_timeout)
		{
			this->request_timeout = request_timeout;
		}

		inline void set_auto_connect(bool auto_connect)
		{
			this->auto_connect = auto_connect;
		}
	private:
		watcher_t *watcher;
		thread_t *main_loop_thread;

		node_instance_map_t node_instance_map;
		node_cluster_t node_cluster;
		socket_node_map_t socket_node_map;
		mutex_t node_mutex;

		uint64_t current_id;
                mutex_t id_mutex;

		int request_timeout;
		bool auto_connect;

		bool connect_to_node(node_instance_ptr node_instance);
	};
	typedef Client client_t;

	class ClientMainLoopRunner: public runnable_t
        {
        public:
                ClientMainLoopRunner(client_t *client);
                void run();
        private:
                client_t *client;
        };
}

#endif
