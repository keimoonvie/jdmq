#ifndef JDMQ_PROTOCOL_CLIENT_TIMER_H_
#define JDMQ_PROTOCOL_CLIENT_TIMER_H_

#include "../../common/common.h"
#include "../../common/timer.h"

#define JDMQ_CLIENT_TIMER_HEARTBEAT 2000
#define JDMQ_CLIENT_TIMER_KICK_ITER 15

namespace jdmq
{
	class Client;
	class ClientTimer: public timer_t
	{
        public:
		ClientTimer(long pedioric);
		void run();

		void set_client(Client *client);
	private:
		int times;
                Client *client;
	};
}

#endif
