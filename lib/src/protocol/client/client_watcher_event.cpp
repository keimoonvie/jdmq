#include "../../mq/watcher.h"
#include "../../mq/socket.h"
#include "client.h"
#include "client_watcher_event.h"

namespace jdmq
{
	void ClientWatcherEvent::set_client(Client *client)
	{
		this->client = client;
	}

	void ClientWatcherEvent::on_accept(socket_ptr socket)
	{
        }

        void ClientWatcherEvent::on_close(socket_ptr socket)
	{
		client->down_node(socket);
        }
}
