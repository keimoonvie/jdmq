#include "client.h"
#include "../context_manager.h"
#include "client_timer.h"

namespace jdmq
{
	ClientTimer::ClientTimer(long pedioric): timer_t(pedioric), times(0)
        {
        }

        void ClientTimer::set_client(Client *client)
        {
		this->client = client;
	}

	void ClientTimer::run()
	{
		times++;
		client->reconnect_node();
		if (times % JDMQ_CLIENT_TIMER_KICK_ITER == 0)
			ContextManager::get_instance().destroy_contexts();
	}
}
