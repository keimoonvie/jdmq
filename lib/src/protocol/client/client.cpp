#include <string.h>
#include <errno.h>
#include "../../common/logger.h"
#include "../../common/utilities.h"
#include "../../common/socket_utils.h"
#include "../../mq/message.h"
#include "../../mq/watcher.h"
#include "../../mq/socket.h"
#include "../sync_service.h"
#include "client_timer.h"
#include "client_watcher_event.h"
#include "client.h"

using namespace std;


namespace jdmq
{
	Client::Client()
	{
		watcher = new watcher_t();
		main_loop_thread = NULL;
		current_id = 0;
		request_timeout = 10;
		auto_connect = true;		
		node_instance_map.set_deleted_key(0);
		socket_node_map.set_deleted_key(NULL);
	}

	Client::~Client()
	{
		if (watcher)
			delete watcher;
		if (main_loop_thread)
			delete main_loop_thread;
	}

	bool Client::init()
	{
		watcher->add_spare_service(new SyncService());

		ClientTimer *client_timer = new ClientTimer(JDMQ_CLIENT_TIMER_HEARTBEAT);
		client_timer->set_client(this);
		watcher->add_timer(client_timer);

		ClientWatcherEvent *watcher_event = new ClientWatcherEvent();
		watcher_event->set_client(this);
		watcher->set_watcher_event(watcher_event);
		return watcher->init();
	}

	void Client::serve()
	{
		watcher->serve();
	}

	bool Client::fork_and_serve()
	{
		runnable_ptr main_loop_runner(new ClientMainLoopRunner(this));
                main_loop_thread = new thread_t(main_loop_runner, "mainloop");
                main_loop_thread->start();
                while (true) {
                        if (watcher->is_running())
                                break;
                        usleep(1000);
                }
                return true;
	}

	void Client::join()
	{
		main_loop_thread->join();
	}

	void Client::stop()
	{
		watcher->finalize();
	}

	bool Client::add_node(int id, const string & group_name, const string & host, const string & port)
	{
		node_instance_ptr node_instance(new node_instance_t(id, group_name, host, port));
                int times = 0;
                int max_try = 10;
                while (true) {
                        times++;
                        if (times > max_try)
				return false;
                        if (connect_to_node(node_instance))
				break;
			sleep(1);
		}
		jdmq_synchronize(node_mutex);
		node_instance_map[node_instance->get_info().node_id()] = node_instance;
		if (node_cluster.add_node(node_instance)) {
			socket_node_map[node_instance->get_socket().get()] = node_instance;
			return true;
		} else {
			return false;
		}
	}

	node_instance_ptr Client::get_node_from_socket(socket_ptr socket)
	{
		jdmq_synchronize(node_mutex);
		if (socket_node_map.find(socket.get()) == socket_node_map.end())
			return node_instance_ptr();
		else
                        return socket_node_map[socket.get()];
	}

	void Client::del_node(node_instance_ptr node_instance)
	{
		jdmq_synchronize(node_mutex);
                if (node_instance_map.find(node_instance->get_info().node_id()) != node_instance_map.end())
                        node_instance_map.erase(node_instance->get_info().node_id());
		node_cluster.del_node(node_instance);
		if (socket_node_map.find(node_instance->get_socket().get()) != socket_node_map.end())
                        socket_node_map.erase(node_instance->get_socket().get());
	}

	void Client::down_node(socket_ptr socket)
	{
		jdmq_synchronize(node_mutex);
                if (socket_node_map.find(socket.get()) == socket_node_map.end())
                        return;
                node_instance_ptr node_instance = socket_node_map[socket.get()];
                JDMQ_LOGGER_INFO("client", "Node down: %s", node_instance->to_string().c_str());
		socket_node_map.erase(socket.get());
		node_instance->disconnect();
                node_cluster.del_node(node_instance);
	}

	void Client::reconnect_node()
	{
		if (!auto_connect)
                        return;
                jdmq_synchronize(node_mutex);
                for (node_instance_map_t::iterator it = node_instance_map.begin(); it != node_instance_map.end(); it++) {
                        node_instance_ptr node_instance = it->second;
                        if (node_instance->get_info().state() == NODE_DEATH) {
                                if (connect_to_node(node_instance)) {
                                        if (node_cluster.add_node(node_instance)) {
                                                socket_node_map[node_instance->get_socket().get()] = node_instance;
                                                JDMQ_LOGGER_INFO("client", "Reconnect to node %s success", node_instance->to_string().c_str());
                                        } else {
                                                watcher->close(node_instance->get_socket());
                                                node_instance->disconnect();
                                                JDMQ_LOGGER_INFO("client", "Reconnect to node %s fail", node_instance->to_string().c_str());
                                        }
                                } else {
                                        JDMQ_LOGGER_INFO("client", "Reconnect to node %s fail", node_instance->to_string().c_str());
                                }
                        }
                }
	}

	bool Client::send_async(const string & group_name, message_t *message)
	{
		uint64_t message_id = 0;
		{
			jdmq_synchronize(id_mutex);
			message_id = ++current_id;
		}
		message->set_id(message_id);
		string key = Utilities::make_string(message_id);
		node_instance_ptr node_instance = node_cluster.get_node(group_name, key);
		if (node_instance == NULL)
			return false;
		else
			return node_instance->send_async(message);
	}

	bool Client::broadcast(const string & group_name, message_t *message)
	{
		node_ring_ptr node_ring = node_cluster.get_node_ring(group_name);
		if (node_ring == NULL)
			return false;
		node_ring->broadcast(message);
		return true;
	}

	bool Client::send_sync(const string & group_name, message_t *message, message_t **reply)
	{
		uint64_t message_id = 0;
                {
                        jdmq_synchronize(id_mutex);
                        message_id = ++current_id;
		}
                message->set_id(message_id);
		string key = Utilities::make_string(message_id);
                node_instance_ptr node_instance = node_cluster.get_node(group_name, key);
		if (node_instance == NULL)
			return false;
		else
			return node_instance->send_sync(message, request_timeout, reply);
	}

	bool Client::connect_to_node(node_instance_ptr node_instance)
	{
		if (node_instance->get_info().state() == NODE_ALIVE)
                        return true;

                const string & host = node_instance->get_info().host();
		const string & port = node_instance->get_info().port();
		int fd = SocketUtils::socket_new_connection(host.c_str(), port.c_str());
		if (fd == -1) {
			JDMQ_LOGGER_ERROR("client", "Connect fail: %s, errno: %d (%s)", node_instance->to_string().c_str(), errno, strerror(errno));
			return false;
                } else {
			if (!SocketUtils::socket_make_non_blocking(fd)) {
				JDMQ_LOGGER_ERROR("client", "Cannot make socket non blocking: %d", fd);
				::close(fd);
				return false;
			}
			if (!SocketUtils::socket_make_no_delay(fd)) {
				JDMQ_LOGGER_ERROR("client", "Cannot make socket no delay: %d", fd);
				::close(fd);
				return false;
			}
			if (!watcher->watch(fd, host, port)) {
				JDMQ_LOGGER_ERROR("client", "Cannot watch socket %d", fd);
				::close(fd);
				return false;
			} else {
				socket_ptr socket = watcher->get_socket(fd);
				node_instance->connect(socket);
				return true;
			}
			
		}
	}

	ClientMainLoopRunner::ClientMainLoopRunner(client_t *client): runnable_t(), client(client)
	{
	}

	void ClientMainLoopRunner::run()
	{
		client->serve();
	}
}
