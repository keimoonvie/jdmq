#include <sstream>
#include "protobuf/jdmq.pb.h"
#include "node_group.h"

using namespace std;

namespace jdmq
{
	NodeGroup::NodeGroup(const string & name, bool self_connected): name(name), self_connected(self_connected)
	{
		nodes.set_deleted_key(-1);
	}

	NodeGroupMessage NodeGroup::serialize_to_proto() const
	{
		NodeGroupMessage msg;
		msg.set_name(name);
                msg.set_self_connected(self_connected);
		for (set<string>::iterator it = connect_to.begin(); it != connect_to.end(); it++)
                        msg.add_connect_to(*it);
                for (set<string>::iterator it = connect_from.begin(); it != connect_from.end(); it++)
			msg.add_connect_from(*it);
		for (node_instance_map_t::const_iterator it = nodes.begin(); it != nodes.end(); it++) {
			NodeInfoMessage * new_node = msg.add_node();
			*new_node = it->second->get_info();
                }
		return msg;
	}
	
	string NodeGroup::serialize() const
	{
		NodeGroupMessage msg = serialize_to_proto();
		return msg.SerializeAsString();
	}

	string NodeGroup::to_string() const
	{
		stringstream ss;
		ss << "{" << name << ", " << self_connected << ", [";
		for (set<string>::iterator it = connect_from.begin(); it != connect_from.end(); it++)
			ss << *it << ", ";
		ss << "], [";
		for (set<string>::iterator it = connect_to.begin(); it != connect_to.end();it++)
			ss << *it << ", ";
		ss << "], [";
		for (node_instance_map_t::const_iterator it = nodes.begin(); it != nodes.end(); it++)
			ss << it->second->to_string() << ", ";
		ss << "]}";
		return ss.str();
	}
}

