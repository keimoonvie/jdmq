#ifndef JDMQ_PROTOCOL_NODE_RING_H_
#define JDMQ_PROTOCOL_NODE_RING_H_

#include <stdint.h>
#include <string>
#include <map>
#include "../common/common.h"
#include "../common/scoped_lock.h"
#include "../common/shared_ptr.h"
#include "../mq/mq.h"
#include "node_instance.h"

namespace jdmq
{
	typedef uint16_t KeyType;
	typedef std::map<KeyType, node_instance_ptr> node_ring_map_t;

	class NodeRing
	{
	public:
		NodeRing(int virtual_node_number = 1, int rand_factor = 314);
		virtual ~NodeRing();

		bool add_node(node_instance_ptr node_instance);
		bool del_node(node_instance_ptr node_instance);
		node_instance_ptr get_node(const std::string & key);

		void broadcast(message_t *msg);
	private:
		static const KeyType MAX_KEY = (1 << (sizeof(KeyType) * 8)) - 1;
		node_ring_map_t node_ring_map;
		int virtual_node_number;
		int rand_factor;
		mutex_t mutex;

		uint32_t node_hash(node_instance_ptr node_instance);
		uint32_t hash_func(const std::string & key);
	};

	typedef NodeRing node_ring_t;
	typedef pointer_t<NodeRing>::shared node_ring_ptr;
}

#endif
