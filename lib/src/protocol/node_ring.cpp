#include <boost/lexical_cast.hpp>
#include "../common/logger.h"
#include "../mq/message.h"
#include "murmur_hash3.h"
#include "node_ring.h"

using namespace std;

namespace jdmq
{
	NodeRing::NodeRing(int virtual_node_number, int rand_factor): virtual_node_number(virtual_node_number), rand_factor(rand_factor)
	{
	}

	NodeRing::~NodeRing()
	{
	}

	bool NodeRing::add_node(node_instance_ptr node_instance)
	{
		jdmq_synchronize(mutex);
		uint32_t node_hash_value = node_hash(node_instance);
		int node_added = 0;
                for (uint16_t i = 1; i <= virtual_node_number; i++) {
			uint64_t virtual_node_hash_value = (uint64_t) (node_hash_value * rand_factor * i);
			KeyType pos = virtual_node_hash_value % MAX_KEY;
			if (node_ring_map.find(pos) != node_ring_map.end()) {
				JDMQ_LOGGER_ERROR("node-ring", "Hash collision at %d", i);
			} else {
				node_added++;
				JDMQ_LOGGER_DEBUG("node-ring", "Added node %d/%d - %s at pos %d", i, virtual_node_number, node_instance->to_string().c_str(), pos);
				node_ring_map[pos] = node_instance;
			}
		}
		return node_added > 0;
	}

	bool NodeRing::del_node(node_instance_ptr node_instance)
	{
		jdmq_synchronize(mutex);
		uint32_t node_hash_value = node_hash(node_instance);
		for (uint16_t i = 1; i <= virtual_node_number; ++i) {
			uint64_t virtual_node_hash_value = (uint64_t) (node_hash_value * rand_factor * i);
                        KeyType pos = virtual_node_hash_value % MAX_KEY;
			if (node_ring_map.find(pos) != node_ring_map.end()) {
				node_ring_map.erase(pos);
				JDMQ_LOGGER_DEBUG("node-ring", "Erased node %d/%d - %s", i, virtual_node_number, node_instance->to_string().c_str());
			} else {
				JDMQ_LOGGER_ERROR("node-ring", "Cannot find node at position %d", pos);
                        }
		}
		return true;
	}

	node_instance_ptr NodeRing::get_node(const string & key)
	{
		jdmq_synchronize(mutex);
		uint32_t node_hash_value = hash_func(key);
		KeyType pos = node_hash_value % MAX_KEY;
		if (node_ring_map.size() == 0)
			return node_instance_ptr();
		if (node_ring_map.count(pos) != 0) {
			return node_ring_map[pos];
		} else {
			node_ring_map_t::const_iterator iter = node_ring_map.upper_bound(pos);
			if (iter == node_ring_map.end())
				return node_ring_map.begin()->second;
			else
				return iter->second;
			}
	}

	void NodeRing::broadcast(message_t *msg)
	{
		jdmq_synchronize(mutex);
		for (node_ring_map_t::iterator it = node_ring_map.begin(); it != node_ring_map.end(); it++)
			it->second->send_async(msg);
	}

	uint32_t NodeRing::node_hash(node_instance_ptr node_instance)
	{
		string iden = node_instance->get_info().host() + ":" + node_instance->get_info().port() + "/" + boost::lexical_cast<string>(node_instance->get_info().node_id());
		return hash_func(iden);
	}

	uint32_t NodeRing::hash_func(const string & key)
	{
		uint32_t val;
		MurmurHash3_x86_32(key.data(), key.size(), 0, &val);
		return val;
	}
}
