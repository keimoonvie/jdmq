#include <errno.h>
#include <string.h>
#include "../../common/logger.h"
#include "../../common/utilities.h"
#include "../../common/socket_utils.h"
#include "../../mq/watcher.h"
#include "../../mq/socket.h"
#include "../../mq/message.h"
#include "../constants.h"
#include "../protocol_utils.h"
#include "../sync_service.h"
#include "node_spare_service.h"
#include "node_watcher_event.h"
#include "node_timer.h"
#include "node.h"

using namespace std;
using namespace libconfig;

namespace jdmq
{
	Node::Node()
        {
		watcher = new watcher_t();
		current_id = 0;
		caught_signal = false;
		master_alive = false;
		ready = false;
		node_event = new NullNodeEvent();
		nodes.set_deleted_key(0);
		socket_nodes.set_deleted_key(NULL);
        }

	Node::Node(const Node & rhs)
        {
        }

        const Node & Node::operator=(const Node & rhs)
        {
        }

	Node & Node::get_instance()
	{
		static Node instance;
                return instance;
        }

	Node::~Node()
	{
		if (watcher)
			delete watcher;
		if (main_loop_thread)
			delete main_loop_thread;
		if (node_event)
			delete node_event;
        }

	bool Node::init(const char *config_file)
	{
		try {
                        config.readFile(config_file);
                } catch (const FileIOException &fioex) {
                        JDMQ_LOGGER_ERROR("config", "I/O error while reading configuration file: %s", config_file);
			return false;
                } catch (const ParseException &pex) {
                        JDMQ_LOGGER_ERROR("config", "Parse error at %s:%d - %s", pex.getFile(), pex.getLine(), pex.getError());
                        return false;
                }

		//Node info
		{
			const Setting & node_setting = config.getRoot()["node"];
			int id;
			string host, port, group;
			if (!node_setting.lookupValue("id", id) || !node_setting.lookupValue("host", host) || !node_setting.lookupValue("port", port) ||
			    !node_setting.lookupValue("group", group)) {
				JDMQ_LOGGER_ERROR("node", "Read node setting failed");
				return false;
			}
			me.reset(new node_instance_t(id, group, host, port));

			//Read worker size
			int worker_size;
			if (node_setting.lookupValue("io_worker_size", worker_size))
				watcher->create_io_workers(worker_size);
			if (node_setting.lookupValue("process_worker_size", worker_size))
				watcher->create_process_workers(worker_size);
			if (node_setting.lookupValue("cron_worker_size", worker_size))
				watcher->create_cron_workers(worker_size);
			if (node_setting.lookupValue("spare_worker_size", worker_size))
				watcher->create_spare_workers(worker_size);

			//Buffer/pipe size
			int buffer_size;
			if (node_setting.lookupValue("buffer_size", buffer_size))
				watcher->set_socket_buffer_size(buffer_size);
			int pipe_size;
			if (node_setting.lookupValue("pipe_size", pipe_size))
                        watcher->set_socket_pipe_size(pipe_size);
		}

		//Master
		{
			const Setting & master_setting = config.getRoot()["masters"];
			if (!master_setting.lookupValue("host", master_interface.host) || !master_setting.lookupValue("port", master_interface.port)) {
				JDMQ_LOGGER_INFO("node", "Use master: false");
				use_master = false;
			} else {
				use_master = true;
			}
		}

		//Groups
		{
			const Setting & group_settings = config.getRoot()["required_groups"];
			{
				for (int i = 0; i < group_settings.getLength(); i++)
					required_groups.insert(group_settings[i]);
			}
		}

		//Interfaces
		{
			const Setting & interface_settings = config.getRoot()["interfaces"];
			{
				string host, port;
				bool timeout;
				for (int i = 0; i < interface_settings.getLength(); i++) {
					const Setting & interface_setting = interface_settings[i];
					if (!interface_setting.lookupValue("host", host) || !interface_setting.lookupValue("port", port)) {
						JDMQ_LOGGER_ERROR("config", "Lookup interface fail at item %d", i);
						return false;
					}
					if (!interface_setting.lookupValue("timeout", timeout))
						timeout = false;
					struct host_port_t interface = {host, port, timeout};
					interfaces.push_back(interface);
				}
			}
		}

		//Add default services, timers and watcher event.
		watcher->add_spare_service(new SyncService());
		watcher->add_spare_service(new NodeSpareService());
		watcher->set_watcher_event(new NodeWatcherEvent());
		watcher->add_timer(new NodeTimer(JDMQ_NODE_TIMER_HEARTBEAT));

		//Init watcher;
		return watcher->init();
	}

	void Node::serve()
	{
		if (!register_signal_handler())
			return;
		if (use_master && !connect_master())
			return;
		runnable_ptr main_loop_runner(new NodeMainLoopRunner());
		main_loop_thread = new thread_t(main_loop_runner, "mainloop");
		if (!main_loop_thread->start())
			return;
		while (true) {
			if (watcher->is_running())
				break;
			usleep(1000);
		}

		if (use_master) {
			//Send register
			if (!send_register()) {
				JDMQ_LOGGER_ERROR("node", "Send register fail");
                                stop();
			}
		} else {
			//Bind
			bind();
		}

		main_loop_thread->join();
	}

	void Node::fork_and_serve()
	{
		if (use_master && !connect_master())
                        return;
                runnable_ptr main_loop_runner(new NodeMainLoopRunner());
		main_loop_thread = new thread_t(main_loop_runner, "mainloop");
		if (!main_loop_thread->start())
                        return;
		while (true) {
			if (watcher->is_running())
				break;
			usleep(1000);
		}
		if (use_master) {
			//Send register
			if (!send_register()) {
                                JDMQ_LOGGER_ERROR("node", "Send register fail");
                                stop();
                        }
		} else {
			//Bind
			bind();
		}
	}

	void Node::join()
	{
		main_loop_thread->join();
	}

	void Node::stop()
	{
		JDMQ_LOGGER_INFO("node", "Node stopping");
		watcher->finalize();
	}

	void Node::set_node_event(node_event_t *node_event)
	{
		if (this->node_event)
			delete this->node_event;
		this->node_event = node_event;
	}

	node_instance_ptr Node::get_node(uint64_t node_id)
	{
		if (node_id == 0)
			return node_instance_ptr();
		jdmq_synchronize(node_mutex);
		if (nodes.find(node_id) != nodes.end())
			return nodes[node_id];
		else
			return node_instance_ptr();
	}

	node_instance_ptr Node::get_node(socket_ptr socket)
	{
		if (socket.get() == NULL)
			return node_instance_ptr();
		jdmq_synchronize(node_mutex);
		if (socket_nodes.find(socket.get()) != socket_nodes.end())
			return socket_nodes[socket.get()];
		else
			return node_instance_ptr();
	}

	bool Node::send_async(const string & group_name, message_t *message)
	{
		uint64_t message_id = 0;
		{
                        jdmq_synchronize(id_mutex);
                        message_id = ++current_id;
                }
		message->set_id(message_id);
                string key = Utilities::make_string(message_id);
                node_instance_ptr node_instance = node_cluster.get_node(group_name, key);
                if (node_instance == NULL)
                        return false;
                else
			return node_instance->send_async(message);
	}

	bool Node::send_async(const string & group_name, const string & key, message_t *message)
        {
                uint64_t message_id = 0;
                {
                        jdmq_synchronize(id_mutex);
                        message_id = ++current_id;
                }
                message->set_id(message_id);
                node_instance_ptr node_instance = node_cluster.get_node(group_name, key);
                if (node_instance == NULL)
                        return false;
                else
                        return node_instance->send_async(message);
	}

	bool Node::broadcast(const string & group_name, message_t *message)
	{
		node_ring_ptr node_ring = node_cluster.get_node_ring(group_name);
		if (node_ring == NULL)
                        return false;
                node_ring->broadcast(message);
                return true;
	}

	bool Node::send_sync(const string & group_name, message_t *message, message_t **reply)
	{
		uint64_t message_id = 0;
		{
			jdmq_synchronize(id_mutex);
                        message_id = ++current_id;
                }
                message->set_id(message_id);
		string key = Utilities::make_string(message_id);
                node_instance_ptr node_instance = node_cluster.get_node(group_name, key);
                if (node_instance == NULL)
                        return false;
                else
                        return node_instance->send_sync(message, request_timeout, reply);
	}

	bool Node::send_sync(const string & group_name, const string & key, message_t *message, message_t **reply)
        {
                uint64_t message_id = 0;
                {
                        jdmq_synchronize(id_mutex);
                        message_id = ++current_id;
                }
                message->set_id(message_id);
                node_instance_ptr node_instance = node_cluster.get_node(group_name, key);
                if (node_instance == NULL)
                        return false;
                else
                        return node_instance->send_sync(message, request_timeout, reply);
        }

	bool Node::is_master_socket(socket_ptr socket)
	{
		return use_master && socket.get() == master_socket.get();
	}

	void Node::master_down()
	{
		if (use_master) {
			JDMQ_LOGGER_DEBUG("node", "Master down");
			jdmq_synchronize(master_mutex);
			master_alive = false;
			master_socket.reset();
		}
	}

	void Node::reconnect_master()
	{
		if (use_master) {
			jdmq_synchronize(master_mutex);
			if (!master_alive) {
				if (connect_master()) {
					string data = me->serialize();
					message_t message(data.data(), data.size(), REBUILD_NODE_MSG);
					message.set_flag(6);
					master_socket->send(&message, true);
				}
			}
		}
	}

	void Node::add_node(node_instance_ptr new_node)
	{
		if (nodes.find(new_node->get_info().node_id()) != nodes.end())
			return;
		int fd = SocketUtils::socket_new_connection(new_node->get_info().host().c_str(), new_node->get_info().port().c_str());
                if (fd == -1) {
                        JDMQ_LOGGER_ERROR("node", "Cannot connect to node: %s", new_node->to_string().c_str());
                        return;
                }
                if (!SocketUtils::socket_make_non_blocking(fd)) {
                        JDMQ_LOGGER_ERROR("node", "Cannot make socket non blocking: %d", fd);
			::close(fd);
			return;
		}
                if (!SocketUtils::socket_make_no_delay(fd)) {
			JDMQ_LOGGER_ERROR("node", "Cannot make socket no delay: %d", fd);
			::close(fd);
			return;
		}
                if (!watcher->watch(fd, master_interface.host, master_interface.port)) {
			JDMQ_LOGGER_ERROR("node", "Cannot watch socket %d", fd);
			::close(fd);
			return;
		} else {
			socket_ptr socket = watcher->get_socket(fd);
			new_node->connect(socket);
			nodes[new_node->get_info().node_id()] = new_node;
			socket_nodes[socket.get()] = new_node;

			send_greeting(socket);
			JDMQ_LOGGER_INFO("node", "Connect success to node %s", new_node->to_string().c_str());
		}
	}

	void Node::send_greeting(socket_ptr socket)
	{
		string data = me->serialize();
		message_t message(data.data(), data.size(), NODE_GREETING_MSG);
		message.set_flag(7);
		socket->send(&message);
	}

	void Node::register_node(node_instance_ptr new_node)
	{
		if (nodes.find(new_node->get_info().node_id()) != nodes.end())
                        return;
		nodes[new_node->get_info().node_id()] = new_node;
	}

	int Node::assign_node(uint64_t node_id, socket_ptr socket)
	{
		if (nodes.find(node_id) == nodes.end())
			return NOT_FOUND_ERROR;
		node_instance_ptr node_instance = nodes[node_id];
		node_instance->connect(socket);
		node_instance->active();
		socket_nodes[socket.get()] = node_instance;
		node_cluster.add_node(node_instance);
		node_event->on_node_connected(node_instance);
		JDMQ_LOGGER_INFO("node", "Greeting success with %s", node_instance->to_string().c_str());
		return NO_ERROR;
	}

	void Node::assign_node(socket_ptr socket)
	{
		if (socket_nodes.find(socket.get()) == socket_nodes.end())
			return;
		node_instance_ptr node_instance = socket_nodes[socket.get()];
		node_instance->active();
		node_cluster.add_node(node_instance);
		node_event->on_node_connected(node_instance);
		JDMQ_LOGGER_INFO("node", "Greeting success with %s", node_instance->to_string().c_str());
	}

	void Node::del_node(socket_ptr socket)
	{
		node_instance_ptr node = socket_nodes[socket.get()];
		socket_nodes.erase(socket.get());
		if (nodes.find(node->get_info().node_id()) != nodes.end())
			nodes.erase(node->get_info().node_id());
		node_cluster.del_node(node);
		node->disconnect();
		JDMQ_LOGGER_INFO("node", "Node death: %s", node->to_string().c_str());
		node_event->on_node_disconnected(node);
	}

	void Node::check_ready()
	{
		if (ready)
			return;
		for (node_instance_map_t::iterator it = nodes.begin(); it != nodes.end(); it++) {
			if (it->second->get_info().state() == NODE_ACTIVE && required_groups.find(it->second->get_info().group()) != required_groups.end())
                                required_groups.erase(it->second->get_info().group());
		}
                if (required_groups.size() == 0) {
			ready = true;
			bind();			
		}
	}

	void node_interrupt_cb(int sig)
        {
		Node::get_instance().handle_interrupt(sig);
	}

	void node_abnormal_cb(int sig)
        {
		Node::get_instance().handle_segfault(sig);
        }

	bool Node::register_signal_handler()
	{
		//Hook SIGTERM and SIGINT
                if (!Utilities::register_interrupt_cb(node_interrupt_cb)) {
                        JDMQ_LOGGER_ERROR("node", "Register interrupt handler error: %d (%s)", errno, strerror(errno));
                        return false;
                }
                //Hook SIGSEGV and SIGABRT
		if (!Utilities::register_abnormal_cb(node_abnormal_cb)) {
                        JDMQ_LOGGER_ERROR("node", "Register abnormal handler error: %d (%s)", errno, strerror(errno));
                        return false;
                }
		return true;
	}

	void Node::handle_interrupt(int sig)
	{
		if (!caught_signal) {
			caught_signal = true;
			JDMQ_LOGGER_INFO("node", "Interrupt signal caught: %d", sig);
			stop();
		}
	}

	void Node::handle_segfault(int sig)
	{
		if (!caught_signal) {
			caught_signal = true;
			JDMQ_LOGGER_INFO("master", "Abnormal signal caught: %d", sig);
			vector<string> stack_traces = Utilities::get_stack_traces(50);
			for (int i = 0; i < stack_traces.size(); i++)
                                JDMQ_LOGGER_ERROR("stack", "%s", stack_traces[i].c_str());
			stop();
		}
	}

	bool Node::connect_master()
	{
		int fd = SocketUtils::socket_new_connection(master_interface.host.c_str(), master_interface.port.c_str());
		if (fd == -1) {
			JDMQ_LOGGER_ERROR("node", "Cannot connect to master: %s:%s", master_interface.host.c_str(), master_interface.port.c_str());
			return false;
                }
		if (!SocketUtils::socket_make_non_blocking(fd)) {
			JDMQ_LOGGER_ERROR("node", "Cannot make socket non blocking: %d", fd);
			::close(fd);
			return false;
		}
		if (!SocketUtils::socket_make_no_delay(fd)) {
			JDMQ_LOGGER_ERROR("node", "Cannot make socket no delay: %d", fd);
			::close(fd);
			return false;
		}
		if (!watcher->watch(fd, master_interface.host, master_interface.port)) {
			JDMQ_LOGGER_ERROR("node", "Cannot watch socket %d", fd);
			::close(fd);
			return false;
		} else {
			master_socket = watcher->get_socket(fd);
			master_alive = true;
			JDMQ_LOGGER_INFO("node", "Connected to master: %s", master_socket->to_string().c_str());
			return true;
		}
	}

	bool Node::send_register()
	{
		string data = me->serialize();
		message_t message(data.data(), data.size(), REGISTER_NODE_MSG);
		message.set_flag(6);
		return master_socket->send(&message, true) > 0;
	}

	void Node::bind()
	{
		for (int i = 0; i < interfaces.size(); i++)
			watcher->add_interface(interfaces[i].host, interfaces[i].port, interfaces[i].timeout);
		if (interfaces.size() > 0 && use_master) {
			string data = me->serialize();
			message_t message(data.data(), data.size(), ACTIVE_NODE_MSG);
			message.set_flag(6);
			master_socket->send(&message, true);
		}
	}

	void NodeMainLoopRunner::run()
	{
                JDMQ_LOGGER_INFO("node", "Begin serving");
		Node::get_instance().get_watcher()->serve();
	}
}
