#include "../../common/logger.h"
#include "../../mq/message.h"
#include "../../mq/socket.h"
#include "../constants.h"
#include "../protocol_utils.h"
#include "../protobuf/jdmq.pb.h"
#include "node.h"
#include "node_spare_service.h"

namespace jdmq
{
	bool NodeSpareService::process(message_t *message, socket_ptr socket)
	{
		switch (message->get_type()) {
		case RET_REGISTER_NODE_SUCCESS_MSG:
		case RET_REGISTER_NODE_ERROR_MSG:
			process_ret_register_node(message, socket);
			return true;
		case RET_ACTIVE_NODE_SUCCESS_MSG:
		case RET_ACTIVE_NODE_ERROR_MSG:
			process_ret_active_node(message, socket);
			return true;
		case NOTIFY_NODE_ALIVE_MSG:
			process_notify_node_alive(message, socket);
			return true;
		case NOTIFY_NODE_ALIVE_TO_CONNECT_MSG:
                        process_notify_node_alive_to_connect(message, socket);
			return true;
		case NODE_GREETING_MSG:
                        process_node_greeting(message, socket);
			return true;
		case RET_NODE_GREETING_SUCCESS_MSG:
                case RET_NODE_GREETING_ERROR_MSG:
			process_ret_node_greeting(message, socket);
                        return true;
		case RET_REBUILD_NODE_SUCCESS_MSG:
		case RET_REBUILD_NODE_ERROR_MSG:
			process_ret_rebuild_node(message, socket);
			return true;
		default:
                        return false;
		}
	}

	void NodeSpareService::process_ret_register_node(message_t *message, socket_ptr socket)
        {
		Node & node = Node::get_instance();
		if (message->get_type() == RET_REGISTER_NODE_ERROR_MSG) {
			ACKMessage ack;
			ProtocolUtils::parse_proto(message, &ack);
			JDMQ_LOGGER_ERROR("node-spare-service", "Register fail: %d", ack.code());
                        node.stop();
			return;
                }
		NodeListMessage ret_msg;
		if (!ProtocolUtils::parse_proto(message, &ret_msg)) {
			JDMQ_LOGGER_ERROR("node-spare-service", "Parse register result fail");
                        node.stop();
			return;
		}
		JDMQ_LOGGER_INFO("node-spare-service", "Register success");
		mutex_t & mutex = node.get_node_mutex();
		jdmq_synchronize(mutex);
		for (int i = 0; i < ret_msg.node_size(); i++) {
			node_instance_ptr new_node = node_instance_ptr(new node_instance_t(ret_msg.node(i).node_id(),
											   ret_msg.node(i).group(),
											   ret_msg.node(i).host(),
											   ret_msg.node(i).port()));
			node.add_node(new_node);
		}
		node.get_me()->get_info().set_state(NODE_ALIVE);
		node.check_ready();
	}

	void NodeSpareService::process_ret_active_node(message_t *message, socket_ptr socket)
	{
		Node & node = Node::get_instance();
                if (message->get_type() == RET_ACTIVE_NODE_ERROR_MSG) {
			ACKMessage ack;
			ProtocolUtils::parse_proto(message, &ack);
                        JDMQ_LOGGER_ERROR("node-spare-service", "Active fail: %d", ack.code());
                        node.stop();
                        return;
                }
                NodeListMessage ret_msg;
                if (!ProtocolUtils::parse_proto(message, &ret_msg)) {
                        JDMQ_LOGGER_ERROR("node-spare-service", "Parse active result fail");
                        node.stop();
                        return;
                }
		JDMQ_LOGGER_INFO("node-spare-service", "Active success");
		mutex_t & mutex = node.get_node_mutex();
                jdmq_synchronize(mutex);
		for (int i = 0; i < ret_msg.node_size(); i++) {
			node_instance_ptr new_node = node_instance_ptr(new node_instance_t(ret_msg.node(i).node_id(),
                                                                                           ret_msg.node(i).group(),
                                                                                           ret_msg.node(i).host(),
											   ret_msg.node(i).port()));
			node.register_node(new_node);
		}
		node.get_me()->get_info().set_state(NODE_ACTIVE);
	}

	void NodeSpareService::process_notify_node_alive(message_t *message, socket_ptr socket)
	{
		NodeInfoMessage node_info;
		if (!ProtocolUtils::parse_proto(message, &node_info)) {
                        JDMQ_LOGGER_ERROR("node-spare-service", "Parse notify node alive fail");
                        return;
                }
		Node & node = Node::get_instance();
		mutex_t & mutex = node.get_node_mutex();
		jdmq_synchronize(mutex);
		node_instance_ptr new_node = node_instance_ptr(new node_instance_t(node_info.node_id(),
										   node_info.group(),
										   node_info.host(),
										   node_info.port()));
		node.register_node(new_node);
	}

	void NodeSpareService::process_notify_node_alive_to_connect(message_t *message, socket_ptr socket)
	{
		NodeInfoMessage node_info;
                if (!ProtocolUtils::parse_proto(message, &node_info)) {
                        JDMQ_LOGGER_ERROR("node-spare-service", "Parse notify node alive to connect fail");
                        return;
                }
                Node & node = Node::get_instance();
                mutex_t & mutex = node.get_node_mutex();
                jdmq_synchronize(mutex);
		node_instance_ptr new_node = node_instance_ptr(new node_instance_t(node_info.node_id(),
                                                                                   node_info.group(),
                                                                                   node_info.host(),
                                                                                   node_info.port()));
                node.add_node(new_node);
	}

	void NodeSpareService::process_node_greeting(message_t *message, socket_ptr socket)
	{
		NodeInfoMessage node_info;
                if (!ProtocolUtils::parse_proto(message, &node_info)) {
			JDMQ_LOGGER_ERROR("node-spare-service", "Parse notify node greeting fail");
			ProtocolUtils::send_ack(socket, PARSE_ERROR, "PARSE_ERROR", RET_NODE_GREETING_ERROR_MSG, message->get_flags(), message->get_id());
                        return;
                }
		Node & node = Node::get_instance();
		mutex_t & mutex = node.get_node_mutex();
		jdmq_synchronize(mutex);
		int code = node.assign_node(node_info.node_id(), socket);
		if (code == NO_ERROR)
			ProtocolUtils::send_ack(socket, NO_ERROR, "NO_ERROR", RET_NODE_GREETING_SUCCESS_MSG, message->get_flags(), message->get_id());
		else
			ProtocolUtils::send_ack(socket, code, "", RET_NODE_GREETING_ERROR_MSG, message->get_flags(), message->get_id());
	}

	void NodeSpareService::process_ret_node_greeting(message_t *message, socket_ptr socket)
	{
		Node & node = Node::get_instance();
		if (message->get_type() == RET_NODE_GREETING_ERROR_MSG) {
			ACKMessage ack;
			ProtocolUtils::parse_proto(message, &ack);
			if (ack.code() != NOT_FOUND_ERROR) {
				JDMQ_LOGGER_ERROR("node-spare-service", "Greeting fail");
				node.stop();
				return;
			} else {
				usleep(100000);
				node.send_greeting(socket);
				return;
			}
		}
		mutex_t & mutex = node.get_node_mutex();
                jdmq_synchronize(mutex);
		node.assign_node(socket);
		node.check_ready();
	}

	void NodeSpareService::process_ret_rebuild_node(message_t *message, socket_ptr socket)
        {
		if (message->get_type() == RET_REBUILD_NODE_SUCCESS_MSG)
			JDMQ_LOGGER_INFO("node-spare-service", "Rebuild node success");
		else
			JDMQ_LOGGER_ERROR("node-spare-service", "Rebuild node fail");
	}
}
