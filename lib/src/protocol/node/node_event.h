#ifndef JDMQ_PROTOCOL_NODE_EVENT_H_
#define JDMQ_PROTOCOL_NODE_EVENT_H_

#include "../../common/common.h"
#include "../../mq/mq.h"
#include "../node_instance.h"

namespace jdmq
{
	class NodeEvent
        {
        public:
                virtual void on_node_connected(node_instance_ptr node_instance) = 0;
                virtual void on_node_disconnected(node_instance_ptr node_instance) = 0;
		virtual void on_socket_connected(socket_ptr socket) = 0;
                virtual void on_socket_disconnected(socket_ptr socket) = 0;
        };

	typedef NodeEvent node_event_t;

        class NullNodeEvent: public NodeEvent
        {
        public:
                void on_node_connected(node_instance_ptr node_instance)
                {
		}

		void on_node_disconnected(node_instance_ptr node_instance)
                {
                }

		void on_socket_connected(socket_ptr socket)
                {
                }

                void on_socket_disconnected(socket_ptr socket)
                {
                }
        };
}

#endif
