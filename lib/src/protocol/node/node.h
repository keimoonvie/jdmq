#ifndef JDMQ_PROTOCOL_NODE_H_
#define JDMQ_PROTOCOL_NODE_H_

#include <vector>
#include <set>
#include <libconfig.h++>
#include "../../common/common.h"
#include "../../common/scoped_lock.h"
#include "../../common/thread.h"
#include "../../mq/mq.h"
#include "../node_cluster.h"
#include "node_event.h"

namespace jdmq
{
	class Node
	{
	public:
		static Node & get_instance();
                virtual ~Node();

                bool init(const char *config_file);
                void serve();
		void fork_and_serve();
		void join();
                void stop();

		void set_node_event(node_event_t *node_event);
		node_instance_ptr get_node(uint64_t node_id);
		node_instance_ptr get_node(socket_ptr socket);

		bool send_async(const std::string & group_name, message_t *message);
		bool send_async(const std::string & group_name, const std::string & key, message_t *message);
                bool broadcast(const std::string & group_name, message_t *message);
                bool send_sync(const std::string & group_name, message_t *message, message_t **reply);
		bool send_sync(const std::string & group_name, const std::string & key, message_t *message, message_t **reply);

		inline void set_request_timeout(int request_timeout)
                {
                        this->request_timeout = request_timeout;
                }

		inline watcher_t * get_watcher()
		{
			return watcher;
		}

		inline node_instance_ptr get_me()
		{
			return me;
		}

		//BEGIN INTERNAL API
		
		void add_node(node_instance_ptr node_instance);
		void send_greeting(socket_ptr socket);
		void register_node(node_instance_ptr node_instance);
		int assign_node(uint64_t node_id, socket_ptr socket);
		void assign_node(socket_ptr socket);
		void del_node(socket_ptr socket);
		void check_ready();

		bool is_master_socket(socket_ptr socket);
		void master_down();
		void reconnect_master();

		void handle_interrupt(int sig);
		void handle_segfault(int sig);

		inline node_cluster_t & get_node_cluster()
                {
			return node_cluster;
                }

		inline mutex_t & get_node_mutex()
		{
			return node_mutex;
		}

		inline node_instance_map_t & get_nodes()
		{
			return nodes;
		}

		inline socket_node_map_t & get_socket_nodes()
		{
			return socket_nodes;
		}

		inline node_event_t *get_node_event()
		{
			return node_event;
		}

	private:
		struct host_port_t
		{
			std::string host;
			std::string port;
			bool timeout;
		};

		Node();
                Node(const Node &rhs);
                const Node & operator=(const Node &rhs);

		libconfig::Config config;

		watcher_t *watcher;
		thread_t *main_loop_thread;
		node_event_t *node_event;

		node_instance_ptr me;
		node_instance_map_t nodes;
		node_cluster_t node_cluster;
		socket_node_map_t socket_nodes;
		mutex_t node_mutex;

		bool use_master;
		socket_ptr master_socket;
		bool master_alive;
		struct host_port_t master_interface;
		mutex_t master_mutex;

		std::vector<struct host_port_t> interfaces;
		std::set<std::string> required_groups;

		uint64_t current_id;
		mutex_t id_mutex;

		int request_timeout;
		bool caught_signal;
		bool ready;

		bool register_signal_handler();
		bool connect_master();
		bool send_register();
		void bind();
	};

	class NodeMainLoopRunner: public runnable_t
	{
        public:
		void run();
	};
}

#endif
