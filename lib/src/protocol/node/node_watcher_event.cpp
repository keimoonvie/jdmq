#include "../../mq/socket.h"
#include "node.h"
#include "node_watcher_event.h"

namespace jdmq
{
	void NodeWatcherEvent::on_accept(socket_ptr socket)
        {
		Node & node = Node::get_instance();
                node.get_node_event()->on_socket_connected(socket);
	}

	void NodeWatcherEvent::on_close(socket_ptr socket)
        {
		Node & node = Node::get_instance();
		if (node.is_master_socket(socket)) {
			node.master_down();
			return;
		}
		mutex_t & mutex = node.get_node_mutex();
		{
			jdmq_synchronize(mutex);
			socket_node_map_t & socket_nodes = node.get_socket_nodes();
			if (socket_nodes.find(socket.get()) != socket_nodes.end()) {
				node.del_node(socket);
				return;
			}
		}
		node.get_node_event()->on_socket_disconnected(socket);
	}
}
