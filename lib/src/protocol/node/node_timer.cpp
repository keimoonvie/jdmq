#include "node.h"
#include "../../mq/watcher.h"
#include "../context_manager.h"
#include "node_timer.h"

namespace jdmq
{
	NodeTimer::NodeTimer(long pedioric): timer_t(pedioric), times(0)
	{
        }

        void NodeTimer::run()
	{
		Node & node = Node::get_instance();
		times++;
		node.reconnect_master();
		//if (times % JDMQ_NODE_TIMER_KICK_ITER == 0) {
			ContextManager::get_instance().destroy_contexts();
			Node::get_instance().get_watcher()->destroy_idle();
			//}
	}
}
