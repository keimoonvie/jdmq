#ifndef JDMQ_PROTOCOL_NODE_SPARE_SERVICE_H_
#define JDMQ_PROTOCOL_NODE_SPARE_SERVICE_H_

#include "../../common/common.h"
#include "../../mq/service.h"
#include "../node_instance.h"

namespace jdmq
{
	class NodeSpareService: public Service
        {
        public:
                bool process(message_t* message, socket_ptr socket);
	private:
                void process_ret_register_node(message_t *message, socket_ptr socket);
		void process_ret_active_node(message_t *message, socket_ptr socket);
		void process_notify_node_alive(message_t *message, socket_ptr socket);
		void process_notify_node_alive_to_connect(message_t *message, socket_ptr socket);
		void process_node_greeting(message_t *message, socket_ptr socket);
		void process_ret_node_greeting(message_t *message, socket_ptr socket);
		void process_ret_rebuild_node(message_t *message, socket_ptr socket);
	};
}

#endif
