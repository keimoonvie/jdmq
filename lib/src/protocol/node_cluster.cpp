#include "../mq/message.h"
#include "node_cluster.h"

using namespace std;

namespace jdmq
{
	NodeCluster::NodeCluster(int virtual_node_number, int rand_factor): virtual_node_number(virtual_node_number), rand_factor(rand_factor)
	{
	}

	NodeCluster::~NodeCluster()
	{
	}

	bool NodeCluster::add_node(node_instance_ptr node_instance)
	{
		const string & group_name = node_instance->get_info().group();
		if (node_cluster_map.find(group_name) == node_cluster_map.end())
			node_cluster_map[group_name] = node_ring_ptr(new node_ring_t(virtual_node_number, rand_factor));
		return node_cluster_map[group_name]->add_node(node_instance);
	}

	bool NodeCluster::del_node(node_instance_ptr node_instance)
	{
		jdmq_synchronize(mutex);
		const string & group_name = node_instance->get_info().group();
		if (node_cluster_map.find(group_name) == node_cluster_map.end())
			return false;
		else
			return node_cluster_map[group_name]->del_node(node_instance);
	}

	node_ring_ptr NodeCluster::get_node_ring(const string & group_name)
	{
		jdmq_synchronize(mutex);
		if (node_cluster_map.find(group_name) == node_cluster_map.end())
			return node_ring_ptr();
		else
			return node_cluster_map[group_name];
	}

	node_instance_ptr NodeCluster::get_node(const string & group_name, const string & key)
	{
		node_ring_ptr node_ring = get_node_ring(group_name);
		if (node_ring)
			return node_ring->get_node(key);
		else
			return node_instance_ptr();
	}
}
