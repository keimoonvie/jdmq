#include "../common/logger.h"
#include "../mq/message.h"
#include "../mq/socket.h"
#include "context_manager.h"
#include "sync_context.h"
#include "sync_service.h"

namespace jdmq
{
	bool SyncService::process(message_t *message, socket_ptr socket)
	{
		context_ptr context = ContextManager::get_instance().seize_context(message->get_id(), socket);
		if (!context)
			return false;
		SyncContext *sync_context = (SyncContext *) context.get();
		sync_context->notify(Message::clone(message, message->get_type(), message->get_id()));
		return true;
	}
}
