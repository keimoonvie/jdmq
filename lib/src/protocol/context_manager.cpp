#include "../common/encoder.h"
#include "../mq/socket.h"
#include "context_manager.h"

using namespace std;

namespace jdmq
{
	ContextManager::ContextManager()
	{
		white_list = new context_map_t();
		grey_list = new context_map_t();
		black_list = new context_map_t();
		white_list->set_deleted_key("");
		grey_list->set_deleted_key("");
		black_list->set_deleted_key("");
	}

	ContextManager & ContextManager::get_instance()
	{
		static ContextManager instance;
		return instance;
	}

	ContextManager::ContextManager(const ContextManager & rhs)
	{
	}

	const ContextManager & ContextManager::operator=(const ContextManager & rhs)
	{
	}

	ContextManager::~ContextManager()
	{
		delete white_list;
		delete grey_list;
		delete black_list;
	}

	void ContextManager::add_context(context_ptr context, socket_ptr socket)
	{
		if (context == NULL || socket == NULL)
			return;
		string key = hash_key(context->get_id(), socket);
		jdmq_synchronize(mutex);
		(*white_list)[key] = context;
	}

	context_ptr ContextManager::seize_context(uint64_t id, socket_ptr socket)
	{
		string key = hash_key(id, socket);
		jdmq_synchronize(mutex);
		if (white_list->find(key) != white_list->end()) {
			context_ptr context = (*white_list)[key];
			white_list->erase(key);
			return context;
		} else if (grey_list->find(key) != grey_list->end()) {
			context_ptr context = (*grey_list)[key];
			grey_list->erase(key);
			return context;
		} else {
			return context_ptr();
		}
	}

	void ContextManager::destroy_contexts()
	{
		jdmq_synchronize(mutex);
		black_list->clear();
		context_map_t *tmp = black_list;
		black_list = grey_list;
		grey_list = white_list;
		white_list = tmp;
	}

	string ContextManager::hash_key(uint64_t id, socket_ptr socket)
	{
		int size = 16;
		_enc_declare_(enc, size);
		_enc_put_fix64_(enc, id);
		_enc_put_fix64_(enc, socket->get_id());
		return string(_enc_data_(enc), _enc_size_(enc));
	}
}
