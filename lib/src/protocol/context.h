#ifndef JDMQ_MQ_CONTEXT_H_
#define JDMQ_MQ_CONTEXT_H_

#include <stdint.h>
#include <string>
#include <google/sparse_hash_map>
#include "../common/common.h"
#include "../common/shared_ptr.h"

namespace jdmq
{
	class Context
	{
	public:
		Context(uint64_t id, int type);
		virtual ~Context();

		inline void set_id(uint64_t id)
		{
			this->id = id;
		}

		inline uint64_t get_id() const
		{
			return id;
		}

		inline int get_type() const
		{
			return type;
		}

		inline int get_init_time() const
		{
			return init_time;
		}
	private:
		uint64_t id;
		int type;
		int init_time;
	};

	typedef Context context_t;
	typedef pointer_t<context_t>::shared context_ptr;
	typedef google::sparse_hash_map<std::string, context_ptr> context_map_t;
}

#endif
