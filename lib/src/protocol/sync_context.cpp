#include "../mq/message.h"
#include "sync_context.h"

namespace jdmq
{
	SyncContext::SyncContext(int64_t id): context_t(id, JDMQ_CLIENT_SYNC_CONTEXT_TYPE), message(NULL), timed_out(false), done(false)
	{
	}

	SyncContext::~SyncContext()
	{
	}

	bool SyncContext::wait(int sec)
	{
		jdmq_synchronize(mutex);
		if (done)
			return true;
		if (cond.wait(mutex, sec * 1000000)) {
			timed_out = false;
			return true;
		} else {
			timed_out = true;
			return false;
		}
	}

	bool SyncContext::notify(message_t *message)
	{
		jdmq_synchronize(mutex);
		if (timed_out) {
			delete message;
			return false;
		}
		this->message = message;
		done = true;
		cond.notify_once();
		return true;
	}
}
