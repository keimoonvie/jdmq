#ifndef JDMQ_PROTOCOL_SYNC_SERVICE_H_
#define JDMQ_PROTOCOL_SYNC_SERVICE_H_

#include "../common/common.h"
#include "../mq/service.h"

namespace jdmq
{
	class SyncService: public Service
	{
	public:
		bool process(message_t* message, socket_ptr socket);
	};
}

#endif
