#include <sstream>
#include "../common/logger.h"
#include "../mq/socket.h"
#include "../mq/message.h"
#include "context_manager.h"
#include "sync_context.h"
#include "node_instance.h"

using namespace std;

namespace jdmq
{
	NodeInstance::NodeInstance()
	{
		info.set_node_id(0);
		info.set_group("");
		info.set_host("");
		info.set_port("");
		info.set_state(NODE_DEATH);
		socket.reset();
	}

	NodeInstance::NodeInstance(uint32_t id, const string & group, const string & host, const string & port)
	{
		info.set_node_id(id);
		info.set_group(group);
		info.set_host(host);
		info.set_port(port);
		info.set_state(NODE_DEATH);
		socket.reset();
	}

	void NodeInstance::connect(socket_ptr socket)
	{
		jdmq_synchronize(mutex);
		info.set_state(NODE_ALIVE);
		this->socket = socket;
	}

	void NodeInstance::active()
	{
		jdmq_synchronize(mutex);
		info.set_state(NODE_ACTIVE);
	}

	void NodeInstance::disconnect()
	{
		jdmq_synchronize(mutex);
		info.set_state(NODE_DEATH);
		socket.reset();
	}

	bool NodeInstance::send_async(message_t *msg)
	{
		socket_ptr saved_socket;
		{
			jdmq_synchronize(mutex);
			if (socket == NULL)
				return -1;
			saved_socket = socket;
		}
		msg->clear_flag(6);
		return saved_socket->send(msg) > 0;
	}

	bool NodeInstance::send_sync(message_t *msg, int timeout, message_t **reply)
	{
		socket_ptr saved_socket;
		{
			jdmq_synchronize(mutex);
			if (socket == NULL)
				return -1;
			saved_socket = socket;
		}
		msg->set_flag(6);
		context_ptr context(new SyncContext(msg->get_id()));
		ContextManager::get_instance().add_context(context, saved_socket);		
		if (saved_socket->send(msg, true) > 0) {
			SyncContext *sync_context = (SyncContext *) context.get();
			if (sync_context->wait(timeout)) {
				*reply = sync_context->get_message();
				return true;
			} else {
				ContextManager::get_instance().seize_context(context->get_id(), saved_socket);
				return false;
			}
		} else {
			return false;
		}
	}

	string NodeInstance::serialize() const
	{
		return info.SerializeAsString();
	}

	bool NodeInstance::deserialize(const string & str)
	{
		return info.ParseFromString(str);
	}

	bool NodeInstance::deserialize(const char *data, int size)
	{
		return info.ParseFromArray(data, size);
	}

	string NodeInstance::to_string() const
	{
		stringstream ss;
		ss << info.node_id();
		ss << " (";
		ss << info.host();
		ss << ", ";
		ss << info.port();
		ss << ")";
		return ss.str();
	}
}
