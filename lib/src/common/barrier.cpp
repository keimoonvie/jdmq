#include "scoped_lock.h"
#include "barrier.h"

namespace jdmq
{
	Barrier::Barrier(int parties, runnable_ptr command): parties(parties), command(command),
							    count(parties), reset(0), broken(false)
	{
	}

	Barrier::~Barrier()
	{
	}

	runnable_ptr Barrier::set_command(runnable_ptr command)
	{
		jdmq_synchronize(mutex);
		runnable_ptr old = this->command;
		this->command = command;
		return old;
	}

	void Barrier::restart()
	{
		jdmq_synchronize(mutex);
		broken = false;
		reset++;
		count = parties;
		cond.notify_all();
	}

	void Barrier::break_barrier()
	{
		jdmq_synchronize(mutex);
		broken = true;
		cond.notify_all();
	}

	int Barrier::barrier(long timeout)
	{
		jdmq_synchronize(mutex);
		int index = --count;
		if (broken) {
			return index;
                } else if (index == 0) {
			count = parties;
			reset++;
			cond.notify_all();
			if (command)
				command->run();
			return 0;
		} else {
			bool ret = cond.wait(mutex, timeout);
			if (timeout > 0 && !ret) {				
				broken = true;
				cond.notify_all();
			}
			return index;
                }
	}
}
