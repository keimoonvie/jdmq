/*
 * Shared pointer.
 */
#ifndef JDMQ_COMMON_SHARED_PTR_H_
#define JDMQ_COMMON_SHARED_PTR_H_

#include <boost/smart_ptr/shared_ptr.hpp>
#include "common.h"

namespace jdmq
{
	template<class T> 
	class pointer_t
	{
	public:
		typedef boost::shared_ptr<T> shared;
		typedef boost::weak_ptr<T> weak;
	};
}

#endif
