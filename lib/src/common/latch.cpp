#include "scoped_lock.h"
#include "latch.h"

namespace jdmq
{
	CountdownLatch::CountdownLatch(int count): count(count)
	{
	}

	CountdownLatch::~CountdownLatch()
	{
	}

	bool CountdownLatch::await(long timeout)
	{
		jdmq_synchronize(mutex);
		if (count > 0)
			return cond.wait(mutex, timeout); 
	}

	void CountdownLatch::countdown()
	{
		jdmq_synchronize(mutex);
		count--;
		if (count <= 0)
			cond.notify_all();
	}
}
