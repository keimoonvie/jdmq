#ifndef JDMQ_COMMON_QUEUE_H_
#define JDMQ_COMMON_QUEUE_H_

#include <stdint.h>
#include "common.h"
#include "scoped_lock.h"
#include "condition_variable.h"

#define JDMQ_QUEUE_CHUNK_SIZE 100
#define JDMQ_HEAP_MAX_SIZE 1000

namespace jdmq
{
	class Queue
	{
	public:
		Queue();
		virtual ~Queue();

		virtual bool init() = 0;
		virtual void finalize() = 0;
		virtual bool enqueue(void *data, int priority = 0) = 0;
		virtual void *dequeue() = 0;
		virtual int get_size() = 0;
	};

	class FIFOQueue: public Queue
	{
	public:
		FIFOQueue();
		virtual ~FIFOQueue();

		virtual bool init();
		virtual void finalize();
		virtual bool enqueue(void *data, int priority = 0);
		virtual void *dequeue();
		virtual int get_size();

	private:
		struct chunk_t
		{
			void *items[JDMQ_QUEUE_CHUNK_SIZE];
			struct chunk_t *next;
		};

		int size;
		struct chunk_t *head;
		struct chunk_t *tail;
		int chunk_head;
		int chunk_tail;
		bool running;
		mutex_t mutex;
		cond_t cond;
	};

	class PriorityQueue: public Queue
	{
	public:
		PriorityQueue();
		virtual ~PriorityQueue();

		virtual bool init();
		virtual void finalize();
		virtual bool enqueue(void *data, int priority = 0);
		virtual void *dequeue();
		virtual int get_size();
	private:
		struct heap_item_t
		{
			int priority;
			uint64_t order;
			void *data;
		};

		int size;
		heap_item_t *heap;
		int max_size;
		uint64_t order;
		bool running;
		mutex_t mutex;
		cond_t cond;
	};

	typedef Queue queue_t;
}

#endif
