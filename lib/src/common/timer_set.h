/*
 * Simple timer set based on a list.
 */
#ifndef JDMQ_COMMON_TIMER_SET_H_
#define JDMQ_COMMON_TIMER_SET_H_

#include <vector>
#include "common.h"
#include "timer.h"

namespace jdmq
{
        class TimerSet
	{
	public:
		TimerSet();
		virtual ~TimerSet();

                void add_timer(timer_t *timer);
		timer_t *get_nearest_timer();
                void start();
		inline const std::vector<timer_t *> & get_timers() const
		{
			return timer_set;
		}
	private:
		std::vector<timer_t *> timer_set;
	};

	typedef TimerSet timer_set_t;
}

#endif
