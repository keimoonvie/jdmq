/*
 * Timer astract class. Extend this class and implement the 'run' function.
 */
#ifndef JDMQ_COMMON_TIMER_H_
#define JDMQ_COMMON_TIMER_H_

#include "common.h"

#define JDMQ_TIMER_THRESHOLD 1000

namespace jdmq
{
	class Timer
        {
	public:
		Timer(long pedioric);

		inline long get_next_tick()
		{
                        return next_tick;
		}

		void advance(long now);
		void start();
		bool can_be_fired(long now);
		virtual void run() = 0;
		static void current_tick(long *tick);
	protected:
                long pedioric;
		long next_tick;
	};

	typedef Timer timer_t;
}

#endif
