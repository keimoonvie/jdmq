#include <stdio.h>
#include <time.h>
#include "condition_variable.h"

namespace jdmq
{
	ConditionVariable::ConditionVariable()
	{
                pthread_cond_init(&cond, NULL);
	}

	ConditionVariable::~ConditionVariable()
	{
	}

	bool ConditionVariable::wait(mutex_t & mutex, long timeout)
	{
		if (timeout <= 0) {
			return pthread_cond_wait(&cond, mutex.get_ptr()) == 0;
		} else {
			struct timespec time_to_wait = {0, 0};
			int sec = timeout / 1000000;
			long nsec = (timeout - sec * 1000000) * 1000000;
			time_to_wait.tv_sec = time(NULL) + sec;
			time_to_wait.tv_nsec = nsec;
			return pthread_cond_timedwait(&cond, mutex.get_ptr(), &time_to_wait) == 0;
		}
	}

	bool ConditionVariable::notify_once()
	{
		return pthread_cond_signal(&cond);
	}

	bool ConditionVariable::notify_all()
	{
		return pthread_cond_broadcast(&cond);
	}
}
