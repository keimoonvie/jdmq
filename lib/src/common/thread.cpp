#ifdef __FreeBSD__
#include <pthread_np.h>
#elif defined __linux__
#include <sys/prctl.h>
#endif
#include <sys/time.h>
#include "logger.h"
#include "thread.h"

using namespace std;

namespace jdmq
{
	Runnable::Runnable()
	{
	}

	Runnable::~Runnable()
	{
	}

	void * jdmq_common_thread_cb(void * ptr)
	{
		thread_t * thread = (thread_t *) ptr;
		pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
#ifdef __linux__
		prctl(PR_SET_NAME, thread->get_name().c_str(), 0, 0, 0);
#endif
		thread->get_runner()->run();
	}

	Thread::Thread(runnable_ptr runner, const string & name): runner(runner), name(name)
	{
	}

	Thread::~Thread()
	{
	}

	bool Thread::start()
	{
		if (pthread_create(&thread, NULL, jdmq_common_thread_cb, this) == 0) {
#ifdef __FreeBSD__
			pthread_set_name_np(thread, name.c_str());
#endif
			return true;
		}
		return false;
	}

	bool Thread::join()
	{
		return pthread_join(thread, NULL) == 0;
	}

	bool Thread::try_join()
	{
#ifdef __FreeBSD__
		struct timespec ts;
		ts.tv_sec = 0;
		ts.tv_nsec = 250000000;
		return pthread_timedjoin_np(thread, NULL, &ts) == 0;
#elif defined __linux__
		if (pthread_tryjoin_np(thread, NULL) == 0) {
			return true;
		} else {
			usleep(250000);
			return false;
		}
#endif
	}

	bool Thread::cancel()
	{
		return pthread_cancel(thread);
	}
}
