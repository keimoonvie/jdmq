/*
 * Pthread mutex wrapper.
 */
#ifndef JDMQ_COMMON_MUTEX_H_
#define JDMQ_COMMON_MUTEX_H_

#include <pthread.h>
#include "common.h"

namespace jdmq
{
	class Mutex
	{
	public:
		Mutex();
		virtual ~Mutex();

		void lock();
		void unlock();
		bool trylock();

		inline pthread_mutex_t & get()
		{
			return mutex;
		}

		inline pthread_mutex_t * get_ptr()
		{
			return &mutex;
		}

	private:
		pthread_mutex_t mutex;
	};

	typedef Mutex mutex_t;
}

#endif
