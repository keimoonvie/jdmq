/*
 * Countdown latch
 */

#ifndef JDMQ_COMMON_LATCH_H_
#define JDMQ_COMMON_LATCH_H_

#include "common.h"
#include "mutex.h"
#include "condition_variable.h"

namespace jdmq
{
	class CountdownLatch
	{
	public:
		CountdownLatch(int count = 1);
		virtual ~CountdownLatch();

		/*
		 * Wait for countdown to reach 0.
		 */
		bool await(long timeout = 0);

		/*
		 * Count down.
		 */
		void countdown();
	private:
		int count;
		mutex_t mutex;
		cond_t cond;
	};

	typedef CountdownLatch countdown_latch_t;
}

#endif
