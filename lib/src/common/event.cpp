#include <string.h>
#ifdef __FreeBSD__
#include <sys/types.h>
#include <sys/event.h>
#include <sys/time.h>
#elif defined __linux__
#include <sys/epoll.h>
#endif
#include <signal.h>
#include <errno.h>
#include <string.h>
#include "logger.h"
#include "event.h"

using namespace std;

namespace jdmq
{
	Event::Event(): running(false)
	{
#ifdef __linux__
		event_masks.set_deleted_key(-1);
#endif
	}

	Event::~Event()
	{
	}

	bool Event::init()
	{
#ifdef __FreeBSD__
		backend_fd = kqueue();
#elif defined __linux__
		backend_fd = epoll_create(1024);
#endif
		return backend_fd > 0;
	}

	bool Event::add_file_event(int fd, int type)
	{
		if (!running)
			return true;
#ifdef __FreeBSD__
		struct kevent event;
		int op = EV_ADD;
		if (type & EVENT_ONESHOT)
			op |= EV_ONESHOT;
		if (type & EVENT_READ) {
			EV_SET(&event, fd, EVFILT_READ, op, 0, 0, NULL);
			if (kevent(backend_fd, &event, 1, NULL, 0, NULL) == -1) {
				JDMQ_LOGGER_ERROR("event", "Cannot add event event on socket %d, error: %d (%s)", fd, errno, strerror(errno));
				return false;
			}
		}
		if (type & EVENT_WRITE) {
			EV_SET(&event, fd, EVFILT_WRITE, op, 0, 0, NULL);
			if (kevent(backend_fd, &event, 1, NULL, 0, NULL) == -1) {
				JDMQ_LOGGER_ERROR("event", "Cannot add event write on socket %d, error: %d (%s)", fd, errno, strerror(errno));
				return false;
			}
		}
		return true;
#elif defined __linux__
		jdmq_synchronize(event_mutex);
		struct epoll_event event;
		memset(&event, 0, sizeof(event));
                event.data.fd = fd;
                event.events = 0;
		int op, mask;
		if (event_masks.find(fd) == event_masks.end()) {
			op = EPOLL_CTL_ADD;
                        mask = 0;
		} else {
                        op = EPOLL_CTL_MOD;
			mask = event_masks[fd];
                }
                mask = mask | type;
		if (mask & EVENT_READ)
                        event.events = event.events | EPOLLIN;
                if (mask & EVENT_WRITE)
			event.events = event.events | EPOLLOUT;
		if (mask & EVENT_ONESHOT)
			event.events = event.events | EPOLLONESHOT;
                event_masks[fd] = mask;
		if (epoll_ctl(backend_fd, op, fd, &event) == 0) {
			return true;
		} else {
                        JDMQ_LOGGER_ERROR("epoll", "Add event error on fd %d, errno: %d", fd, errno);
			return false;
		}
#endif
	}

	bool Event::clear_file_event(int fd, int type)
	{
#ifdef __FreeBSD__
		struct kevent event;
		if (type & EVENT_READ) {
                        EV_SET(&event, fd, EVFILT_READ, EV_DELETE, 0, 0, NULL);
			if (kevent(backend_fd, &event, 1, NULL, 0, NULL) == -1)
                                return false;
		}
                if (type & EVENT_WRITE) {
                        EV_SET(&event, fd, EVFILT_WRITE, EV_DELETE, 0, 0, NULL);
                        if (kevent(backend_fd, &event, 1, NULL, 0, NULL) == -1)
                                return false;
                }
#elif defined __linux__
		jdmq_synchronize(event_mutex);
		if (event_masks.find(fd) == event_masks.end())
			return false;

		struct epoll_event event;
                memset(&event, 0, sizeof(event));
                event.data.fd = fd;
                event.events = 0;
                int mask = event_masks[fd] & (~type);
                int op = (mask == 0) ? EPOLL_CTL_DEL : EPOLL_CTL_MOD;
                if (mask & EVENT_READ)
                        event.events = event.events | EPOLLIN;
                if (mask & EVENT_WRITE)
                        event.events = event.events | EPOLLOUT;
		if (mask & EVENT_ONESHOT)
                        event.events = event.events | EPOLLONESHOT;
                event_masks[fd] = mask;

		if (epoll_ctl(backend_fd, op, fd, &event) == 0) {
                        return true;
                } else {
                        JDMQ_LOGGER_ERROR("epoll", "Delete event error on fd %d, errno: %d", fd, errno);
                        return false;
                }
#endif
	}

	void Event::delete_file_event(int fd)
	{
#ifdef __linux__
		jdmq_synchronize(event_mutex);
                if (event_masks.find(fd) != event_masks.end())
                        event_masks.erase(fd);
#endif
	}

	bool Event::add_timer(timer_t *timer)
	{
		timer_set.add_timer(timer);
	}

	void Event::loop()
	{
#ifdef __FreeBSD__
		struct kevent events[JDMQ_MAX_EVENT];
#elif defined __linux__
		struct epoll_event events[JDMQ_MAX_EVENT];
#endif
		int n, i, fd, mask, event_type;
		long now;
		timer_set.start();
		running = true;
		long wait_time;
#ifdef __FreeBSD__
		struct timespec timeout;
#endif
		while (running) {
			//Calculate wait timer.
			wait_time = 0;
			timer_t *timer = timer_set.get_nearest_timer();
			if (timer == NULL) {
				wait_time = 60;
			} else {
				timer_t::current_tick(&now);
				wait_time = timer->get_next_tick() - now;
				if (wait_time < 1)
					wait_time = 1;
				if (wait_time > 100)
					wait_time = 100;
			}
#ifdef __FreeBSD__
			timeout.tv_sec = wait_time / 1000;
			timeout.tv_nsec = wait_time * 1000000;
#endif
			//Wait
#ifdef __FreeBSD__
			n = kevent(backend_fd, NULL, 0, events, JDMQ_MAX_EVENT, &timeout);
#elif defined __linux__
			n = epoll_wait(backend_fd, events, JDMQ_MAX_EVENT, wait_time);
#endif

			//File event
			for (i = 0; i < n; i++) {
				mask = 0;
#ifdef __FreeBSD__
				event_type = events[i].filter;
				fd = events[i].ident;
				if (events[i].flags & EV_ERROR)
					mask = mask | EVENT_ERROR;
				if (event_type == EVFILT_READ)
					mask = mask | EVENT_READ;
				if (event_type == EVFILT_WRITE)
					mask = mask | EVENT_WRITE;
#elif __linux__
				event_type = events[i].events;
                                fd = events[i].data.fd;
                                if (event_type & EPOLLERR || event_type & EPOLLHUP)
                                        mask = mask | EVENT_ERROR;
                                if (event_type & EPOLLIN)
                                        mask = mask | EVENT_READ;
                                if (event_type & EPOLLOUT)
                                        mask = mask | EVENT_WRITE;
#endif
				file_event_handler(fd, mask);
			}

			//Timer event
			timer_t::current_tick(&now);
			const vector<timer_t *> & timers = timer_set.get_timers();
			for (i = 0; i < timers.size(); i++) {
				if (timers[i]->can_be_fired(now)) {
					timer_handler(timers[i]);
					timers[i]->advance(now);
				}
			}
		}
		JDMQ_LOGGER_INFO("event", "Event loop stopped");
	}

	void Event::stop()
	{
		running = false;
		close(backend_fd);
	}
}
