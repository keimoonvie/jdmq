#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <signal.h>
#include <errno.h>
#include <execinfo.h>
#include <boost/lexical_cast.hpp>
#include "scoped_lock.h"
#include "encoder.h"
#include "utilities.h"

using namespace std;

namespace jdmq
{
	mutex_t Utilities::print_mem_mutex;

	void Utilities::print_mem(const void *data, size_t len)
        {
		scoped_lock_t scoped_lock(Utilities::print_mem_mutex);
		if (len < 1)
                        return;
		int i;
                fprintf(stderr, "0x ");
                for (i = 0; i < len - 1; i++)
                        fprintf(stderr, "%x ", (int) * ((unsigned char *) data + i));
		fprintf(stderr, "%x\n", (int) * ((unsigned char *) data + len - 1));
		fflush(stderr);
	}

	string Utilities::make_string(int32_t val)
	{
		return boost::lexical_cast<string>(val);
	}

	string Utilities::make_string(uint32_t val)
	{
		return boost::lexical_cast<string>(val);
	}

	string Utilities::make_string(int64_t val)
	{
		return boost::lexical_cast<string>(val);
	}

	string Utilities::make_string(uint64_t val)
	{
		return boost::lexical_cast<string>(val);
	}

	uint32_t Utilities::str_to_uint32(const string & str)
        {
                if (str.size() == 0)
                        return 0;
                _dec_declare2_(dec, str.data(), str.size());
                int val;
                _dec_get_var32_(dec, val);
                return (_dec_valid_(dec)) ? (uint32_t) val : 0;
        }

        string Utilities::uint32_to_str(uint32_t val)
        {
                _enc_declare_(enc, 5);
                _enc_put_var32_(enc, val);
                return string(_enc_data_(enc), _enc_size_(enc));
        }

        uint64_t Utilities::str_to_uint64(const string & str)
        {
                if (str.size() == 0)
                        return 0;
                _dec_declare2_(dec, str.data(), str.size());
                int64_t val;
                _dec_get_fix64_(dec, val);
                return (_dec_valid_(dec)) ? (uint64_t) val : 0;
        }

	string Utilities::uint64_to_str(uint64_t val)
        {
                _enc_declare_(enc, sizeof(uint64_t));
                _enc_put_fix64_(enc, (int64_t) val);
                return string(_enc_data_(enc), _enc_size_(enc));
        }

	void Utilities::str_vector_to_uint32_vector(const vector<string> & str_vector, vector<uint32_t> & uint32_vector)
        {
                uint32_vector.clear();
                int i;
                for (i = 0; i < str_vector.size(); i++)
                        uint32_vector.push_back(Utilities::str_to_uint32(str_vector[i]));
        }

	void Utilities::uint32_vector_to_str_vector(const vector<uint32_t> & uint32_vector, vector<string> & str_vector)
        {
                str_vector.clear();
                int i;
                for (i = 0; i < uint32_vector.size(); i++)
                        str_vector.push_back(Utilities::uint32_to_str(uint32_vector[i]));
        }

	void Utilities::str_vector_to_uint64_vector(const vector<string> & str_vector, vector<uint64_t> & uint64_vector)
        {
                uint64_vector.clear();
                int i;
                for (i = 0; i < str_vector.size(); i++) {
                        uint64_vector.push_back(Utilities::str_to_uint64(str_vector[i]));
                }
        }

	void Utilities::uint64_vector_to_str_vector(const vector<uint64_t> & uint64_vector, vector<string> & str_vector)
        {
                str_vector.clear();
                int i;
                for (i = 0; i < uint64_vector.size(); i++)
                        str_vector.push_back(Utilities::uint64_to_str(uint64_vector[i]));
        }

	void Utilities::str_set_to_uint32_set(const set<string> & str_set, set<uint32_t> & uint32_set)
        {
                uint32_set.clear();
                set<string>::const_iterator it;
                for (it = str_set.begin(); it != str_set.end(); it++)
                        uint32_set.insert(Utilities::str_to_uint32(*it));
        }

	void Utilities::uint32_set_to_str_set(const set<uint32_t> & uint32_set, set<string> & str_set)
        {
                str_set.clear();
                set<uint32_t>::const_iterator it;
                for (it= uint32_set.begin(); it != uint32_set.end(); it++)
                        str_set.insert(Utilities::uint32_to_str(*it));
        }

	void Utilities::str_set_to_uint64_set(const set<string> & str_set, set<uint64_t> & uint64_set)
        {
                uint64_set.clear();
                set<string>::const_iterator it;
                for (it= str_set.begin(); it != str_set.end(); it++)
                        uint64_set.insert(Utilities::str_to_uint64(*it));
        }

	void Utilities::uint64_set_to_str_set(const set<uint64_t> & uint64_set, set<string> & str_set)
        {
                str_set.clear();
                set<uint64_t>::const_iterator it;
                for (it= uint64_set.begin(); it != uint64_set.end(); it++)
                        str_set.insert(Utilities::uint64_to_str(*it));
        }

	int Utilities::get_current_time()
	{
		return (uint32_t) time(NULL);
	}

	int64_t Utilities::get_current_time_micro()
	{
		struct timeval tv;
                gettimeofday(&tv, NULL);
		return tv.tv_sec * 1000000 + tv.tv_usec;
	}

	void Utilities::get_current_time_as_string(char *str)
	{
		time_t rawtime;
		struct tm timeinfo;
		time(&rawtime);
		localtime_r(&rawtime, &timeinfo);

		strftime(str, 1024, "%m/%d/%Y %H:%M:%S %Z", &timeinfo);
	}

	void Utilities::zeromem(void *mem, int size)
	{
		memset(mem, 0, size);
	}

	void Utilities::mem_append(void *dest, int dest_size, const void *src, int src_size)
	{
		char *real_dest = (char *) dest + dest_size;
		memcpy(real_dest, src, src_size);
	}

	bool Utilities::register_interrupt_cb(void (*handler)(int))
	{
		struct sigaction act;
                memset(&act, 0, sizeof(act));
		act.sa_handler = handler;
		return sigaction(SIGTERM, &act, 0) == 0 && sigaction(SIGINT, &act, 0) == 0;
	}

	bool Utilities::register_abnormal_cb(void (*handler)(int))
	{
		struct sigaction act;
		memset(&act, 0, sizeof(act));
		act.sa_handler = handler;
		return sigaction(SIGSEGV, &act, 0) == 0 && sigaction(SIGABRT, &act, 0) == 0;
	}

	bool Utilities::register_reload_cb(void (*handler)(int))
	{
		struct sigaction act;
		memset(&act, 0, sizeof(act));
		act.sa_handler = handler;
		return sigaction(SIGUSR1, &act, 0) == 0 && sigaction(SIGABRT, &act, 0) == 0;
	}

	vector<string> Utilities::get_stack_traces(int size)
	{
		void *array[size];
		size_t stack_size;
		char **strings;
		size_t i;

		stack_size = backtrace(array, size);
		strings = backtrace_symbols(array, stack_size);

		vector<string> stack_traces;
		for (int i = 0; i < stack_size; i++)
			stack_traces.push_back(strings[i]);

		free(strings);
		return stack_traces;
	}
}

