#include <stdio.h>
#include "timer_set.h"

namespace jdmq
{
	TimerSet::TimerSet()
	{
	}

	TimerSet::~TimerSet()
	{
		for (int i = 0; i < timer_set.size(); i++)
			delete timer_set[i];
	}

        void TimerSet::add_timer(timer_t *timer)
        {
                timer_set.push_back(timer);
	}

        timer_t *TimerSet::get_nearest_timer()
	{
		Timer *nearest = NULL;
                for (int i = 0; i < timer_set.size(); i++) {
			if (!nearest || nearest->get_next_tick() > timer_set[i]->get_next_tick())
				nearest = timer_set[i];
		}
                return nearest;
	}

        void TimerSet::start()
	{
		for (int i = 0; i < timer_set.size(); i++) {
			timer_set[i]->start();
		}
	}
}
