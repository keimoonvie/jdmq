/*
 * Cyclic barrier implementation.
 */

#ifndef JDMQ_COMMON_BARRIER_H_
#define JDMQ_COMMON_BARRIER_H_

#include "common.h"
#include "thread.h"
#include "mutex.h"
#include "condition_variable.h"

namespace jdmq
{
	class Barrier
	{
	public:
		Barrier(int parties, runnable_ptr command);
		virtual ~Barrier();

		inline bool is_broken() const
		{
			return broken;
		}

		inline int get_parties() const
		{
			return parties;
		}

		runnable_ptr set_command(runnable_ptr command);
		void break_barrier();
		void restart();
		int barrier(long timeout = 0);

	private:
		int parties;
		int count;
		int reset;
		bool broken;
		runnable_ptr command;
		mutex_t mutex;
		cond_t cond;
	};

	typedef Barrier barrier_t;
}

#endif
