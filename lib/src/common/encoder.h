/*
 * Message encoder.
 */

#ifndef JDMQ_COMMON_ENCODER_H_
#define JDMQ_COMMON_ENCODER_H_

#include <stdint.h>
#include <string>
#include "common.h"

#define _var_uint32_(buf,nindex,value)\
{\
	buf[nindex++] = ((value & 0x7F) | (0x80));\
	if (value>=128)\
	{\
		value >>= 7;\
		buf[nindex++] = ((value & 0x7F) | (0x80));\
		if (value>=128)\
		{\
			value >>= 7;\
			buf[nindex++] = ((value & 0x7F) | (0x80));\
			if (value>=128)\
			{\
				value >>= 7;\
				buf[nindex++] = ((value & 0x7F) | (0x80));\
				if (value>=128)\
				{\
					value >>= 7;\
					buf[nindex++] = ((value & 0x7F) | (0x80));\
				}\
			}\
		}\
	}\
	buf[nindex-1] &= 0x7F;\
}

/************************************** ENCODER **************************************************/
#define _enc_declare_(enc, size)\
	char enc##_buf[size];\
	int enc##_index = 0;\

#define _enc_size_(enc)		(enc##_index)
#define _enc_data_(enc)		(enc##_buf)

#define _enc_put_byte_(enc, val)\
{\
	enc##_buf[enc##_index++] = val;\
}

#define _enc_put_fix16_(enc, val)\
{\
	enc##_buf[enc##_index++] = (val >> 8);\
	enc##_buf[enc##_index++] = (val & 0xFF);\
}

#define _enc_put_fix32_(enc, val)\
{\
	enc##_buf[enc##_index++] = (val>>24);\
	enc##_buf[enc##_index++] = ((val>>16) & 0xFF);\
	enc##_buf[enc##_index++] = ((val>>8) & 0xFF);\
	enc##_buf[enc##_index++] = (val & 0xFF);\
}

#define _enc_put_fix24_(enc, val)\
{\
	enc##_buf[enc##_index++] = ((val>>16) & 0xFF);\
	enc##_buf[enc##_index++] = ((val>>8) & 0xFF);\
	enc##_buf[enc##_index++] = (val & 0xFF);\
}

#define _enc_put_fix64_(enc, val)\
{\
	int64_t val1 = val;\
	_enc_put_fix32_(enc, (val1 >> 32));\
	_enc_put_fix32_(enc, (val1 & 0xFFFFFFFF));\
}

#define _enc_put_string_(enc, val)\
{\
	std::string val1 = val;\
	size_t size = val1.size();\
	_enc_put_var32_(enc, size);\
	for (size_t i=0;i<size; i++) enc##_buf[enc##_index++] = val1[i];\
}

#define _enc_put_buffer_(enc, val, size)\
{\
	_enc_put_var32_(enc, size);\
	for (size_t i=0; i<size; i++) enc##_buf[enc##_index++] = val[i];\
}

#define _enc_put_var32_(enc, val) {int val1 = val; _var_uint32_(enc##_buf,enc##_index,val1);}

#define _enc_put_msg_header_(enc, msg_type, msg_id, streaming)\
{\
	_enc_put_byte_(enc, 0xEE);\
	_enc_put_byte_(enc, 0xEE);\
	_enc_put_fix16_(enc, 0)\
	_enc_put_fix24_(enc, msg_type)\
	char tmp = 0;\
	if (streaming) tmp = 1;\
	if (msg_id > 0) tmp += 2;\
	_enc_put_byte_(enc, tmp);\
	if (msg_id > 0) _enc_put_fix64_(enc, msg_id);\
}

#define _enc_set_msg_size_(enc, size)\
{\
	enc##_buf[2] = ((size >>8) & 0xFF);\
	enc##_buf[3] = (size & 0xFF);\
}

/************************************** DECODER **************************************************/
#define _dec_declare1_(dec, data)\
	const char* dec##_buf = data.c_str();\
	int dec##_index = 0;\
	int dec##_size = data.size();\
	bool dec##_valid = true;

#define _dec_declare2_(dec, data, size)\
	const char* dec##_buf = data;\
	int dec##_index = 0;\
	int dec##_size = size;\
	bool dec##_valid = true;

#define _dec_valid_(dec)	(dec##_valid)

#define _dec_get_byte_(dec, val)\
{\
	if (dec##_index > dec##_size - 1) dec##_valid = false;\
	else val = dec##_buf[dec##_index++];\
}

#define _dec_get_fix16_(dec, val)\
{\
	if (dec##_index > dec##_size - 2) dec##_valid = false;\
	else\
	{\
		val = dec##_buf[dec##_index++];\
		val = (val<<8) + (dec##_buf[dec##_index++] & 0xFF);\
	}\
}

#define _dec_get_fix32_(dec, val)\
{\
	if (dec##_index > dec##_size - 4) dec##_valid = false;\
	else\
	{\
		val = dec##_buf[dec##_index++];\
		val = (val<<8) + (dec##_buf[dec##_index++] & 0xFF);\
		val = (val<<8) + (dec##_buf[dec##_index++] & 0xFF);\
		val = (val<<8) + (dec##_buf[dec##_index++] & 0xFF);\
	}\
}

#define _dec_get_fix64_(dec, val)\
{\
	if (dec##_index > dec##_size - 8) dec##_valid = false;\
	else\
	{\
		val = dec##_buf[dec##_index++];\
		val = (val<<8) + (dec##_buf[dec##_index++] & 0xFF);\
		val = (val<<8) + (dec##_buf[dec##_index++] & 0xFF);\
		val = (val<<8) + (dec##_buf[dec##_index++] & 0xFF);\
		val = (val<<8) + (dec##_buf[dec##_index++] & 0xFF);\
		val = (val<<8) + (dec##_buf[dec##_index++] & 0xFF);\
		val = (val<<8) + (dec##_buf[dec##_index++] & 0xFF);\
		val = (val<<8) + (dec##_buf[dec##_index++] & 0xFF);\
	}\
}

#define _dec_get_string_(dec, val)\
{\
	if (dec##_index > dec##_size - 1) dec##_valid = false;\
	else\
	{\
		int size;\
		_dec_get_var32_(dec, size);\
		if (dec##_index > dec##_size - size) dec##_valid = false;\
		else\
		{\
			std::string ret( size, 0 );\
			for (int i=0;i<size;i++) ret[i]=dec##_buf[dec##_index++];\
			val.swap(ret);\
		}\
	}\
}

#define _dec_get_var32_(dec, val)\
{\
	unsigned char* m = (unsigned char*) (dec##_buf + dec##_index);\
	val = (m[0] & 0x7F);\
	if (m[0] >= 128)\
	{\
		val += ((m[1] & 0x7F)<<7);\
		if (m[1] >= 128)\
		{\
			val += ((m[2] & 0x7F)<<14);\
			if (m[2] >= 128)\
			{\
				val += ((m[3] & 0x7F)<<21);\
				if (m[3] >= 128)\
				{\
					val += ((m[4] & 0x7F)<<28);\
					dec##_index += 5;\
				}\
				else dec##_index += 4;\
			}\
			else dec##_index += 3;\
		}\
		else dec##_index += 2;\
	}\
	else dec##_index += 1;\
}

#endif

