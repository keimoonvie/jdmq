#include "scoped_lock.h"

namespace jdmq
{
	ScopedLock::ScopedLock(mutex_t & mutex)
	{
		mutex_ptr = &mutex;
		mutex_ptr->lock();
	}

	ScopedLock::~ScopedLock()
	{
		if (mutex_ptr)
			mutex_ptr->unlock();
	}
}
