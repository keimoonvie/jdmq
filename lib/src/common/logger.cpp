#include <stdlib.h>
#include <string.h>
#include "utilities.h"
#include "scoped_lock.h"
#include "logger.h"

namespace jdmq
{
	Logger::Logger(): level(INFO), verbose(true)
	{
	}

	Logger::~Logger()
	{
	}

	void Logger::debug(const char * file, int line, const char *group, const char *format, ...)
	{
		va_list ap;
		va_start(ap, format);
		vlog(file, line, group, DEBUG, format, ap);
		va_end(ap);
	}

	void Logger::info(const char * file, int line, const char *group, const char *format, ...)
	{
		va_list ap;
                va_start(ap, format);
                vlog(file, line, group, INFO, format, ap);
                va_end(ap);
        }

	void Logger::warning(const char * file, int line, const char *group, const char *format, ...)
	{
		va_list ap;
                va_start(ap, format);
                vlog(file, line, group, WARNING, format, ap);
                va_end(ap);
        }

	void Logger::error(const char * file, int line, const char *group, const char *format, ...)
	{
		va_list ap;
                va_start(ap, format);
                vlog(file, line, group, ERROR, format, ap);
                va_end(ap);
        }

	void Logger::fatal(const char * file, int line, const char *group, const char *format, ...)
	{
		va_list ap;
                va_start(ap, format);
                vlog(file, line, group, FATAL, format, ap);
                va_end(ap);
		abort();
        }

	const char *Logger::log_level_string[] = {
		"DEBUG", "INFO", "WARNING", "ERROR", "FATAL"
	};

	DefaultLogger::DefaultLogger(): initialized(false), stream(NULL)
	{
	}

	bool DefaultLogger::init(LogLevel level, const char *file_name)
	{
		if (!initialized) {
			set_level(level);
			if (file_name == NULL) {
				stream = stderr;
				return true;
			} else {
				stream = fopen(file_name, "a");
				return stream != NULL;
			}
			initialized = true;
		}
	}

	FILE * DefaultLogger::set_stream(FILE *stream)
	{
		FILE *old = this->stream;
		this->stream = stream;
		return old;
	}

	void DefaultLogger::finalize()
	{
		if (initialized) {
			if (stream != stderr)
				fclose(stream);
		}
	}

	void DefaultLogger::vlog(const char * file, int line, const char *group, LogLevel level, const char *format, va_list ap)
	{
		jdmq_synchronize(mutex);
		if (stream == NULL) {
			fprintf(stderr, "Logger is not initialized.\n");
			abort();
		}

		if (level >= this->level) {
			if (verbose) {
				char time_string[1024];
				Utilities::get_current_time_as_string(time_string);
				fprintf(stream, "\"%s\"\t", time_string);
				fprintf(stream, "%s\t", log_level_string[level]);
				char *filename = (char *) strrchr(file, '/');
				if (filename == NULL)
					filename = (char *) file;
				else
					filename = filename + 1;
				fprintf(stream, "%s:%d\t", filename, line);
			}
			fprintf(stream, "%s\t", group);
			vfprintf(stream, format, ap);
			fprintf(stream, "\n");
			fflush(stream);
		}
	}

	JdmqInternalLogger::JdmqInternalLogger()
	{
		logger.init(Logger::DEBUG, NULL);
	}

	JdmqInternalLogger::~JdmqInternalLogger()
	{
	}

	JdmqInternalLogger & JdmqInternalLogger::get_instance()
	{
		static JdmqInternalLogger instance;
		return instance;
	}

	void JdmqInternalLogger::init(Logger::LogLevel level, FILE *stream)
	{
		logger.set_level(level);
		if (stream)
			logger.set_stream(stream);
	}
}
