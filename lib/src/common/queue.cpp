#include <iostream>
#include "queue.h"

using namespace std;

namespace jdmq
{
	Queue::Queue()
	{
	}

	Queue::~Queue()
	{
	}

	FIFOQueue::FIFOQueue(): size(0), running(false)
	{
		struct chunk_t *first_chunk = (struct chunk_t *) malloc(sizeof(struct chunk_t));
		first_chunk->next = NULL;
		head = tail = first_chunk;
	}

	FIFOQueue::~FIFOQueue()
	{
		if (head)
			free(head);
	}

	bool FIFOQueue::init()
	{
		if (running == true)
			return true;
		if (head == NULL)
			return false;
		chunk_head = chunk_tail = -1;
		return running = true;
	}

	void FIFOQueue::finalize()
	{
		jdmq_synchronize(mutex);
		if (running) {
			running = false;
			cond.notify_all();
		}
	}

	bool FIFOQueue::enqueue(void *data, int priority)
	{
		if (data == NULL || !running)
			return false;
		jdmq_synchronize(mutex);
		chunk_tail++;
		if (chunk_head == -1) {
			chunk_head = 0;
		} else if (chunk_tail >= JDMQ_QUEUE_CHUNK_SIZE) {
			struct chunk_t *new_chunk = (struct chunk_t *) malloc(sizeof(struct chunk_t));
			if (new_chunk == NULL)
				return false;
			tail->next = new_chunk;
			new_chunk->next = NULL;
			tail = new_chunk;
			chunk_tail = 0;
		}
		tail->items[chunk_tail] = data;
		size++;
		cond.notify_once();
		return true;
	}

	void *FIFOQueue::dequeue()
	{
		jdmq_synchronize(mutex);
		while (running && size == 0)
			cond.wait(mutex);

		if (!running && size == 0)
			return NULL;
		void *data = head->items[chunk_head];
		chunk_head++;
		if (head == tail) {
			if (chunk_head > chunk_tail)
				chunk_head = chunk_tail = -1;
		} else {
			if (chunk_head >= JDMQ_QUEUE_CHUNK_SIZE) {
				struct chunk_t *old_head = head;
				head = old_head->next;
				chunk_head = 0;
				free(old_head);
			}
		}
		size--;
		return data;
	}

	int FIFOQueue::get_size()
	{
		return size;
	}

	PriorityQueue::PriorityQueue(): size(0), max_size(JDMQ_HEAP_MAX_SIZE), order(0), running(false)
	{
		heap = (struct heap_item_t *) malloc(max_size * sizeof(struct heap_item_t));
	}

	PriorityQueue::~PriorityQueue()
	{
		if (heap)
			free(heap);
	}

	bool PriorityQueue::init()
	{
		if (running)
			return true;
		if (heap == NULL)
			return false;
		running = true;
		return true;
	}

	void PriorityQueue::finalize()
	{
		jdmq_synchronize(mutex);
                if (running) {
                        running = false;
			cond.notify_all();
		}
	}

	bool PriorityQueue::enqueue(void *data, int priority)
	{
		if (data == NULL || !running)
			return false;
		jdmq_synchronize(mutex);
		if (size >= max_size) {			
			int new_max_size = 2 * max_size;
			struct heap_item_t *new_heap = (struct heap_item_t *) realloc(heap, new_max_size * sizeof(struct heap_item_t));
			if (new_heap == NULL)
				return false;
			heap = new_heap;
			max_size = new_max_size;
		}
		struct heap_item_t *item = heap + size;
		item->data = data;
		item->priority = priority;
		item->order = ++order;

		if (size > 0) {
			struct heap_item_t tmp;
			struct heap_item_t *parent;
			int pos = size;
			int parent_pos;
			while (true) {
				parent_pos = (pos - 1) / 2;
				parent = heap + parent_pos;
				if (parent->priority < item->priority) {
					tmp = *item;
					*item = *parent;
					*parent = tmp;
					pos = parent_pos;
					item = parent;
				} else {
					break;
				}
			}
		}
		size++;
		cond.notify_once();
		return true;
	}

	void *PriorityQueue::dequeue()
	{
		jdmq_synchronize(mutex);
		while (running && size == 0)
			cond.wait(mutex);
                if (!running && size == 0)
                        return NULL;
		struct heap_item_t *root = heap;
		void *data = root->data;
		if (size == 1) {
			root->data = NULL;
			root->priority = root->order = 0;
			size--;
		} else {
			struct heap_item_t tmp = *root;
			struct heap_item_t *last = heap + size - 1;
			*root = *last;
			*last = tmp;

			last->data = NULL;
                        last->priority = last->order = 0;
			size--;

			struct heap_item_t *item = root;
			struct heap_item_t *child;
			int pos = 0;
			int child_pos;

			while (true) {
				if (2 *pos + 1 >= size)
					break;
				if (2 * pos + 2 >= size || 
				    heap[2 * pos + 1].priority > heap[2 * pos + 2].priority || 
				    heap[2 * pos + 1].priority == heap[2 * pos + 2].priority && 
				    heap[2 * pos + 1].order < heap[2 * pos + 2].order)
					child_pos = 2 * pos + 1;
				else
					child_pos = 2 * pos + 2;
				child = heap + child_pos;
				if (child->priority < item->priority || child->priority == item->priority && child->order > item->order)
					break;
				tmp = *item;
				*item = *child;
				*child = tmp;
				pos = child_pos;
				item = child;
			}
		}
		if (size < max_size / 3 && max_size > JDMQ_HEAP_MAX_SIZE) {
			int new_max_size = max_size / 2;
			struct heap_item_t *new_heap = (struct heap_item_t *) realloc(heap, new_max_size * sizeof(struct heap_item_t));
			if (new_heap == NULL)
				return false;
			heap = new_heap;
                        max_size = new_max_size;
		}
		return data;
	}

	int PriorityQueue::get_size()
	{
		return size;
	}
}
