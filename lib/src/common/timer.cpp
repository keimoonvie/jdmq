#include <time.h>
#include "timer.h"

namespace jdmq
{
	Timer::Timer(long pedioric): pedioric(pedioric), next_tick(-2000)
	{
        }

        void Timer::advance(long now)
	{
		next_tick = now + pedioric;
	}

	void Timer::current_tick(long *tick)
	{
		struct timespec ts;
		clock_gettime(CLOCK_MONOTONIC, &ts);
		*tick = ts.tv_sec * 1000 + ts.tv_nsec / 1000000;
	}

	void Timer::start()
	{
		long now;
		current_tick(&now);
		next_tick = now + 2000;
	}

	bool Timer::can_be_fired(long now)
        {
		if (now > next_tick) {
			if (now - next_tick < JDMQ_TIMER_THRESHOLD) {
				return true;
			} else {
				next_tick = now + pedioric;
				return false;
			}
		} else {
			return false;
		}
	}
}
