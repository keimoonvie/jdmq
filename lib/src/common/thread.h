/*
 * Java-like thread implementation.
 */
#ifndef JDMQ_COMMON_THREAD_H_
#define JDMQ_COMMON_THREAD_H_

#include <pthread.h>
#include <string>
#include "common.h"
#include "shared_ptr.h"

namespace jdmq
{
	/*
	 * Runnable abstract class.
	 */
	class Runnable
	{
	public:
		Runnable();
		virtual ~Runnable();

		virtual void run() = 0;
	};

	typedef Runnable runnable_t;
	typedef pointer_t<Runnable>::shared runnable_ptr;

	/*
	 * Thread class.
	 */
	class Thread
	{
	public:
		Thread(runnable_ptr runner, const std::string & name = "");
		virtual ~Thread();

		bool start();
		bool join();
		bool try_join();
		bool cancel();

		inline runnable_ptr get_runner()
		{
			return runner;
		}

		inline const std::string get_name() const
		{
			return name;
		}

	private:		
		pthread_t thread;
		std::string name;
		runnable_ptr runner;
	};

	typedef Thread thread_t;
}

#endif
