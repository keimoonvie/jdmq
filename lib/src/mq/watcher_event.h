#ifndef JDMQ_MQ_WATCHER_EVENT_H_
#define JDMQ_MQ_WATCHER_EVENT_H_

#include "../common/common.h"
#include "mq.h"

namespace jdmq
{
	class WatcherEvent
	{
	public:
		WatcherEvent();
		virtual ~WatcherEvent();

		void set_watcher(watcher_t *watcher);
		virtual void on_accept(socket_ptr socket) = 0;
		virtual void on_close(socket_ptr socket) = 0;
	private:
		watcher_t *watcher;
	};

	class NullWatcherEvent: public WatcherEvent
	{
	public:
		void on_accept(socket_ptr socket);
		void on_close(socket_ptr socket);
	};
}

#endif
