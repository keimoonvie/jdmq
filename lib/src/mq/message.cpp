#include <stdio.h>
#include <string.h>
#include "../common/utilities.h"
#include "message.h"

namespace jdmq
{
	Message::Message(int data_size, int type, uint64_t id): type(type), id(id), data(NULL)
	{
		init(data_size, type, id);
	}

	Message::Message(const void *data, int data_size, int type, uint64_t id): type(type), id(id), data(NULL)
	{
		init(data_size, type, id);
		if (this->data)
			memcpy(this->data + this->header_size, data, data_size);
	}

	void Message::init(int data_size, int type, uint64_t id)
	{
		header_size = 16;
		int real_size = header_size + data_size;
		if (real_size >= 65535)
			header_size += 4;
                size = header_size + data_size;
                data = (unsigned char *) malloc(size);
                if (data) {
                        int current = 0;
                        //2 bytes magic
                        data[0] = data[1] = (unsigned char) PACKET_MAGIC;
			current += 2;

                        //Size
                        if (size < 65535) {
                                data[2] = (size >> 8) & 0xFF;
                                data[3] = size & 0xFF;
                                current += 2;
                        } else {
                                data[2] = (unsigned char) 0xFF;
				data[3] = (unsigned char) 0xFF;

				data[4] = (size >> 24) & 0xFF;
				data[5] = (size >> 16) & 0xFF;
				data[6] = (size >> 8) & 0xFF;
				data[7] = size & 0xFF;
				current += 6;
			}

                        //Type
                        data[current] = (type >> 16) & 0xFF;
                        data[current + 1] = (type >> 8) & 0xFF;
			data[current + 2] = type & 0xFF;
			current += 3;

			//Flags
                        data[current] = 0;
			flag_pos = current;
			//ID
			data[current] = (unsigned char) 0x2; //ID bit.
			current++;
			
			data[current] = (id >> 56) & 0xFF;
			data[current + 1] = (id >> 48) & 0xFF;
			data[current + 2] = (id >> 40) & 0xFF;
			data[current + 3] = (id >> 32) & 0xFF;
			data[current + 4] = (id >> 24) & 0xFF;
			data[current + 5] = (id >> 16) & 0xFF;
			data[current + 6] = (id >> 8) & 0xFF;
			data[current + 7] = id & 0xFF;
		}
	}

	Message::~Message()
	{
		if (data)
			free(data);
	}

	char * Message::get_content() const
	{
		return (char *) (data + header_size);
	}

	bool Message::check_flag(int pos)
	{
		return (data[flag_pos] & (1 << pos)) != 0;
	}

	void Message::set_flags(char new_flags)
	{
		this->flags = new_flags;
		data[flag_pos] = data[flag_pos] & (unsigned char) 0x2;
		new_flags = new_flags & (~(0x2));
		data[flag_pos] = data[flag_pos] | new_flags;
	}

	void Message::set_flag(int pos)
	{
		if (pos == 1)
			return;
		data[flag_pos] = data[flag_pos] | (1 << pos);
	}

	void Message::clear_flag(int pos)
	{
		if (pos == 1)
			return;
		data[flag_pos] = data[flag_pos] & (~(1 << pos));
	}

	void Message::set_id(uint64_t message_id)
	{
		if (check_flag(1)) {
			id = message_id;
			int current = header_size - 8;
			data[current] = (id >> 56) & 0xFF;
			data[current + 1] = (id >> 48) & 0xFF;
			data[current + 2] = (id >> 40) & 0xFF;
			data[current + 3] = (id >> 32) & 0xFF;
			data[current + 4] = (id >> 24) & 0xFF;
			data[current + 5] = (id >> 16) & 0xFF;
			data[current + 6] = (id >> 8) & 0xFF;
			data[current + 7] = id & 0xFF;
		}
	}

	int Message::checksum()
	{
		int chk = 0;
		for (int i = 0; i < size; i++)
			chk += data[i] >> 4;
		return chk;
	}
	
	Message *Message::clone(Message *src, int type, uint64_t id)
	{
		Message *new_message = new Message(src->get_content(), src->get_content_size(), type, id);
		new_message->set_flags(src->get_flags());
		return new_message;
	}
}

