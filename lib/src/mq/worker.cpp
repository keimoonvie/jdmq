#include <boost/lexical_cast.hpp>
#include "../common/logger.h"
#include "worker.h"

using namespace std;

namespace jdmq
{
	Worker::Worker(int id, void *data, string name): id(id), data(data), name(name)
        {
		runner.reset(new WorkerRunner(this));
		string thread_name = name + " " + boost::lexical_cast<string>(id);
                thread = new thread_t(runner, thread_name);
        }

	Worker::~Worker()
	{
		if (thread)
			delete thread;
	}

	void Worker::start()
	{
		JDMQ_LOGGER_INFO("worker", "%s worker %d started", name.c_str(), id);
		thread->start();
	}

	void Worker::stop()
	{
		thread->join();
		JDMQ_LOGGER_INFO("worker", "%s worker %d stopped", name.c_str(), id);
	}

	WorkerRunner::WorkerRunner(Worker *worker): runnable_t(), worker(worker)
	{
	}

	void WorkerRunner::run()
	{
		while (worker->run());
	}
}

