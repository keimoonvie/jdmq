#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "../common/utilities.h"
#include "../common/logger.h"
#include "message.h"
#include "buffer.h"

namespace jdmq
{
	Buffer::Buffer(int initial_size): initial_size(initial_size), max_size(initial_size), 
					  size(0), head(0), current(0), need_resize(false), state(BUFFER_BEGIN)
	{
		stream = (char *) malloc(max_size);
	}

	Buffer::~Buffer()
	{
		if (stream)
			free(stream);
	}

	BufferCode Buffer::read(int fd)
	{
		int count = 0;
                BufferCode code;
                while (true) {
                        count = ::read(fd, stream + size, max_size - size);
                        if (count == -1) {
				if (errno == ECONNRESET)
                                        code = BUFFER_CLOSE;
                                else if (errno != EAGAIN && errno != EWOULDBLOCK && errno != EINTR)
					code = BUFFER_ERROR;
                                else
                                        code = BUFFER_END;
                                break;
                        } else if (count == 0) {
                                code = BUFFER_CLOSE;
                                break;
                        } else {
                                size += count;
                                if (size == max_size) {
                                        code = BUFFER_FULL;
                                        break;
                                }
                        }
                }
                return code;
	}

	MessageCode Buffer::extract_message(message_t **msg)
	{
		unsigned char *ucstream = (unsigned char *) stream + head + current;
		if (state == BUFFER_BEGIN) {
			goto begin;
		} else if (state == BUFFER_READ_SIZE) {
			goto read_size;
		} else if (state == BUFFER_READ_FLAGS) {
			goto read_flags;
		} else if (state == BUFFER_READ_ID) {
			goto read_id;
		} else {
			goto read_content;
		}
	begin:
                current = 0;
	read_size:
		message_size = 0;
		state = BUFFER_READ_SIZE;
                if (size < PACKET_HEADER_MIN_SIZE)
                        return MESSAGE_WAITING;
		//Magic byte
                if (ucstream[0] != (unsigned char) PACKET_MAGIC || ucstream[1] != (unsigned char) PACKET_MAGIC) {
			JDMQ_LOGGER_ERROR("buffer", "Magic byte not match, got: %c, %c", ucstream[0], ucstream[1]);
			return MESSAGE_MAGIC_BYTE_ERROR;
		}
                current += 2;

		//Size
                if (ucstream[2] != (unsigned char) 0xFF || ucstream[3] != (unsigned char) 0xFF) {
                        message_size = (ucstream[2] << 8) + ucstream[3];
                        current += 2;
                } else {
                        message_size = (ucstream[4] << 24) + (ucstream[5] << 16) + (ucstream[6] << 8) + ucstream[7];
			current += 6;
		}
	read_flags:
		state = BUFFER_READ_FLAGS;
		message_type = 0;
		flags = 0;
		if (size < current + 4)
                        return MESSAGE_WAITING;
		//Type and flags
		message_type = (ucstream[current] << 16) + (ucstream[current + 1] << 8) + ucstream[current + 2];
                flags = ucstream[current + 3];
		current += 4;
	read_id:
		state = BUFFER_READ_ID;
		message_id = 0;
		//Message id
                if (flags & 0x2) {
                        if (size < current + 8)
                                return MESSAGE_WAITING;

			for (int i = 0; i < 7; i++) {
                                message_id += ucstream[current + i];
				message_id <<= 8;
                        }
                        message_id += ucstream[current + 7];
                        current += 8;
		}

	read_content:
		state = BUFFER_READ_CONTENT;
		//Content
                if (size < message_size) {
			if (max_size < message_size) {
				int new_size = (message_size / max_size + 1) * max_size;
				char *new_stream = (char *) realloc(stream, new_size);
				if (new_stream == NULL) {
					JDMQ_LOGGER_ERROR("buffer", "Resize error");
					return MESSAGE_MALLOC_ERROR;
				} else {
					stream = new_stream;
				}
				max_size = new_size;
				need_resize = true;
			}
                        return MESSAGE_WAITING;
		}

		void *message_content = ucstream + current;
                int data_size = message_size - current;


		message_t *new_msg = new message_t(message_content, data_size, message_type, message_id);
                if (new_msg == NULL)
                        return MESSAGE_MALLOC_ERROR;
                new_msg->set_flags(flags);
                *msg = new_msg;
                flush(message_size);
		need_resize = false;

		current = 0;
		state = BUFFER_BEGIN;
                return MESSAGE_OK;
	}

	void Buffer::reset()
	{
		char *src = (char *) stream + head;
		memmove(stream, src, size);
		head = 0;
		if (!need_resize && max_size > initial_size) {
			char *new_stream = (char *) realloc(stream, initial_size);
			if (new_stream == NULL) {
				JDMQ_LOGGER_ERROR("buffer", "Resize error");
			} else {
				max_size = initial_size;
				stream = new_stream;
			}
		}
	}

	void Buffer::flush(int message_size)
	{
		size -= message_size;
		head += message_size;
	}
}
