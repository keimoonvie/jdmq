#include "message.h"
#include "watcher.h"
#include "processor.h"

namespace jdmq
{
	ProcessWorker::ProcessWorker(int id, void *data): Worker(id, data, "Proc")
        {
        }

        bool ProcessWorker::run()
	{
		watcher_t *watcher = (watcher_t *) data;
		message_queue_item_t *item = watcher->dequeue_message();
		if (item == NULL)
			return false;

		message_t *message = item->message;
                socket_ptr socket = item->socket;

		watcher->process_message(message, socket);

		delete message;
		delete item;
		return true;
        }
}
