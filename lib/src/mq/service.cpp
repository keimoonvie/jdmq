#include "service.h"

namespace jdmq
{
	Service::Service(): activated(false)
	{
        }

	Service::~Service()
        {
        }

        bool Service::activate()
        {
		activated = true;
		return true;
        }

	bool Service::deactivate()
        {
                activated = false;
		return true;
	}
}
