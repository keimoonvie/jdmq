#ifndef JDMQ_MQ_WATCHER_H_
#define JDMQ_MQ_WATCHER_H_

#include <string>
#include <google/sparse_hash_map>
#include "../common/common.h"
#include "../common/scoped_lock.h"
#include "../common/event.h"
#include "../common/queue.h"
#include "mq.h"

namespace jdmq
{
	class Socket;

	class message_queue_item_t
	{
	public:
		message_t *message;
		socket_ptr socket;
	};

	class Watcher: public event_t
	{
	public:
		Watcher();
		virtual ~Watcher();

		void add_interface(const std::string & host, const std::string & port, bool timeout = false);
		bool watch(int fd, const std::string & host, const std::string & port, int interface_fd = 0);
		bool close(socket_ptr socket);
		bool _close(socket_ptr socket);
		void active(socket_ptr socket);
		socket_ptr get_socket(int fd);
		void destroy_idle();

		void serve();
                void finalize();

		void create_io_workers(int size = 0);
                void create_process_workers(int size = 0);
                void create_cron_workers(int size = 0);
		void create_spare_workers(int size = 0);

		void file_event_handler(int fd, int mask);
                void timer_handler(timer_t *timer);

		void add_service(service_t *service, bool activate = true);
		void add_spare_service(service_t *service, bool activate = true);
		void process_message(message_t *message, socket_ptr socket);
		void process_spare_message(message_t *message, socket_ptr socket);
		void set_watcher_event(watcher_event_t *new_watcher_event);

		socket_ptr dequeue_event();
		bool enqueue_message(message_t *message, socket_ptr socket);
		message_queue_item_t *dequeue_message();
		timer_t *dequeue_timer();
		bool enqueue_spare_message(message_t *message, socket_ptr socket);
		message_queue_item_t *dequeue_spare_message();

		inline void set_socket_buffer_size(int buffer_size)
		{
			this->socket_buffer_size = buffer_size;
		}

		inline void set_socket_pipe_size(int pipe_size)
		{
			this->socket_pipe_size = pipe_size;
		}

	private:
		struct interface_info_t
		{
			std::string host;
			std::string port;
			int timeout;
		};

		typedef google::sparse_hash_map<int, socket_ptr> socket_map_t;
		typedef google::sparse_hash_map<int, interface_t *> interface_map_t;
		typedef std::vector<worker_t *> worker_list_t;
		typedef std::vector<service_t *> service_list_t;

		int socket_buffer_size;
		int socket_pipe_size;

		socket_map_t sockets;
		interface_map_t interfaces;
		std::vector<interface_info_t> interface_infos;
		uint64_t socket_id;
		mutex_t socket_mutex;

		queue_t *event_queue;
		queue_t *message_queue;
		queue_t *timer_queue;
		queue_t *spare_queue;

		worker_list_t io_workers;
		worker_list_t process_workers;
		worker_list_t cron_workers;
		worker_list_t spare_workers;

		service_list_t services;
		service_list_t spare_services;
		watcher_event_t *watcher_event;

		bool enqueue_event(int fd);
	};

	typedef Watcher watcher_t;
}

#endif
