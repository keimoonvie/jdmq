#ifndef JDMQ_MQ_SERVICE_H_
#define JDMQ_MQ_SERVICE_H_

#include "../common/common.h"
#include "mq.h"

namespace jdmq
{
	class Service
	{
	public:
                Service();
		virtual ~Service();

                inline bool is_activated() const
                {
			return activated;
		}

                virtual bool activate();
		virtual bool deactivate();
                virtual bool process(message_t *message, socket_ptr socket) = 0;

	protected:
                bool activated;
		
	};
}

#endif
