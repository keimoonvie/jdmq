#include <math.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include "../common/logger.h"
#include "../common/utilities.h"
#include "watcher.h"
#include "pipe.h"

namespace jdmq
{
	Pipe::Pipe(int fd, watcher_t *watcher, int max_size): fd(fd), watcher(watcher), max_size(max_size), initial_size(max_size), size(0), head(0), state(PIPE_STATE_WAIT_WRITE)
	{
		stream = (char *) malloc(max_size);
	}

	Pipe::~Pipe()
	{
		if (stream)
			free(stream);
	}

	bool Pipe::send(const char *data, int data_size)
	{
		jdmq_synchronize(mutex);
		if (state == PIPE_STATE_WAIT_WRITE) {
			//Write immediately
			int sent = 0;
			int count = 0;
			while (true) {
				count = write(data + sent, data_size - sent);
				if (count == -1) {
					if (errno != EAGAIN && errno != EWOULDBLOCK && errno != EINTR) {
						JDMQ_LOGGER_ERROR("pipe", "Pipe write fail on socket %d, errno: %d (%s)", fd, errno, strerror(errno));
						return false;
					}
					break;
				} else {
					sent += count;
					if (sent == data_size)
						break;
				}
			}
			if (sent < data_size) {
				state = PIPE_STATE_WAIT_EVENT;
				watcher->add_file_event(fd, Event::EVENT_WRITE | Event::EVENT_ONESHOT);
				//Append to stream
				int append_size = data_size - sent;
				return append(data + sent, data_size - sent);
			} else {
				return true;
			}
		} else {
			//Waiting for event, just append to stream
			return append(data, data_size);
		}
	}

	bool Pipe::open_gate()
	{
		jdmq_synchronize(mutex);
		if (state == PIPE_STATE_WAIT_WRITE)
			return true;
		//Write
		int sent = 0;
		int count = 0;
		while (true) {
			count = write(stream + head + sent, size - sent);
			if (count == -1) {
				if (errno != EAGAIN && errno != EWOULDBLOCK && errno != EINTR) {
					JDMQ_LOGGER_ERROR("pipe", "Pipe write fail on socket %d, errno: %d (%s)", fd, errno, strerror(errno));
					return false;
				}
				break;
			} else {
				sent += count;
				if (sent == size)
					break;
			}
		}
		if (sent == size)
			state = PIPE_STATE_WAIT_WRITE;
		else
			watcher->add_file_event(fd, Event::EVENT_WRITE | Event::EVENT_ONESHOT);
		//Reset, realloc, etc...
		head += sent;
		size -= sent;
		if (head > size) {
			//Above 50% space available
			reset();
			int new_size = size < initial_size ? initial_size : max_size / 2;
			char *new_stream = (char *) realloc(stream, new_size);
			if (new_stream == NULL) {
				JDMQ_LOGGER_ERROR("pipe", "Cannot realloc stream");
				return false;
			}
			stream = new_stream;
			max_size = new_size;
		}
		return true;
	}

	bool Pipe::append(const char *data, int data_size)
	{
		int tail = head + size;
		if (tail + data_size > max_size) {
			if (size + data_size < max_size) {
				reset();
			} else {
				//Shit, just expand like hell
				int new_size = max_size + data_size * 2; 
				char *new_stream = (char *) realloc(stream, new_size);
				if (new_stream == NULL) {
					JDMQ_LOGGER_ERROR("pipe", "Cannot realloc stream");
					return false;
				}
				stream = new_stream;
				max_size = new_size;
			}
		}
		tail = head + size;
		memcpy(stream + tail, data, data_size);
		size += data_size;
		return true;
	}

	void Pipe::reset()
	{
		memmove(stream, stream + head, size);
		head = 0;
	}

	int Pipe::write(const char *data, int dsize)
	{
		return ::send(fd, data, dsize, MSG_NOSIGNAL);
	}
}
