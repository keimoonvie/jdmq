#include "../common/logger.h"
#include "socket.h"
#include "watcher.h"
#include "io.h"

namespace jdmq
{
	IoWorker::IoWorker(int id, void *data): Worker(id, data, "Io")
        {
        }

        bool IoWorker::run()
	{
		watcher_t *watcher = (watcher_t *) data;
		socket_ptr socket = watcher->dequeue_event();
		if (socket == NULL)
			return false;
		if (socket->process() >= SOCKET_CLOSE_WAIT) {
			watcher->_close(socket);
		} else
			socket->rearm();
		return true;
        }
}
