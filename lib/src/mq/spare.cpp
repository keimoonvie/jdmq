#include "watcher.h"
#include "message.h"
#include "spare.h"

namespace jdmq
{
	SpareWorker::SpareWorker(int id, void *data): Worker(id, data, "Spare")
        {
        }

        bool SpareWorker::run()
	{
		watcher_t *watcher = (watcher_t *) data;
		message_queue_item_t *item = watcher->dequeue_spare_message();
                if (item == NULL)
			return false;

		message_t *message = item->message;
		socket_ptr socket = item->socket;

                watcher->process_spare_message(message, socket);

		delete message;
		delete item;
		return true;
        }
}
