#ifndef JDMQ_MQ_CRON_H_
#define JDMQ_MQ_CRON_H_

#include "mq.h"
#include "worker.h"

namespace jdmq
{
	class CronWorker: public Worker
	{
	public:
		CronWorker(int id, void *data);
		bool run();
	};
}

#endif
