#include <errno.h>
#include <string.h>
#include <sstream>
#include "../common/logger.h"
#include "../common/utilities.h"
#include "watcher.h"
#include "message.h"
#include "socket.h"

using namespace std;

namespace jdmq
{
	Socket::Socket(uint64_t id, int fd, watcher_t *watcher, const string & host, const string & port, int buffer_size, int pipe_size):
		id(id), fd(fd), interface_fd(0), watcher(watcher), host(host), port(port), state(SOCKET_RUNNING)
	{
		init_time = Utilities::get_current_time();
		buffer = new Buffer(buffer_size);
		pipe = new Pipe(fd, watcher, pipe_size);
	}

	Socket::~Socket()
	{
		if (buffer)
			delete buffer;
		if (pipe)
			delete pipe;
	}

	SocketState Socket::process()
	{
		jdmq_synchronize(state_mutex);
		if (state != SOCKET_RUNNING)
			return state;
		if (!process_read()) {
                        advance_state(SOCKET_CLOSE_WAIT);
		} else if (!process_write()) {
                        advance_state(SOCKET_CLOSE_WAIT);
                }
		return state;
	}

	int Socket::send(message_t *msg, bool force_normal)
	{
		if (msg->check_flag(6) && !force_normal)
			msg->set_flag(7);
		jdmq_synchronize(state_mutex);
		if (state != SOCKET_RUNNING)
			return -1;
		return pipe->send(msg->get_data(), msg->get_size()) ? msg->get_size() : -1;
	}

	bool Socket::is_closed()
	{
		return state == SOCKET_CLOSED;
	}

	bool Socket::close()
	{
		jdmq_synchronize(state_mutex);
		if (state != SOCKET_CLOSED) {
			JDMQ_LOGGER_DEBUG("socket", "Closing socket %d", fd);
                        ::close(fd);
                        state = SOCKET_CLOSED;
			return true;
		} else {
			return false;
		}
	}

	string Socket::to_string() const
	{
		stringstream ss;
                ss << fd;
                ss << " (";
                ss << host;
                ss << ", ";
                ss << port;
                ss << ")";
                return ss.str();
	}

	void Socket::rearm()
	{
		jdmq_synchronize(state_mutex);
		if (state == SOCKET_RUNNING)
			watcher->add_file_event(fd, Event::EVENT_READ | Event::EVENT_ONESHOT);
	}

	void Socket::advance_state(SocketState to_state)
	{
                if ((state == SOCKET_RUNNING && to_state > SOCKET_RUNNING) || (state == SOCKET_CLOSE_WAIT && to_state > SOCKET_CLOSE_WAIT))
                        state = to_state;
	}

	bool Socket::process_read()
	{
		socket_ptr this_socket = watcher->get_socket(fd);
                if (this_socket == NULL) {
                        JDMQ_LOGGER_ERROR("socket", "Socket not found: %s", to_string().c_str());
			return false;
		}
		if (!buffer)
			return false;
		bool success = true;
		BufferCode code = buffer->read(fd);
		if (code == BUFFER_ERROR || code == BUFFER_CLOSE) {
			if (code == BUFFER_ERROR)
				JDMQ_LOGGER_ERROR("socket", "Read error on socket %s, errno: %d (%s)", to_string().c_str(), errno, strerror(errno));
			success = false;
		}
		message_t *message = NULL;
		MessageCode msg_code;
                while (true) {
                        msg_code = buffer->extract_message(&message);
                        if (msg_code == MESSAGE_WAITING) {
                                break;
                        } else if (msg_code != MESSAGE_OK) {
                                JDMQ_LOGGER_ERROR("socket", "Read error on socket %s, code: %d", to_string().c_str(), msg_code);
				success = false;
				break;
                        } else {
				if (message->check_flag(7))
					watcher->enqueue_spare_message(message, this_socket);
				else
					watcher->enqueue_message(message, this_socket);
                        }
                }
		if (success)
			buffer->reset();
		return success;
	}

	bool Socket::process_write()
	{
		return pipe->open_gate();
	}
}
