#ifndef JDMQ_MQ_PROCESSOR_H_
#define JDMQ_MQ_PROCESSOR_H_

#include "mq.h"
#include "worker.h"

namespace jdmq
{
	class ProcessWorker: public Worker
	{
	public:
		ProcessWorker(int id, void *data);
		bool run();
	};
}

#endif
