#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>
#include "../common/logger.h"
#include "../common/socket_utils.h"
#include "interface.h"
#include "socket.h"
#include "io.h"
#include "processor.h"
#include "cron.h"
#include "spare.h"
#include "service.h"
#include "watcher_event.h"
#include "watcher.h"

using namespace std;

namespace jdmq
{
	Watcher::Watcher(): Event()
	{
		sockets.set_deleted_key(-1);
		interfaces.set_deleted_key(-1);
		event_queue = new FIFOQueue();
		message_queue = new FIFOQueue();
		timer_queue = new FIFOQueue();
		spare_queue = new FIFOQueue();
		socket_id = 0;
		socket_buffer_size = 1024;
		socket_pipe_size = 1024;
		watcher_event = new NullWatcherEvent();
		watcher_event->set_watcher(this);
	}

	Watcher::~Watcher()
	{
		for (int i = 0; i < interfaces.size(); i++)
			delete interfaces[i];
		for (int i = 0; i < io_workers.size(); i++)
			delete io_workers[i];
		for (int i = 0; i < process_workers.size(); i++)
			delete process_workers[i];
		for (int i = 0; i < cron_workers.size(); i++)
			delete cron_workers[i];
		for (int i = 0; i < spare_workers.size(); i++)
			delete spare_workers[i];
		for (int i = 0; i < services.size(); i++)
			delete services[i];
		for (int i = 0; i < spare_services.size(); i++)
                        delete spare_services[i];
		delete event_queue;
		delete message_queue;
		delete timer_queue;
		delete spare_queue;
		delete watcher_event;
	}

	void Watcher::add_interface(const string & host, const string & port, bool timeout)
	{
		if (running) {
			int fd = SocketUtils::socket_new_listener(host.c_str(), port.c_str());
			if (fd < 0) {
				JDMQ_LOGGER_ERROR("watcher", "Cannot bind to interface: %s:%s", host.c_str(), port.c_str());
			} else {
				if (!add_file_event(fd, EVENT_READ | EVENT_WRITE)) {
					JDMQ_LOGGER_ERROR("watcher", "Cannot watch interface: %s:%s", host.c_str(), port.c_str());
					::close(fd);
				} else {
					interface_t *interface = new interface_t(this, host, port, timeout);
					interface->set_fd(fd);
					interfaces[fd] = interface;
					JDMQ_LOGGER_INFO("watcher", "Interface added: %d (%s:%s), %d", fd, host.c_str(), port.c_str(), timeout);
				}
			}
		} else {
			interface_info_t info = {host, port, timeout};
			interface_infos.push_back(info);
		}
	}

	bool Watcher::watch(int fd, const string & host, const string & port, int interface_fd)
	{
		{
			jdmq_synchronize(socket_mutex);
			if (sockets.find(fd) != sockets.end()) {
				JDMQ_LOGGER_ERROR("watcher", "Socket existed: %d", fd);
				return false;
			}
		}
		if (!add_file_event(fd, EVENT_READ | EVENT_ONESHOT)) {
			JDMQ_LOGGER_ERROR("watcher", "Cannot add socket event: %d", fd);
			::close(fd);
			return false;
		}
		{
			jdmq_synchronize(socket_mutex);
			socket_id++;
			sockets[fd] = socket_ptr(new Socket(socket_id, fd, this, host, port, socket_buffer_size, socket_pipe_size));
			if (interface_fd > 0) {
				if (interfaces.find(interface_fd) != interfaces.end()) {
					interfaces[interface_fd]->add_socket(fd);
					sockets[fd]->set_interface_fd(interface_fd);
				}
			}
		}
		return true;
	}

	bool Watcher::close(socket_ptr socket)
	{
		{
			jdmq_synchronize(socket_mutex);
			delete_file_event(socket->get_fd());
			if (sockets.find(socket->get_fd()) != sockets.end())
				sockets.erase(socket->get_fd());
			if (socket->get_interface_fd() > 0 && interfaces.find(socket->get_interface_fd()) != interfaces.end())
				interfaces[socket->get_interface_fd()]->remove_socket(socket->get_fd());
		}

		return socket->close();
	}

	bool Watcher::_close(socket_ptr socket)
	{
		{
			jdmq_synchronize(socket_mutex);
			delete_file_event(socket->get_fd());
                        if (sockets.find(socket->get_fd()) != sockets.end())
                                sockets.erase(socket->get_fd());
			if (socket->get_interface_fd() > 0 && interfaces.find(socket->get_interface_fd()) != interfaces.end())
				interfaces[socket->get_interface_fd()]->remove_socket(socket->get_fd());
		}
		if (!socket->close()) {
			return false;
		}
		watcher_event->on_close(socket);
		return true;
	}

	void Watcher::active(socket_ptr socket)
	{
		jdmq_synchronize(socket_mutex);
		if (socket->get_interface_fd() > 0 && interfaces.find(socket->get_interface_fd()) != interfaces.end())
			interfaces[socket->get_interface_fd()]->active_socket(socket->get_fd());
	}

	socket_ptr Watcher::get_socket(int fd)
	{
		if (fd == -1)
			return socket_ptr();
                jdmq_synchronize(socket_mutex);
                if (sockets.find(fd) != sockets.end())
                        return sockets[fd];
                else
                        return socket_ptr();
	}

	void Watcher::destroy_idle()
	{
		jdmq_synchronize(socket_mutex);
		for (interface_map_t::iterator it = interfaces.begin(); it != interfaces.end(); it++) {
			socket_set_t *black = it->second->get_black_list();
			for (socket_set_t::iterator it2 = black->begin(); it2 != black->end(); it2++) {
				if (sockets.find(*it2) != sockets.end()) {
					sockets[*it2]->close();
					sockets.erase(*it2);
				}
			}
			it->second->destroy();
		}
	}

	void Watcher::file_event_handler(int fd, int mask)
	{
		if (!running)
			return;
		int infd;
                struct sockaddr in_addr;
                socklen_t in_len = sizeof(in_addr);
                char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];
		if (interfaces.find(fd) != interfaces.end()) {
			while (true) {
				infd = accept(fd, &in_addr, &in_len);
				if (infd == -1) {
					if (errno != EAGAIN && errno != EWOULDBLOCK && errno != EINTR)
                                                JDMQ_LOGGER_ERROR("watcher", "Accept error on socket %d, errno: %d (%s)", fd, errno, strerror(errno));
                                        break;
				}
				if (!SocketUtils::socket_make_non_blocking(infd)) {
                                        ::close(infd);
                                        JDMQ_LOGGER_ERROR("watcher", "Cannot make socket non blocking: %d", infd);
					continue;
                                }
				if (!SocketUtils::socket_make_no_delay(infd)) {
					::close(infd);
                                        JDMQ_LOGGER_ERROR("watcher", "Cannot make socket no delay: %d", infd);
					continue;
				}
				if (getnameinfo(&in_addr, in_len, hbuf, sizeof(hbuf), sbuf, sizeof(sbuf), NI_NUMERICHOST | NI_NUMERICSERV) != 0) {
                                        ::close(infd);
                                        JDMQ_LOGGER_ERROR("watcher", "Cannot get address info: %d", infd);
                                        continue;
                                }
				if (!watch(infd, hbuf, sbuf, fd)) {
					JDMQ_LOGGER_ERROR("watcher", "Cannot watch socket: %d", infd);
				} else {
					JDMQ_LOGGER_INFO("watcher", "Accepted connection from %d (%s:%s)", infd, hbuf, sbuf);
					watcher_event->on_accept(get_socket(infd));
				}
			}
		} else {
			if (mask & Event::EVENT_ERROR) {
				JDMQ_LOGGER_ERROR("watcher", "Event error on socket: %d, errno: %d (%s)", fd, errno, strerror(errno));
				socket_ptr socket = get_socket(fd);
                                if (socket != NULL)
                                        _close(socket);
			} else {
				if (!enqueue_event(fd)) {
					socket_ptr socket = get_socket(fd);
					if (socket != NULL)
						_close(socket);
				}
			}
		}
	}

	void Watcher::timer_handler(timer_t *timer)
	{
		if (!running)
                        return;
                if (!timer_queue->enqueue(timer))
                        JDMQ_LOGGER_ERROR("timer", "Cannot enqueue timer");
	}

	void Watcher::serve()
	{
		if (!event_queue->init() || !message_queue->init() || !timer_queue->init() || !spare_queue->init()) {
			JDMQ_LOGGER_ERROR("watcher", "Init queue fail");
			return;
		}
		if (io_workers.size() <= 0)
			create_io_workers();
		for (int i = 0; i < io_workers.size(); i++)
                        io_workers[i]->start();
                if (process_workers.size() <= 0)
                        create_process_workers();
                for (int i = 0; i < process_workers.size(); i++)
			process_workers[i]->start();
		if (cron_workers.size() <= 0)
			create_cron_workers();
		for (int i = 0; i < cron_workers.size(); i++)
                        cron_workers[i]->start();
		if (spare_workers.size() <= 0)
			create_spare_workers();
		for (int i = 0; i < spare_workers.size(); i++)
			spare_workers[i]->start();
		for (int i = 0; i < interface_infos.size(); i++) {
			int fd = SocketUtils::socket_new_listener(interface_infos[i].host.c_str(), interface_infos[i].port.c_str(), interface_infos[i].timeout);
			if (fd < 0) {
                                JDMQ_LOGGER_ERROR("watcher", "Cannot bind to interface %s:%s", interface_infos[i].host.c_str(),
                                                  interface_infos[i].port.c_str());
			} else {
                                if (add_file_event(fd, EVENT_READ | EVENT_WRITE)) {
					interface_t *interface = new interface_t(this, interface_infos[i].host, interface_infos[i].port, interface_infos[i].timeout);
                                        interface->set_fd(fd);
					interfaces[fd] = interface;
                                        JDMQ_LOGGER_INFO("watcher", "Interface added: %d (%s:%s)", fd, interface_infos[i].host.c_str(),
							 interface_infos[i].port.c_str());
                                }
                        }
                }
                loop();
	}

	void Watcher::finalize()
	{
		stop();

		event_queue->finalize();
		message_queue->finalize();
                timer_queue->finalize();
		spare_queue->finalize();
		
		for (int i = 0; i < io_workers.size(); i++)
                        io_workers[i]->stop();
		for (int i = 0; i < process_workers.size(); i++)
                        process_workers[i]->stop();
		for (int i = 0; i < cron_workers.size(); i++)
			cron_workers[i]->stop();
		for (int i = 0; i < spare_workers.size(); i++)
                        spare_workers[i]->stop();

		for (int i = 0; i < services.size(); i++)
			services[i]->deactivate();

		for (interface_map_t::iterator it = interfaces.begin(); it != interfaces.end(); it++)
			::close(it->first);
	}

	void Watcher::create_io_workers(int size)
	{
		int worker_size = size > 0 ? size : 1;
		for (int i = 0; i < worker_size; i++) {
			worker_t *worker = new IoWorker(i, this);
			io_workers.push_back(worker);
		}
		JDMQ_LOGGER_INFO("watcher", "Created %d io workers", worker_size);
	}

	void Watcher::create_process_workers(int size)
	{
		int worker_size = size > 0 ? size : 4;
                for (int i = 0; i < worker_size; i++) {
                        worker_t *worker = new ProcessWorker(i, this);
			process_workers.push_back(worker);
                }
                JDMQ_LOGGER_INFO("watcher", "Created %d process workers", worker_size);
	}

	void Watcher::create_cron_workers(int size)
	{
		int worker_size = size > 0 ? size : 1;
                for (int i = 0; i < worker_size; i++) {
                        worker_t *worker = new CronWorker(i, this);
			cron_workers.push_back(worker);
                }
                JDMQ_LOGGER_INFO("watcher", "Created %d cron workers", worker_size);
	}

	void Watcher::create_spare_workers(int size)
	{
		int worker_size = size > 0 ? size : 1;
                for (int i = 0; i < worker_size; i++) {
                        worker_t *worker = new SpareWorker(i, this);
			spare_workers.push_back(worker);
                }
                JDMQ_LOGGER_INFO("watcher", "Created %d spare workers", worker_size);
	}

	void Watcher::add_service(service_t *service, bool activate)
        {
                if (activate)
                        service->activate();
                services.push_back(service);
        }

	void Watcher::add_spare_service(service_t *service, bool activate)
	{
		if (activate)
			service->activate();
		spare_services.push_back(service);
	}

	void Watcher::set_watcher_event(watcher_event_t *new_watcher_event)
	{
		if (watcher_event)
			delete watcher_event;
		watcher_event = new_watcher_event;
		watcher_event->set_watcher(this);
	}

	void Watcher::process_message(message_t *message, socket_ptr socket)
	{
		for (int i = 0; i < services.size(); i++) {
			if (services[i]->is_activated() && services[i]->process(message, socket))
				break;
		}
	}

	void Watcher::process_spare_message(message_t *message, socket_ptr socket)
	{
                for (int i = 0; i < spare_services.size(); i++) {
                        if (spare_services[i]->is_activated() && spare_services[i]->process(message, socket))
                                break;
                }
        }

	bool Watcher::enqueue_event(int fd)
	{
                socket_ptr socket = get_socket(fd);
		if (socket == NULL)
                        return false;

		int *fd_ptr = (int *) malloc(sizeof(int));
		if (fd_ptr == NULL)
			return false;

		*fd_ptr = fd;
		return event_queue->enqueue(fd_ptr);
        }

	socket_ptr Watcher::dequeue_event()
	{
		int *fd_ptr = (int *) event_queue->dequeue();
		if (fd_ptr == NULL)
			return socket_ptr();
		socket_ptr socket = get_socket(*fd_ptr);
		free(fd_ptr);
		return socket;
	}

	bool Watcher::enqueue_message(message_t *message, socket_ptr socket)
	{
		message_queue_item_t *item = new message_queue_item_t();
		if (item == NULL)
			return false;
		item->message = message;
		item->socket = socket;
		return message_queue->enqueue(item);
	}

	message_queue_item_t *Watcher::dequeue_message()
	{
		return (message_queue_item_t *) message_queue->dequeue();
	}

	timer_t *Watcher::dequeue_timer()
	{
		return (timer_t *) timer_queue->dequeue();
	}

	bool Watcher::enqueue_spare_message(message_t *message, socket_ptr socket)
	{
		message_queue_item_t *item = new message_queue_item_t();
		if (item == NULL)
                        return false;
		item->message = message;
		item->socket = socket;
		return spare_queue->enqueue(item);
	}

	message_queue_item_t *Watcher::dequeue_spare_message()
	{
		return (message_queue_item_t *) spare_queue->dequeue();
	}
}
