#ifndef JDMQ_MQ_SPARE_H_
#define JDMQ_MQ_SPARE_H_

#include "mq.h"
#include "worker.h"

namespace jdmq
{
	class SpareWorker: public Worker
	{
	public:
		SpareWorker(int id, void *data);
		bool run();
	};
}

#endif
