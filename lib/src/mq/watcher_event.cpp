#include "socket.h"
#include "watcher.h"
#include "watcher_event.h"

namespace jdmq
{
	WatcherEvent::WatcherEvent()
	{
	}

	WatcherEvent::~WatcherEvent()
	{
	}

	void WatcherEvent::set_watcher(watcher_t *watcher)
	{
		this->watcher = watcher;
	}

	void NullWatcherEvent::on_accept(socket_ptr socket)
	{
	}

	void NullWatcherEvent::on_close(socket_ptr socket)
	{
	}
}
