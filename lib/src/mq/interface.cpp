#include "../common/logger.h"
#include "interface.h"

using namespace std;

namespace jdmq
{
	Interface::Interface(watcher_t *watcher, const string & host, const string & port, bool timeout):
		watcher(watcher), host(host), port(port), timeout(timeout)
	{
		black = new socket_set_t();
		grey = new socket_set_t();
		white = new socket_set_t();
		black->set_deleted_key(0);
		grey->set_deleted_key(0);
		white->set_deleted_key(0);
	}

	Interface::~Interface()
	{
		delete black;
		delete grey;
		delete white;
	}

	void Interface::add_socket(int socket_fd)
	{
		if (timeout) {
			white->insert(socket_fd);
		}
	}

	void Interface::remove_socket(int socket_fd)
	{
		if (timeout) {
			if (white->find(socket_fd) != white->end())
				white->erase(socket_fd);
			else if (grey->find(socket_fd) != grey->end())
				grey->erase(socket_fd);
			else if (black->find(socket_fd) != black->end())
				black->erase(socket_fd);
		}
	}

	void Interface::active_socket(int socket_fd)
	{
		if (timeout) {
			if (white->find(socket_fd) != white->end())
				white->erase(socket_fd);
			else if (grey->find(socket_fd) != grey->end())
				grey->erase(socket_fd);
		}
	}

	void Interface::destroy()
	{
		if (timeout) {
			black->clear();
			socket_set_t *tmp = black;
			black = grey;
			grey = white;
			white = tmp;
		}
	}
}
