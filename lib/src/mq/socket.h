#ifndef JDMQ_MQ_SOCKET_H_
#define JDMQ_MQ_SOCKET_H_

#include <stdint.h>
#include <string>
#include "../common/common.h"
#include "../common/scoped_lock.h"
#include "buffer.h"
#include "pipe.h"
#include "mq.h"

namespace jdmq
{
	class Socket
	{
	public:
		Socket(uint64_t id, int fd, watcher_t *watcher, const std::string & host, const std::string & port, int buffer_size = 1024, int pipe_size = 1024);
		virtual ~Socket();
		
		SocketState process();
		int send(message_t *msg, bool force_normal = false);
		bool is_closed();
                bool close();
		std::string to_string() const;
		void rearm();
		
		inline uint64_t get_id() const
		{
			return id;
		}

		inline int get_fd() const
                {
                        return fd;
                }

                inline const std::string & get_host() const
                {
                        return host;
                }

                inline const std::string & get_port() const
                {
                        return port;
                }

                inline int get_init_time() const
                {
                        return init_time;
                }

		inline void set_interface_fd(int interface_fd)
		{
			this->interface_fd = interface_fd;
		}

		inline int get_interface_fd() const
		{
			return interface_fd;
		}
	private:
		uint64_t id;
		int fd;
		int interface_fd;
                watcher_t *watcher;
		
		std::string host;
		std::string port;

		int init_time;
		SocketState state;
		mutex_t state_mutex;

		Buffer *buffer;
		Pipe *pipe;

		void advance_state(SocketState to_state);
                bool process_read();
                bool process_write();
	};
}

#endif
