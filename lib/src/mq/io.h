#ifndef JDMQ_MQ_IO_H_
#define JDMQ_MQ_IO_H_

#include "mq.h"
#include "worker.h"

namespace jdmq
{
	class IoWorker: public Worker
	{
	public:
		IoWorker(int id, void *data);
		bool run();
	};
}

#endif
