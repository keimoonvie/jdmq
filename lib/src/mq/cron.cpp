#include "watcher.h"
#include "cron.h"

namespace jdmq
{
	CronWorker::CronWorker(int id, void *data): Worker(id, data, "Cron")
        {
        }

        bool CronWorker::run()
	{
		watcher_t *watcher = (watcher_t *) data;
		timer_t *timer = watcher->dequeue_timer();
		if (timer == NULL)
			return false;
		timer->run();
		return true;
        }
}
