/*
 * Utilities for socket.
 */
#ifndef JDMQ_COMMON_SOCKET_UTIL_H_
#define JDMQ_COMMON_SOCKET_UTIL_H_

#define JDMQ_BACKLOG 256

#include <jdmq/common.h>

namespace jdmq
{
	class SocketUtils
	{
	public:
		//Bind and listen on an address.
		static int socket_new_listener(const char *host, const char *port, int backlog = JDMQ_BACKLOG, bool non_blocking = true, bool no_delay = true);

		//Make a socket nonblocking
		static bool socket_make_non_blocking(int socket_fd);

		//Disable nagle algorithm
		static bool socket_make_no_delay(int socket_fd);

		//Socket connect
		static int socket_new_connection(const char *host, const char *port);
	};
}

#endif
