#ifndef JDMQ_INTERFACE_H_
#define JDMQ_INTERFACE_H_

#include <string>
#include <google/sparse_hash_set>
#include <jdmq/common.h>
#include <jdmq/mq.h>

namespace jdmq
{
	typedef google::sparse_hash_set<int> socket_set_t;
	class Interface
	{
	public:
		Interface(watcher_t *watcher, const std::string & host, const std::string & port, bool timeout = false);
		virtual ~Interface();

		void add_socket(int socket_fd);
		void remove_socket(int socket_fd);
		void active_socket(int socket_fd);
		void destroy();

		inline const std::string & get_host() const 
		{
			return host;
		}

		inline const std::string & get_port() const
		{
			return port;
		}

		inline void set_fd(int fd)
		{
			this->fd = fd;
		}

		inline int get_fd() const
		{
			return fd;
		}

		inline socket_set_t *get_black_list()
		{
			return black;
		}

	private:
		std::string host;
		std::string port;
		int fd;
		watcher_t *watcher;
		bool timeout;
		socket_set_t *black;
		socket_set_t *grey;
		socket_set_t *white;
	};
}

#endif
