/*
 * Abstract worker
 */
#ifndef JDMQ_MQ_WORKER_H_
#define JDMQ_MQ_WORKER_H_

#include <string>
#include <jdmq/common.h>
#include <jdmq/thread.h>

namespace jdmq
{
	class Worker
	{
	public:
		Worker(int id, void *data, std::string name = "Generic");
		virtual ~Worker();

		void start();
		void stop();

		virtual bool run() = 0;
	protected:
		void *data;

		thread_t *thread;
		runnable_ptr runner;

		int id;
		std::string name;
	};

	class WorkerRunner: public runnable_t
	{
	public:
		WorkerRunner(Worker *worker);

		virtual void run();
	private:
		Worker *worker;
	};

}

#endif
