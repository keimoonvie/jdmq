/*
 * Common utilities.
 */
#ifndef JDMQ_COMMON_UTILITIES_H_
#define JDMQ_COMMON_UTILITIES_H_

#include <stdint.h>
#include <string>
#include <vector>
#include <set>
#include <jdmq/common.h>
#include <jdmq/mutex.h>

namespace jdmq
{
	class Utilities
	{
	public:
		static void print_mem(const void * data, size_t len);

		static std::string make_string(int32_t val);
		static std::string make_string(uint32_t val);
		static std::string make_string(int64_t val);
		static std::string make_string(uint64_t val);
		static uint32_t str_to_uint32(const std::string & str);
                static std::string uint32_to_str(uint32_t val);
		static uint64_t str_to_uint64(const std::string & str);
                static std::string uint64_to_str(uint64_t val);

		static void str_vector_to_uint32_vector(const std::vector<std::string> & str_vector, std::vector<uint32_t> & uint32_vector);
                static void uint32_vector_to_str_vector(const std::vector<uint32_t> & uint32_vector, std::vector<std::string> & str_vector);
                static void str_vector_to_uint64_vector(const std::vector<std::string> & str_vector, std::vector<uint64_t> & uint64_vector);
                static void uint64_vector_to_str_vector(const std::vector<uint64_t> & uint64_vector, std::vector<std::string> & str_vector);

                static void str_set_to_uint32_set(const std::set<std::string> & str_set, std::set<uint32_t> & uint32_set);
                static void uint32_set_to_str_set(const std::set<uint32_t> & uint32_set, std::set<std::string> & str_set);
		static void str_set_to_uint64_set(const std::set<std::string> & str_set, std::set<uint64_t> & uint64_set);
                static void uint64_set_to_str_set(const std::set<uint64_t> & uint64_set, std::set<std::string> & str_set);

		static int get_current_time();
		static int64_t get_current_time_micro();
		static void get_current_time_as_string(char *str);

		static void zeromem(void *mem, int size);
		static void mem_append(void *dest, int dest_size, const void *src, int src_size);

		static bool register_interrupt_cb(void (*handler)(int));
		static bool register_abnormal_cb(void (*handler)(int));
		static bool register_reload_cb(void (*handler)(int));
		static std::vector<std::string> get_stack_traces(int size);
		
	private:
		static mutex_t print_mem_mutex;
	};
}

#endif
