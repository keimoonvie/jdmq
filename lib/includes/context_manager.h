#ifndef JDMQ_PROTOCOL_CONTEXT_MANAGER_H_
#define JDMQ_PROTOCOL_CONTEXT_MANAGER_H_

#include <jdmq/common.h>
#include <jdmq/scoped_lock.h>
#include <jdmq/mq.h>
#include <jdmq/context.h>

namespace jdmq
{
	class ContextManager
	{
	public:
		static ContextManager & get_instance();
		virtual ~ContextManager();

		void add_context(context_ptr context, socket_ptr socket);
		context_ptr seize_context(uint64_t id, socket_ptr socket);
		void destroy_contexts();
	private:
		ContextManager();
		ContextManager(const ContextManager & rhs);
		const ContextManager & operator=(const ContextManager & rhs);

		std::string hash_key(uint64_t id, socket_ptr socket);

		context_map_t *white_list;
		context_map_t *grey_list;
		context_map_t *black_list;
		mutex_t mutex;
	};
}

#endif
