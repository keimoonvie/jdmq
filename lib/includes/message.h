/*
 * Message class.
 * Protocol:
 * message ::= <magic> <magic> <size> <type> <flags> <id> <data>
 * magic ::= 0xEE
 * if size < 65535l then size ::= <octet>{2}
 * else size ::= OxFF 0xFF <octet>{4}
 * type ::= <octet>{3}
 * flags ::= <octet>
 * if flags & 0x2; then id ::= <octet>{8}
 * else id ::= {empty}
 * data ::= <octet>+
 */
#ifndef JDMQ_MQ_MESSAGE_H_
#define JDMQ_MQ_MESSAGE_H_

#include <stdint.h>
#include <jdmq/common.h>

#define PACKET_HEADER_MIN_SIZE 8
#define PACKET_HEADER_MAX_SIZE 20
#define PACKET_MAGIC 0xEE

namespace jdmq
{
	class Message
	{
	public:
		Message(int data_size, int type, uint64_t id = 0);
		Message(const void *data, int data_size, int type, uint64_t id = 0);
		virtual ~Message();

		inline uint64_t get_id() const
		{
			return id;
		}

		inline int get_type() const
		{
			return type;
		}

		inline int get_size() const
		{
			return size;
		}

		inline int get_content_size() const
		{
			return size - header_size;
		}

		inline char get_flags() const
		{
			return (char) data[flag_pos];
		}

		inline char * get_data() const
		{
			return (char *) data;
		}

		char * get_content() const;

		bool check_flag(int pos);
		void set_flags(char flags);
		void set_flag(int pos);
		void clear_flag(int pos);
		void set_id(uint64_t message_id);

		int checksum();

		static Message *clone(Message *src, int type, uint64_t id = 0);

	private:
		void init(int data_size, int type, uint64_t id);
		uint64_t id;
		int size;
		int header_size;
		int type;
		int flag_pos;
		char flags;
		unsigned char *data;
	};
}

#endif
