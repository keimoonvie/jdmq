/*
 * Event-driven class
 * To use, extend this class and implement 2 virtual functions.
 */

#ifndef JDMQ_COMMON_EVENT_H_
#define JDMQ_COMMON_EVENT_H_

#ifdef __linux__
#include <google/sparse_hash_map>
#endif
#include <jdmq/common.h>
#include <jdmq/thread.h>
#include <jdmq/timer_set.h>
#include <jdmq/scoped_lock.h>

#define JDMQ_MAX_EVENT 256

namespace jdmq
{
	class Event
	{
	public:
		enum EventType
		{
			EVENT_READ = 0x1,
			EVENT_WRITE = 0x2,
			EVENT_ERROR = 0x4,
			EVENT_ONESHOT = 0x8,
		};

		Event();
		virtual ~Event();

		/*
		 * Initialize, must be called before anythings.
		 */
		bool init();

		/*
		 * Add a file event.
		 */
		bool add_file_event(int fd, int type);

		/*
		 * Clear a file event.
		 */
		bool clear_file_event(int fd, int type);

		/*
		 * Delete a file event.
		 */
		void delete_file_event(int fd);

		/*
		 * Add a timer.
		 */
		bool add_timer(timer_t *timer);		

		/*
		 * The main loop.
		 */
		void loop();

		/*
		 * Call this to stop the main loop.
		 */
		void stop();

		inline bool is_running() const
		{
			return running;
		}

		/*
		 * Implement these virtual functions.
		 */
		virtual void file_event_handler(int fd, int mask) = 0;
		virtual void timer_handler(timer_t *timer) = 0;

	protected:
		int backend_fd;
		bool running;

		timer_set_t timer_set;

#ifdef __linux__
		typedef google::sparse_hash_map<int, int> event_mask_map_t;
                event_mask_map_t event_masks;
		mutex_t event_mutex;
#endif
	};

	typedef Event event_t;
}

#endif
