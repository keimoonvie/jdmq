#ifndef JDMQ_MQ_SPARE_H_
#define JDMQ_MQ_SPARE_H_

#include <jdmq/mq.h>
#include <jdmq/worker.h>

namespace jdmq
{
	class SpareWorker: public Worker
	{
	public:
		SpareWorker(int id, void *data);
		bool run();
	};
}

#endif
