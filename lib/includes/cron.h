#ifndef JDMQ_MQ_CRON_H_
#define JDMQ_MQ_CRON_H_

#include <jdmq/mq.h>
#include <jdmq/worker.h>

namespace jdmq
{
	class CronWorker: public Worker
	{
	public:
		CronWorker(int id, void *data);
		bool run();
	};
}

#endif
