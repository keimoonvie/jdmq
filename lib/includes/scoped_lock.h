/*
 * A lock that automatically unlocks when out of scope.
 */
#ifndef JDMQ_COMMON_SCOPED_LOCK_H_
#define JDMQ_COMMON_SCOPED_LOCK_H_

#include <jdmq/common.h>
#include <jdmq/mutex.h>

namespace jdmq
{
	class ScopedLock
	{
	public:
		ScopedLock(mutex_t & mutex);
		virtual ~ScopedLock();

	private:
		mutex_t *mutex_ptr;
	};

	typedef ScopedLock scoped_lock_t;
}

//A handy macro.
#define jdmq_synchronize(m) jdmq::scoped_lock_t slock(m)

#endif



