#ifndef JDMQ_PROTOCOL_NODE_TIMER_H_
#define JDMQ_PROTOCOL_NODE_TIMER_H_

#include <jdmq/common.h>
#include <jdmq/timer.h>

#define JDMQ_NODE_TIMER_HEARTBEAT 2000
#define JDMQ_NODE_TIMER_KICK_ITER 15

namespace jdmq
{
	class NodeTimer: public timer_t
	{
        public:
		NodeTimer(long pedioric);
		void run();

	private:
                int times;
	};
}

#endif
