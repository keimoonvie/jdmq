/*
 * Logger
 */

#ifndef JDMQ_COMMON_LOGGER_H_
#define JDMQ_COMMON_LOGGER_H_

#include <stdio.h>
#include <stdarg.h>
#include <string>
#include <jdmq/common.h>
#include <jdmq/mutex.h>

namespace jdmq
{
	/*
	 * Abstract log class.
	 */
	class Logger
	{
	public:
		enum LogLevel
		{
			DEBUG, INFO, WARNING, ERROR, FATAL,
		};

		Logger();
		virtual ~Logger();

		inline void set_level(LogLevel level)
		{
			this->level = level;
		}

		inline void set_verbose(bool verbose) 
		{
			this->verbose = verbose;
		}

		void debug(const char * file, int line, const char *group, const char *format, ...);
		void info(const char * file, int line, const char *group, const char *format, ...);
		void warning(const char * file, int line, const char *group, const char *format, ...);
		void error(const char * file, int line, const char *group, const char *format, ...);
		void fatal(const char * file, int line, const char *group, const char *format, ...);

	protected:
		/*
		 * Implement this
		 */
		virtual void vlog(const char * file, int line, const char *group, LogLevel level, const char *format, va_list ap) = 0;

		static const char *log_level_string[];
		LogLevel level;
		bool verbose;
	};

	typedef Logger logger_t;

	/*
	 * Default implementation, use internally.
	 */
	class DefaultLogger: public logger_t
	{
	public:
		DefaultLogger();
		bool init(LogLevel level = INFO, const char *file_name = NULL);
		FILE * set_stream(FILE *stream);
		void finalize();
	protected:
		void vlog(const char * file, int line, const char *group, LogLevel level, const char *format, va_list ap);
		FILE *stream;
		mutex_t mutex;
		bool initialized;
	};

	/*
	 * Singleton logger.
	 */
	class JdmqInternalLogger
	{
	public:
		virtual ~JdmqInternalLogger();
		static JdmqInternalLogger & get_instance();

		void init(Logger::LogLevel level = Logger::INFO, FILE *stream = NULL);
		inline DefaultLogger & get_logger()
		{
			return logger;
		}

	private:
		JdmqInternalLogger();
		DefaultLogger logger;
	};
}

//Some macro.
#define JDMQ_LOGGER_INIT jdmq::JdmqInternalLogger::get_instance().init
#define JDMQ_LOGGER_SET_VERBOSE jdmq::JdmqInternalLogger::get_instance().get_logger().set_verbose
#define JDMQ_LOGGER_DEBUG(s, ...) jdmq::JdmqInternalLogger::get_instance().get_logger().debug(__FILE__, __LINE__, s, ##__VA_ARGS__)
#define JDMQ_LOGGER_INFO(s, ...) jdmq::JdmqInternalLogger::get_instance().get_logger().info(__FILE__, __LINE__, s, ##__VA_ARGS__)
#define JDMQ_LOGGER_WARNING(s, ...) jdmq::JdmqInternalLogger::get_instance().get_logger().warning(__FILE__, __LINE__, s, ##__VA_ARGS__)
#define JDMQ_LOGGER_ERROR(s, ...) jdmq::JdmqInternalLogger::get_instance().get_logger().error(__FILE__, __LINE__, s, ##__VA_ARGS__)
#define JDMQ_LOGGER_FATAL(s, ...) jdmq::JdmqInternalLogger::get_instance().get_logger().fatal(__FILE__, __LINE__, s, ##__VA_ARGS__)
#define JDMQ_LOGGER_FINALIZE jdmq::JdmqInternalLogger::get_instance().get_logger().finalize

#endif
