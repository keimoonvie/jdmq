/*
 * Wrapper of pthread condition variable
 */

#ifndef JDMQ_COMMON_CONDITION_VARIABLE_H_
#define JDMQ_COMMON_CONDITION_VARIABLE_H_

#include <pthread.h>
#include <jdmq/common.h>
#include <jdmq/mutex.h>

namespace jdmq
{
	class ConditionVariable
	{
	public:
		ConditionVariable();
		virtual ~ConditionVariable();

		/*
		 * Wait on a mutex, see mutex.h
		 */
		bool wait(mutex_t & mutex, long timeout = 0);

		/*
		 * Wake up one thread waiting for this condition.
		 */
		bool notify_once();

		/*
		 * Wake up all threads waiting for this condition.
		 */
		bool notify_all();

	private:
		pthread_cond_t cond;
	};

	typedef ConditionVariable cond_t;
}

#endif
