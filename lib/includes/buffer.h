#ifndef JDMQ_MQ_BUFFER_H_
#define JDMQ_MQ_BUFFER_H_

#include <jdmq/common.h>
#include <jdmq/mq.h>

#define JDMQ_BUFFER_MIN_SIZE 64

namespace jdmq
{
	enum BufferCode
        {
		BUFFER_END = 0,
                BUFFER_FULL = 1,
		BUFFER_CLOSE = 2,
		BUFFER_ERROR = 3,
	};

	enum BufferState
	{
		BUFFER_BEGIN = 0,
		BUFFER_READ_SIZE = 1,
		BUFFER_READ_FLAGS = 2,
		BUFFER_READ_ID = 3,
		BUFFER_READ_CONTENT = 4,
	};
	
	enum MessageCode
	{
                MESSAGE_OK = 0,
                MESSAGE_WAITING = 1,
		MESSAGE_MAGIC_BYTE_ERROR = 2,
                MESSAGE_SIZE_ERROR = 3,
                MESSAGE_MALLOC_ERROR = 4,
        };

	class Buffer
	{
	public:
		Buffer(int initial_size);
		virtual ~Buffer();

		BufferCode read(int fd);
		MessageCode extract_message(message_t **msg);
		void reset();
	private:
		char *stream;
		int head;
		int size;
		int initial_size;
		int max_size;
		bool need_resize;

		BufferState state;
		int message_size;
                int message_type;
                char flags;
                uint64_t message_id;
		int current;

		void flush(int message_size);
	};
}

#endif
