#ifndef JDMQ_MQ_H_
#define JDMQ_MQ_H_

#include <jdmq/shared_ptr.h>

namespace jdmq
{
	enum SocketState
        {
                SOCKET_RUNNING = 0,
                SOCKET_CLOSE_WAIT = 1,
                SOCKET_CLOSED = 2,
        };

	class Socket;
	class Message;
	class Worker;
	class Watcher;
	class Interface;
	class Service;
	class WatcherEvent;

	typedef Socket socket_t;
	typedef pointer_t<socket_t>::shared socket_ptr;
	typedef Message message_t;
	typedef Worker worker_t;
	typedef Watcher watcher_t;
	typedef Interface interface_t;
	typedef Service service_t;
	typedef WatcherEvent watcher_event_t;
}

#endif
