#ifndef JDMQ_PROTOCOL_NODE_GROUP_H_
#define JDMQ_PROTOCOL_NODE_GROUP_H_

#include <set>
#include <jdmq/common.h>
#include <jdmq/jdmq.pb.h>
#include <jdmq/node_instance.h>

namespace jdmq
{
	class NodeGroup
	{
	public:
		NodeGroup(const std::string & name, bool self_connected);

		inline const std::string & get_name() const
		{
			return name;
		}
		
		inline bool is_self_connected() const
		{
			return self_connected;
		}

		inline std::set<std::string> & get_connect_to()
		{
			return connect_to;
		}

		inline std::set<std::string> & get_connect_from()
		{
			return connect_from;
		}

		inline node_instance_map_t & get_nodes()
		{
			return nodes;
		}

		NodeGroupMessage serialize_to_proto() const;
		std::string serialize() const;
		std::string to_string() const;
	private:
		std::string name;
		bool self_connected;
		std::set<std::string> connect_to;
		std::set<std::string> connect_from;
		node_instance_map_t nodes;
	};

	typedef NodeGroup node_group_t;
	typedef pointer_t<node_group_t>::shared node_group_ptr;
	typedef google::sparse_hash_map<std::string, node_group_ptr> node_group_map_t;
}

#endif
