#ifndef JDMQ_PROTOCOL_NODE_INSTANCE_H_
#define JDMQ_PROTOCOL_NODE_INSTANCE_H_

#include <stdint.h>
#include <string>
#include <google/sparse_hash_map>
#include <jdmq/common.h>
#include <jdmq/scoped_lock.h>
#include <jdmq/shared_ptr.h>
#include <jdmq/mq.h>
#include <jdmq/jdmq.pb.h>

namespace jdmq
{
	enum NodeState
        {
                NODE_DEATH = 0,
                NODE_ALIVE = 1,
                NODE_ACTIVE = 2,
                NODE_DECOMMISSIONED = 3,
        };

	class NodeInstance
	{
	public:
		NodeInstance();
		NodeInstance(uint32_t id, const std::string & group, const std::string & host, const std::string & port);

		inline NodeInfoMessage & get_info()
		{
			return info;
		}

		inline socket_ptr get_socket() const
		{
			return socket;
		}

		void connect(socket_ptr socket);
		void active();
		void disconnect();
		bool send_async(message_t *msg);
		bool send_sync(message_t *msg, int timeout, message_t **reply);

		std::string serialize() const;
                bool deserialize(const std::string & str);
                bool deserialize(const char *data, int size);

		std::string to_string() const;

	private:
		NodeInfoMessage info;
		socket_ptr socket;
		mutex_t mutex;
	};

	typedef NodeInstance node_instance_t;
	typedef pointer_t<node_instance_t>::shared node_instance_ptr;
	typedef google::sparse_hash_map<uint32_t, node_instance_ptr> node_instance_map_t;
	typedef google::sparse_hash_map<Socket *, node_instance_ptr> socket_node_map_t;
}

#endif

