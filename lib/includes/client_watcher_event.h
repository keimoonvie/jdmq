#ifndef JDMQ_PROTOCOL_CLIENT_WATCHER_EVENT_H_
#define JDMQ_PROTOCOL_CLIENT_WATCHER_EVENT_H_

#include <jdmq/common.h>
#include <jdmq/watcher_event.h>

namespace jdmq
{
	class Client;
	class ClientWatcherEvent: public watcher_event_t
	{
	public:
		void set_client(Client *client);
		void on_accept(socket_ptr socket);
		void on_close(socket_ptr socket);
	private:
		Client *client;
	};
}

#endif
