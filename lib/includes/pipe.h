#ifndef JDMQ_MQ_PIPE_H_
#define JDMQ_MQ_PIPE_H_

#include <jdmq/common.h>
#include <jdmq/scoped_lock.h>
#include <jdmq/mq.h>

namespace jdmq
{
	class Pipe
	{
	public:
		Pipe(int fd, watcher_t *watcher, int max_size);
		virtual ~Pipe();

		bool send(const char *data, int data_size);
		bool open_gate();
	private:
		enum PipeState
		{
			PIPE_STATE_WAIT_WRITE = 0,
			PIPE_STATE_WAIT_EVENT = 1,
		};
		watcher_t *watcher;
		PipeState state;
		char *stream;
		int fd;
		int head;
		int size;
		int initial_size;
		int max_size;
		mutex_t mutex;

		bool append(const char *data, int data_size);
		void reset();
		int write(const char *data, int dsize);
	};
}

#endif
