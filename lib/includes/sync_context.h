#ifndef JDMQ_PROTOCOL_SYNC_CONTEXT_H_
#define JDMQ_PROTOCOL_SYNC_CONTEXT_H_

#include <jdmq/common.h>
#include <jdmq/scoped_lock.h>
#include <jdmq/condition_variable.h>
#include <jdmq/mq.h>
#include <jdmq/context.h>

#define JDMQ_CLIENT_SYNC_CONTEXT_TYPE 1000000

namespace jdmq
{
	class SyncContext: public context_t
	{
	public:
		SyncContext(int64_t id);
		virtual ~SyncContext();

		bool wait(int sec);
		bool notify(message_t *message);

		inline message_t *get_message()
		{
			return message;
		}
		
	private:
		mutex_t mutex;
		cond_t cond;

		message_t *message;
		bool timed_out;
		bool done;
	};
}

#endif
