#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <jdmq/jdmq.h>

using namespace std;
using namespace jdmq;

int main()
{
	JDMQ_LOGGER_INIT(Logger::INFO);
	client_t client;
	client.init();
	client.add_node(1, "test", "127.1", "51001");
	client.fork_and_serve();

	int size = 1000000;
	char *data = (char *) malloc(size);
	memset(data, 'A', size);
	cout << Utilities::get_current_time_micro() << endl;
	for (int i = 0; i < 1000; i++) {
		message_t msg(data, size, 1337);
		client.send_async("test", &msg);
	}

	sleep(1);

	client.stop();
	client.join();
}
