#include <iostream>
#include <jdmq/jdmq.h>

using namespace std;
using namespace jdmq;

class TestService: public service_t
{
public:
	bool process(message_t *message, socket_ptr socket)
	{
		if (message->get_id() == 1000)
			cout << Utilities::get_current_time_micro() << endl;
	}
};

int main(int argc, char *argv[])
{
	if (argc < 2) {
		cerr << "Usage: " << argv[0] << " config_file" << endl;
		return 2;
	}
	Node & node = Node::get_instance();
	node.get_watcher()->add_service(new TestService());
	if (!node.init(argv[1]))
		return 1;
	node.serve();
}
